﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XSLTComponent.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The xslt component.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using ikvm.extensions;
using NTB.Normalizer.Components.enums;
using NTB.Normalizer.Components.interfaces;

namespace NTB.Normalizer.Components
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Timers;
    using System.Xml;
    
    // Logging
    using log4net;

    // XML Utilities
    using Utilities;

    /// <summary>
    /// The xslt component.
    /// </summary>
    public partial class XsltComponent : Component, IBaseNormComponents
    {
        #region Common class variables
        
        /// <summary>
        /// The my xslt converter.
        /// </summary>
        public XsltConverter MainXsltConverter = new XsltConverter();

        /// <summary>
        /// The configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected string Name;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly configured
        /// </summary>
        /// <remarks>Holds the configured status of the instance. <c>True</c> means successfully configured</remarks>
        protected bool Configured = false;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected bool ErrorRetry = false;

        /// <summary>
        /// PollDelay is used to make sure that the file is ready to be accessed. 
        /// </summary>
        protected int PollDelay = 0;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected string EmailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailBody;

        /// <summary>
        /// The iptc sequence number.
        /// </summary>
        protected bool IptcSequenceNumber;

        /// <summary>
        /// The iptc sequence number file.
        /// </summary>
        protected string IptcSequenceNumberFile;

        #endregion

        #region Polling settings

        /// <summary>
        /// The configured schedule for this instance
        /// </summary>
        /// <remarks>This is set to a time of day and is used by the <c>Scheduled</c> pollstyle. Work is triggered at the given time of day.</remarks>
        protected string Schedule;

        /// <summary>
        /// The configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;

        #endregion

        #region File folders

        /// <summary>
        /// Input file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter"/> and <see cref="IncludeSubdirs"/>
        /// </remarks>
        protected string FileInputFolder;

        /// <summary>
        /// Output file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter"/> and <see cref="IncludeSubdirs"/>
        /// </remarks>
        protected Dictionary<int, string> FileOutputFolders = new Dictionary<int, string>();

        /// <summary>
        /// Error file folder
        /// </summary>
        /// <remarks>
        ///   <para>File folder where failing files are stored.</para>
        ///   <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        protected string FileErrorFolder;

        /// <summary>
        /// File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///   <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///   <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        protected string FileDoneFolder;

        #endregion

        #region Optional configuration

        /// <summary>
        /// Input file filter
        /// </summary>
        /// <remarks>
        ///   <para>Defines what file types are being read from <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>NTBIDType.None</c></para>
        /// </remarks>
        protected string FileFilter = "*.xml";

        /// <summary>
        /// Watch subdirs for new files as well
        /// </summary>
        /// <remarks>
        ///   <para>Defines if the instance should look in subdirectories within <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>False</c></para>
        /// </remarks>
        protected bool IncludeSubdirs = false;

        /// <summary>
        /// Buffer size for the file watcher
        /// </summary>
        /// <remarks>
        ///   <para>Defines the internal buffer size the FileSystemWatcher when using this polling.
        /// Should not be set to high. The instance will fall back to contious polling on buffer overflows.</para>
        ///   <para>Default value: <c>4096</c></para>
        /// </remarks>
        protected int BufferSize = 4096;

        /// <summary>
        /// Flag to say if this component is working or not. I have a feeling we have another parameter for this though.
        /// </summary>
        protected bool BusyFlag = false;

        /// <summary>
        /// Dictionary to map ID to encodings, which we can use to say which type of encoding the output file shall have
        /// </summary>
        protected Dictionary<int, string> DocumentEncodings = new Dictionary<int, string>();

        /// <summary>
        /// Dictionary to map ID to file extensions, which we can use to say which type of encoding the output file shall have
        /// </summary>
        protected Dictionary<int, string> DocumentExtensions = new Dictionary<int, string>();

        #endregion

        #region Logging

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(XsltComponent));
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        #endregion

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);

        /// <summary>
        /// The _path xslt.
        /// </summary>
        private readonly string _pathXslt = ConfigurationManager.AppSettings["dirXSLTRoot"];

        private ReaderWriterLockSlim rwlock;

        
        /// <summary>
        /// Initializes a new instance of the <see cref="XsltComponent"/> class. 
        /// Variable used to tell if there is a convertion in the job that has started
        /// </summary>
        public XsltComponent()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            rwlock = new ReaderWriterLockSlim();
            // filesWatcher.InternalBufferSize = 4*4096;

            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XsltComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public XsltComponent(IContainer container)
        {
            container.Add(this);
            
            InitializeComponent();
        }

        #region Component Related

        /// <summary>
        /// Gets a value indicating whether the enabled status is true or false.
        /// </summary>
        /// <value>The enabled status.</value>
        /// <remarks><c>True</c> if the job instance is enabled.</remarks>
        public bool Enabled
        {
            get
            {
                return enabled;
            }
        }

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The configured instance job name.</remarks>
        public string InstanceName
        {
            get
            {
                return Name;
            }
        }

        /// <summary>
        /// Gets the component state.
        /// </summary>
        public ComponentState ComponentState
        {
            get
            {
                return componentState;
            }
        }

        /// <summary>
        /// Gets the operation mode. 
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Converter</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get
            {
                return OperationMode.Converter;
            }
        }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Converter</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get
            {
                return pollStyle;
            }
        }

        /// <summary>
        /// Gets or sets the iptc sequence number max.
        /// </summary>
        protected int IptcSequenceNumberMax { get; set; }

        /// <summary>
        /// Gets or sets the iptc sequence number min.
        /// </summary>
        protected int IptcSequenceNumberMin { get; set; }

        #endregion

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// If something goes wrong we are returing an ArgumentException exception
        /// </exception>
        /// <exception cref="NotSupportedException">
        /// If something goes wrong we are returing an NotSupportedException exception
        /// </exception>
        public void Configure(XmlNode configNode)
        {
            

            Logger.Debug("Node: " + configNode.Name);

            // Basic configuration sanity check
            if (configNode.Attributes != null
                && (configNode.Name != "XSLTComponent" || configNode.Attributes.GetNamedItem("Name") == null))
            {
                throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
            }

            // Logger.Debug("Name attribut: " + configNode.Attributes.GetNamedItem("Name"));
            // Basic configuration sanity check
            // if (configNode.Attributes != null && (configNode != null && configNode.Name == "XSLTComponent" &&
            // configNode.Attributes.GetNamedItem("Name") != null))
            // {
            // throw new ArgumentException("The XML configuration node passed is invalid", 
            // "configNode");
            // }

            // Getting the name of this Component instance
            try
            {
                if (configNode.Attributes != null)
                {
                    Name = configNode.Attributes["Name"].Value;
                    
                    ThreadContext.Stacks["NDC"].Push(InstanceName);
                    
                    enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);

                    PollDelay = Convert.ToInt32(configNode.Attributes["Delay"].Value);
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance. Check error log for more information.");
                ErrorLogger.Fatal("Not possible to configure this job instance!", ex);

                ThreadContext.Stacks["NDC"].Pop();

                throw new ArgumentException("Invalid or missing values in XML configuration node", ex);
            }

            // Basic operation
            try
            {
                if (configNode.Attributes != null)
                {
                    pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }

            // Check if we are to use sequence numbers
            SetIptcSequenceNumber(configNode);

            SetXmlInputFolder(configNode);

            SetOutputFolders(configNode);
            SetDocumentEncodings(configNode);
            SetDocumentExtensions(configNode);

            // adding the done folder
            SetDoneFolder(configNode);

            // adding the error folder
            SetErrorFolder(configNode);

            if (enabled)
            {
                #region File folders to access

                // Checking if file folders exists
                string fileOutputFolder = string.Empty;

                try
                {
                    DirectoryInfo di = new DirectoryInfo(FileInputFolder);
                    if (!di.Exists)
                    {
                        Directory.CreateDirectory(FileInputFolder);
                    }

                    foreach (KeyValuePair<int, string> kvp in FileOutputFolders)
                    {
                        fileOutputFolder = kvp.Value;
                        di = new DirectoryInfo(fileOutputFolder);
                        if (!di.Exists)
                        {
                            Directory.CreateDirectory(fileOutputFolder);
                        }
                    }

                    // Check if folder exists
                    di = new DirectoryInfo(FileErrorFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }

                    // Check if folder exists
                    di = new DirectoryInfo(FileDoneFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid, unknown or missing file folder: " + fileOutputFolder, ex);
                }

                #endregion

                #region Set up polling

                try
                {
                    // Switch on pollstyle
                    switch (pollStyle)
                    {
                        case PollStyle.Continous:
                            if (configNode.Attributes != null)
                            {
                                Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
                            }
                            if (Interval == 0)
                            {
                                Interval = 5000;
                            }

                            pollTimer.Interval = Interval * 1000; // short startup - interval * 1000;
                            Logger.DebugFormat("Poll interval: {0} seconds", Interval);

                            break;

                        case PollStyle.Scheduled:

                            /* 
                            schedule = configNode.Attributes["Schedule"].Value;
                            TimeSpan s = Utilities.GetScheduleInterval(schedule);
                            logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                            pollTimer.Interval = s.TotalMilliseconds;
                            */
                            throw new NotSupportedException("Invalid polling style for this job type");

                        case PollStyle.FileSystemWatch:

                            // Check for config overrides
                            if (configNode.Attributes != null && configNode.Attributes.GetNamedItem("BufferSize") != null)
                            {
                                BufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                            }

                            Logger.DebugFormat("FileSystemWatcher buffer size: {0}", BufferSize);

                            // Set values
                            filesWatcher.Path = FileInputFolder;
                            filesWatcher.Filter = FileFilter;
                            filesWatcher.IncludeSubdirectories = IncludeSubdirs;
                            filesWatcher.InternalBufferSize = BufferSize;
                            filesWatcher.Error += FilesWatcherOnError;

                            // Do not start the event watcher here, wait until after the startup cleaning job
                            pollTimer.Interval = 5000; // short startup;

                            break;

                        default:

                            // Unsupported pollstyle for this object
                            throw new NotSupportedException("Invalid polling style for this job type");
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException(
                        "Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
                }

                #endregion
            }

            // Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            Configured = true;
        }

        

        private void SetErrorFolder(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("XMLErrorFolder");
                if (folderNode != null)
                {
                    FileErrorFolder = folderNode.InnerText;
                }

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / File done folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof (PollStyle), pollStyle),
                    FileErrorFolder,
                    enabled);
            }
            catch (Exception ex)
            {
                ErrorLogger.Fatal("Not possible to get Done-folder", ex);
                Logger.Fatal("Not possible to get Done-folder. Check error logger for more information");
            }
        }

        private void SetDoneFolder(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("XMLDoneFolder");
                if (folderNode != null)
                {
                    FileDoneFolder = folderNode.InnerText;
                }

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / File done folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof (PollStyle), pollStyle),
                    FileDoneFolder,
                    enabled);
            }
            catch (Exception ex)
            {
                ErrorLogger.Fatal("Not possible to get Done-folder", ex);
                Logger.Fatal("Not possible to get Done-folder. Check error log for more information");
            }
        }

        private void SetDocumentExtensions(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                XmlNodeList folderNodes = configNode.SelectNodes("XMLOutputFolders/XMLOutputFolder");
                if (folderNodes != null)
                {
                    foreach (XmlNode folderNode in folderNodes.Cast<XmlNode>().Where(folderNode => folderNode != null))
                    {
                        if (folderNode.Attributes == null)
                        {
                            continue;
                        }

                        DocumentExtensions.Add(
                            Convert.ToInt32(folderNode.Attributes["Id"].Value),
                            folderNode.Attributes["Extention"] != null ? folderNode.Attributes["Extention"].Value : "xml");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder. Check error log for more information");
                ErrorLogger.Fatal("Not possible to get output-folder", ex);
            }
        }


        private void SetDocumentEncodings(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                XmlNodeList folderNodes = configNode.SelectNodes("XMLOutputFolders/XMLOutputFolder");
                if (folderNodes != null)
                {
                    foreach (XmlNode folderNode in folderNodes.Cast<XmlNode>().Where(folderNode => folderNode != null))
                    {
                        if (folderNode.Attributes == null)
                        {
                            continue;
                        }
                        
                        DocumentEncodings.Add(
                            Convert.ToInt32(folderNode.Attributes["Id"].Value),
                            folderNode.Attributes["Encoding"] != null ? folderNode.Attributes["Encoding"].Value : "iso-8859-1");

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder. Check error log for more information");
                ErrorLogger.Fatal("Not possible to get output-folder", ex);
            }
        }
        
        private void SetOutputFolders(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                XmlNodeList folderNodes = configNode.SelectNodes("XMLOutputFolders/XMLOutputFolder");
                if (folderNodes != null)
                {
                    foreach (XmlNode folderNode in folderNodes.Cast<XmlNode>().Where(folderNode => folderNode != null))
                    {
                        if (folderNode.Attributes == null)
                        {
                            continue;
                        }

                        FileOutputFolders.Add(Convert.ToInt32(folderNode.Attributes["Id"].Value), folderNode.InnerText);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder. Check error log for more information.");
                ErrorLogger.Fatal("Not possible to get output-folder", ex);
            }
        }

        private void SetXmlInputFolder(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("XMLInputFolder");

                // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                if (folderNode != null)
                {
                    FileInputFolder = folderNode.InnerText;

                    // Check if folder exists
                    DirectoryInfo di = new DirectoryInfo(FileInputFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof (PollStyle), pollStyle),
                    FileInputFolder,
                    enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get input-folder. Check error log for more information.");
                ErrorLogger.Fatal("Not possible to get input-folder", ex);
            }
        }

        private void SetIptcSequenceNumber(XmlNode configNode)
        {
            try
            {
                XmlNode folderNode = configNode.SelectSingleNode("IPTCSequenceNumber");
                if (folderNode != null)
                {
                    if (configNode.Attributes != null)
                    {
                        if (folderNode.Attributes != null)
                        {
                            IptcSequenceNumber = Convert.ToBoolean(folderNode.Attributes["value"].Value);
                            IptcSequenceNumberMin = Convert.ToInt32(folderNode.Attributes["min-number"].Value);
                            IptcSequenceNumberMax = Convert.ToInt32(folderNode.Attributes["max-number"].Value);
                        }
                    }

                    if (IptcSequenceNumber)
                    {
                        XmlNode iptcFolderNode = configNode.SelectSingleNode("IPTCSequenceNumber/SequenceNumberFile");

                        if (iptcFolderNode != null)
                        {
                            IptcSequenceNumberFile = iptcFolderNode.InnerText;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Info("We are not to use IPTC-Sequence-numbers");
                Logger.Debug(exception.Message);
                Logger.Debug(exception.StackTrace);
            }
        }

        /// <summary>
        /// The start.
        /// </summary>
        /// <exception cref="NormalizerException">
        /// If the service does not start, we send a Normalizer Exception
        /// </exception>
        public void Start()
        {
            if (!Configured)
            {
                throw new NormalizerException(
                    "XSLTComponent is not properly configured. XSLTComponent::Start() Aborted!");
            }

            if (!enabled)
            {
                throw new NormalizerException("XSLTComponent is not enabled. XSLTComponent::Start() Aborted!");
            }

            Logger.Info("Starting " + InstanceName);

            _stopEvent.Reset();
            _busyEvent.Set();

            componentState = ComponentState.Running;
            pollTimer.Start();
        }

        /// <summary>
        /// The stop.
        /// </summary>
        /// <exception cref="NormalizerException">
        /// If the service does not stop, we send a Normalizer Exception
        /// </exception>
        public void Stop()
        {
            if (!Configured)
            {
                throw new NormalizerException("XSLTComponent is not properly configured. XSLTComponent::Stop() Aborted");
            }

            if (!enabled)
            {
                throw new NormalizerException("XSLTComponent is not properly configured. XSLTComponent::Stop() Aborted");
            }

            const double epsilon = 0;
            if (!pollTimer.Enabled
                && (ErrorRetry || pollStyle != PollStyle.FileSystemWatch
                    || Math.Abs(pollTimer.Interval - 5000) < epsilon))
            {
                Logger.InfoFormat("Waiting for instance to complete work. Job {0}", InstanceName);
            }

            // Signal Stop
            _stopEvent.Set();

            // Stop events
            filesWatcher.EnableRaisingEvents = false;

            if (!_busyEvent.WaitOne(30000))
            {
                Logger.InfoFormat(
                    "Instance did not complete properly. Data may be inconsistent. Job: {0}", InstanceName);
            }

            componentState = ComponentState.Halted;

            // Kill polling
            pollTimer.Stop();
        }

        /// <summary>
        /// This method is used to call the converter class 
        /// </summary>
        /// <param name="files">
        /// List of strings/files that are to be converted
        /// </param>
        /// <param name="fileXsl">
        /// The XSLT-document to be used to parse the files
        /// </param>
        public void Converter(IEnumerable<string> files, string fileXsl)
        {
            Logger.Debug("In Converter - List<string> Files, string FileXSL");

            Logger.Debug("FileXSL: " + fileXsl);

            // Setting the value of the IptcSequenceNumber Check
            MainXsltConverter.IptcSequenceNumber = IptcSequenceNumber;

            // Setting the value of the IptcSequenceNumberFile
            MainXsltConverter.IptcSequenceNumberFile = IptcSequenceNumberFile;

            MainXsltConverter.IptcSequenceNumberMin = IptcSequenceNumberMin;

            MainXsltConverter.IptcSequenceNumberMax = IptcSequenceNumberMax;

            // Defining the XSLT-file
            MainXsltConverter.FileXsl = fileXsl;

            // defining document output
            MainXsltConverter.OutputDocumentExtension = DocumentExtensions;

            // Defining output path(s)
            MainXsltConverter.OutputPaths = FileOutputFolders;

            // Defining error folder
            MainXsltConverter.PathError = FileErrorFolder;

            // Defining encoding(s)
            MainXsltConverter.OutputEncodings = DocumentEncodings;

            MainXsltConverter.JobName = InstanceName;

            MainXsltConverter.PathError = FileErrorFolder;
            /*
             * We are using this for the time being
             */
            var enumerable = files as string[] ?? files.ToArray();
            if (files != null && !enumerable.Any()) return;
            
            Logger.Debug("---------------------------> Starting loop <--------------------------");

            foreach (string filename in enumerable)
            {
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(filename);
                }
                catch (Exception ex)
                {
                    throw new NormalizerException("Could not open XML file: " + filename, ex);
                }

                BusyFlag = true;
                Logger.Debug("Files: " + filename);

                string transformedFile = MainXsltConverter.XslConverter(filename);
                if (transformedFile != string.Empty)
                {
                    // Now we shall call a new method
                    MainXsltConverter.CreateNewFile(transformedFile, filename);
                }

                BusyFlag = false;
            }
            Logger.Debug("---------------------------> Ending loop <--------------------------");
            BusyFlag = false;
        }

        /// <summary>
        /// The files watcher_ renamed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void filesWatcher_Renamed(object sender, RenamedEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = InstanceName;
            Logger.InfoFormat("XSLTComponent::filesWatcher_Created() hit with file data: {0}", e.FullPath);

            int pollTime = PollDelay * 1000;
            Logger.Debug("In filesWatcher_Renamed");
            Logger.Debug("We are waiting: " + pollTime + " Seconds before starting to convert!");

            if (PollDelay > 0)
            {
                // We take the value and multiply with thousand - to get the seconds... 
                Thread.Sleep(pollTime);
            }

            try
            {
                // Wait for the busy state
                _busyEvent.WaitOne();

                // Adding files to the list and then sending that list to the converter
                // @done: Create a method that takes string as argument also
                // @done: change XMLToNITFConverter so that it does not loop, but does only one file at the time
                string files = e.FullPath;

                // = new // = e.Name;
                Converter(new[]{files}, _pathXslt + @"\" + InstanceName + @"\" + InstanceName + ".xsl");

                // Reset event
                _busyEvent.Set();
            }
            catch (Exception ex)
            {
                Logger.Debug("Something happened while doing filesWatcher_Created! Check error log for more information");
                ErrorLogger.Debug("Something happened while doing filesWatcher_Created!", ex);
            }
        }

        /// <summary>
        /// The poll timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void pollTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = InstanceName;
            Logger.InfoFormat("XSLTComponent::pollTimer_Elapsed() hit");

            // Reset flags and pollers
            _busyEvent.WaitOne();
            pollTimer.Stop();

            // Process any wating files
            List<string> files =
                new List<string>(
                    Directory.GetFiles(
                        FileInputFolder,
                        FileFilter,
                        IncludeSubdirs ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));

            try
            {
                Logger.Debug("InputFolder for " + InstanceName + " : " + FileInputFolder);

                
                // Some logging
                Logger.DebugFormat("XSLTComponent::pollTimer_Elapsed() Convertion items scheduled: {0}", files.Count);

                // I need to have an Debug-flag here
                if (ConfigurationManager.AppSettings["Debug"].ToUpper() == "TRUE")
                {
                    foreach (string stringFiles in files)
                    {
                        Logger.Debug("Files to be converted are: " + stringFiles);
                    }
                }

                // Enable the event listening
                if (pollStyle == PollStyle.FileSystemWatch && !ErrorRetry)
                {
                    Logger.Debug("Setting filesWatcher.EnableRaisingEvents to true");
                    filesWatcher.EnableRaisingEvents = true;
                }

                // Process the files
                Logger.Debug(
                    "Process the files using XSLT-sheet: " + _pathXslt + @"\" + InstanceName + @"\" + InstanceName
                    + ".xsl");
                Converter(files, _pathXslt + @"\" + InstanceName + @"\" + InstanceName + ".xsl");
            }
            catch (Exception ex)
            {
                ErrorLogger.Debug("Something happened while doing filesWatcher_Created!", ex);
                ErrorLogger.Error("XSLTComponent::pollTimer_Elapsed() error: " + ex.Message, ex);

                // We have to move the file
                foreach (string file in files)
                {
                    ErrorFunctions.PathError = FileErrorFolder;
                    ErrorFunctions.MoveErrorFile(file);
                }
            }

            // Check error state
            if (ErrorRetry)
            {
                filesWatcher.EnableRaisingEvents = false;
            }

            // Restart
            pollTimer.Interval = Interval * 1000;
            if (pollStyle != PollStyle.FileSystemWatch
                || (pollStyle == PollStyle.FileSystemWatch && filesWatcher.EnableRaisingEvents == false))
            {
                pollTimer.Start();
            }

            _busyEvent.Set();
        }

        private void FilesWatcherOnError(object sender, ErrorEventArgs errorEventArgs)
        {
            ErrorLogger.Error(errorEventArgs.GetException());

            if (errorEventArgs.GetException() is InternalBufferOverflowException)
            {
                try
                {
                    int retries = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertRetries"]);
                    int sleep = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertRetryInterval"]);

                    Logger.Debug("======================================");
                    Logger.Debug("======================================");
                    Logger.Debug("======================================");
                    Logger.Debug("======================================");
                    Logger.Debug("======================================");
                    Logger.Debug("======================================");
                    Logger.Debug("=========  DOING STUFF  =============");
                    Logger.Debug("=========     AFTER     =============");
                    Logger.Debug("=========   WATCH HIT   =============");
                    Logger.Debug("======================================");
                    Logger.Debug("======================================");
                    Logger.Debug("======================================");
                    Logger.Debug("======================================");
                    Logger.Debug("======================================");
                    Logger.Debug("======================================");
                    var systemWatcher = sender as FileSystemWatcher;

                    // reflection
                    
                    if (systemWatcher != null)
                    {
                        var watcher = systemWatcher;

                        var di = new DirectoryInfo(watcher.Path);

                        var files = di.GetFiles();

                        if (files.Any())
                        {
                            foreach (var file in files)
                            {
                                ProcessFile(file.FullName, retries, sleep);
                            }
                        }
                    }
                    
                }
                catch (Exception exception)
                {
                    ErrorLogger.Error(exception);
                }
            }

            if (errorEventArgs.GetException().InnerException != null)
            {
                ErrorLogger.Error(errorEventArgs.GetException().InnerException);
            }

            
            

            



            // Making sure the files watcher is listening.
            if (filesWatcher.EnableRaisingEvents == false)
            {
                filesWatcher.EnableRaisingEvents = true;
            }
        }

        /// <summary>
        /// The files watcher_ created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void filesWatcher_Created(object sender, FileSystemEventArgs e)
        {

            int retries = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertRetries"]);
            int sleep = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertRetryInterval"]);

            ThreadContext.Properties["JOBNAME"] = InstanceName;
            Logger.InfoFormat("XSLTComponent::filesWatcher_Created() hit with file data: {0}", e.FullPath);

            int pollTime = PollDelay * 1000;
            Logger.Debug("In filesWatcher_Created");
            Logger.Info("We are waiting: " + pollTime + " Seconds before starting to convert!");

            if (PollDelay > 0)
            {
                // We take the value and multiply with thousand - to get the seconds... 
                Thread.Sleep(pollTime);
            }

            try
            {
                rwlock.EnterWriteLock();

            }
            finally
            {
                rwlock.ExitWriteLock();
            }

            bool errorState = false;

            _busyEvent.WaitOne();

            var listOfFiles = new List<string> {e.FullPath};
            var newFile = e.FullPath;
            ProcessFile(newFile, retries, sleep);

            //if (errorState == true)
            //{
            //    ErrorFunctions.PathError = FileErrorFolder;
            //    ErrorFunctions.MoveErrorFile(e.FullPath);
            //}


            // Reset event
            _busyEvent.Set();

            
            //try
            //{
            //    Logger.Debug("======================================");
            //    Logger.Debug("======================================");
            //    Logger.Debug("======================================");
            //    Logger.Debug("======================================");
            //    Logger.Debug("======================================");
            //    Logger.Debug("======================================");
            //    Logger.Debug("=========  DOING STUFF  =============");
            //    Logger.Debug("=========     AFTER     =============");
            //    Logger.Debug("=========   WATCH HIT   =============");
            //    Logger.Debug("======================================");
            //    Logger.Debug("======================================");
            //    Logger.Debug("======================================");
            //    Logger.Debug("======================================");
            //    Logger.Debug("======================================");
            //    Logger.Debug("======================================");
            //    var path = Path.GetDirectoryName(e.FullPath);
            //    var di = new DirectoryInfo(path);
                
            //    var files = di.GetFiles();

            //    if (files.Any())
            //    {
            //        foreach (var file in files)
            //        {
            //            ProcessFile(file.FullName, retries, sleep);
            //        }
            //    }
            //}
            //catch (Exception exception)
            //{
            //    ErrorLogger.Error(exception);
            //}

            

            // Making sure the files watcher is listening.
            if (filesWatcher.EnableRaisingEvents == false)
            {
                filesWatcher.EnableRaisingEvents = true;
            }

        }

        private void ProcessFile(string file, int retries, int sleep)
        {
            bool errorState;
            for (int i = 0; i <= retries; i++)
            {
                try
                {
                    // Wait for the busy state


                    // Adding files to the list and then sending that list to the converter
                    
                    try
                    {
                        Converter(new[] {file}, _pathXslt + @"\" + InstanceName + @"\" + InstanceName + ".xsl");


                        // Exit loop here...
                        i = retries;
                    }
                    catch (NormalizerException exception)
                    {
                        // Checking if we can read the file or not. 
                        if ((exception.InnerException is IOException || exception.InnerException is XmlException)
                            && i < retries)
                        {
                            Logger.WarnFormat("Open/Read failed, retrying file: {0} Error: {1} ", file,
                                exception.InnerException.Message);
                            Thread.Sleep(sleep);
                        }
                        else
                        {
                            // Moving file to error folder
                            throw;
                        }
                    }
                    finally
                    {
                        errorState = false;
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.Error("Something happened while doing filesWatcher_Created!", ex);
                    ErrorLogger.Error(ex.Message);
                    ErrorLogger.Error(ex.StackTrace);

                    if (ex.InnerException == null) continue;
                    ErrorLogger.Error(ex.InnerException.Message);
                    ErrorLogger.Error(ex.InnerException.StackTrace);

                    errorState = true;
                }
            }
        }
    }
}