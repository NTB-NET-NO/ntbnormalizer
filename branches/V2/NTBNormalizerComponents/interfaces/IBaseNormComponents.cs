﻿using System;
using System.Xml;
using NTB.Normalizer.Components.enums;

namespace NTB.Normalizer.Components.interfaces
{
    public interface IBaseNormComponents
    {
        /// <summary>
        /// Configure the component instance with all necessary data
        /// </summary>
        /// <param name="configNode"></param>
        void Configure(XmlNode configNode);

        /// <summary>
        /// Starts the polling for this instance
        /// </summary>
        void Start();

        /// <summary>
        /// Stops the polling for this instance
        /// </summary>
        void Stop();

        /// <summary>
        /// Gets the enabled status of the instance.
        /// </summary>
        /// <value>The enabled status.</value>
        Boolean Enabled { get; }

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        String InstanceName { get; }

        /// <summary>
        /// Gets the operation mode.
        /// </summary>
        /// <value>The operation mode.</value>
        OperationMode OperationMode { get; }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        PollStyle PollStyle { get; }

        /// <summary>
        /// Gets the ComponentState
        /// </summary>
        ComponentState ComponentState { get; }
    }
}
