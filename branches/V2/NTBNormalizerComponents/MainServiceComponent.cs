﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.ServiceModel;
using System.IO;
using System.Xml;

// Adding support for log4net
using log4net;
using NTB.Normalizer.Components.enums;
using NTB.Normalizer.Components.interfaces;

namespace NTB.Normalizer.Components
{
    public partial class MainServiceComponent : Component
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(MainServiceComponent));
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        static MainServiceComponent()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();
        }

        /// <summary>
        /// Initializes a new instance of the class
        /// </summary>
        public MainServiceComponent()
        {
            InitializeComponent();
        }

        public MainServiceComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        #region Instance variables and control data

        /// <summary>
        /// Name of the file that contains the job configuration set
        /// </summary>
        /// <remarks>The name of the job config XML file is stored here.</remarks>
        protected String ConfigSet = String.Empty;

        /// <summary>
        /// Enable watching the config set for changes and reconfigure at runtime
        /// </summary>
        /// <remarks>config setting that defines if we are wathcing the config set for changes/auto reconfigure or not.</remarks>
        protected Boolean WatchConfigSet = false;


        /// <summary>
        /// List of currently configured jobs
        /// </summary>
        /// <remarks>Internal Dictionary to keep track of running worker jobs.</remarks>
        protected Dictionary<String, IBaseNormComponents> JobInstances = 
            new Dictionary<String, IBaseNormComponents>();


        /// <summary>
        /// Notification handler service host
        /// </summary>
        /// <remarks>
        /// This is the actuall service host that controls the notification object 
        /// </remarks>
        protected ServiceHost ServiceHost = null;



        #endregion





        #region Control functions


        public void Configure()
        {
            try
            {
                // NDC.Push("CONFIG");
                ThreadContext.Stacks["NDC"].Push("CONFIG");


                // Checking if a directory exists or not
                DirectoryInfo di = new DirectoryInfo(
                    ConfigurationManager.AppSettings["dirOutput"]);

                if (!di.Exists)
                {
                    di.Create();
                }


                // Read params 
                ConfigSet = ConfigurationManager.AppSettings["ConfigurationSet"];
                WatchConfigSet = Convert.ToBoolean(ConfigurationManager.AppSettings["WatchConfiguration"]);


                // Logging
                Logger.InfoFormat("{0} : {1}", "ConfigurationSet", ConfigSet);
                Logger.InfoFormat("{0} : {1}", "WatchConfiguration", WatchConfigSet);

                // Load configuration set

                XmlDocument config = new XmlDocument();
                config.Load(ConfigSet);

                // Setting up the watcher
                configFileWatcher.Path = Path.GetDirectoryName(ConfigSet);
                configFileWatcher.Filter = Path.GetFileName(ConfigSet);

                // Create XSLT Components 
                XmlNodeList nodes = config.SelectNodes("/ComponentConfiguration/XSLTComponents/XSLTComponent[@Enabled='True']");
                if (nodes != null) Logger.InfoFormat("NormalizerComponent job instances found: {0}", nodes.Count);
                JobInstances.Clear();
                if (nodes != null)
                    foreach (XmlNode nd in nodes)
                    {
                        IBaseNormComponents xsltComponent = new XsltComponent();
                        xsltComponent.Configure(nd);

                        Logger.Debug("Adding " + xsltComponent.InstanceName);
                        JobInstances.Add(xsltComponent.InstanceName, xsltComponent);
                    }

                nodes = config.SelectNodes("/ComponentConfiguration/CSVComponents/CSVComponent[@Enabled='True']");
                
                if (nodes == null) return;

                Logger.InfoFormat("NormalizerComponent job instances found: {0}", nodes.Count);
                // jobInstances.Clear();
                foreach (XmlNode nd in nodes)
                {
                    IBaseNormComponents csvComponent = new CsvComponent();
                    csvComponent.Configure(nd);

                    Logger.Debug("Adding " + csvComponent.InstanceName);
                    JobInstances.Add(csvComponent.InstanceName, csvComponent);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                MailMessage mailMessage = new MailMessage("505@ntb.no", "thu@ntb.no")
                    {
                        Subject = "NTB Normalizer error",
                        Body =
                            "There has been an error in NTB normalizer: " + exception.Message + "\r\n" +
                            exception.StackTrace + "\r\n"
                    };

                SmtpClient client = new SmtpClient("mail.ntb.no");
                client.Send(mailMessage);
                    
                Logger.Info("We are quitting");
                
                
            }
            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
            }

        }


        /// <summary>
        /// Starts the main service instance.
        /// </summary>
        /// <remarks>
        /// The function will set up polling and events to Start all processing jobs.
        /// </remarks>
        public void Start()
        {
            ThreadContext.Stacks["NDC"].Push("START");

            try
            {
                // Start instances
                Logger.Debug("Number of jobs to start: " + JobInstances.Count.ToString());

                // Looping jobs
                foreach (KeyValuePair<string, IBaseNormComponents> kvp in JobInstances.Where(kvp => kvp.Value.Enabled))
                {
                    Logger.Info("Starting " + kvp.Value.InstanceName);
                    kvp.Value.Start();
                }

                // Starting maintenance
                Logger.Info("Starting maintenance timer");
                Logger.Debug("Maintenance timer interval: " + maintenanceTimer.Interval.ToString());
                maintenanceTimer.Start();

                // Watch the config file
                configFileWatcher.EnableRaisingEvents = WatchConfigSet;
            }
            catch (Exception ex)
            {
                Logger.Fatal("An error has occured. Could not Start service. Check errorlog for more information");
                ErrorLogger.Error(ex.Message);
                ErrorLogger.Error(ex.StackTrace);

                if (ex.InnerException != null) 
                {
                    ErrorLogger.Error(ex.InnerException.Message);
                    ErrorLogger.Error(ex.InnerException.StackTrace);
                }
            }

            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
            }

        }

        public void Pause()
        {
            maintenanceTimer.Stop();
            configFileWatcher.EnableRaisingEvents = false;

            // Stop instances
            foreach (KeyValuePair<string, IBaseNormComponents> kvp in JobInstances.Where(kvp => kvp.Value.Enabled))
            {
                kvp.Value.Stop();
            }
        }

        public void Stop()
        {
            Pause();

            // Kill notification service handler
            if (ServiceHost != null)
            {
                ServiceHost.Close();
                ServiceHost = null;
            }
        }

        #endregion

        private void maintenanceTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            
            MDC.Set("JOBNAME", "MainWorkerJob");

            Logger.Debug("In maintenanceTimer_Elapsed");

            try
            {
                Logger.Debug("Stopping timer");

                maintenanceTimer.Stop();

                /*
                 * I shall check if the jobs that are going really are working and running...
                 * 
                 */
                foreach (
                    KeyValuePair<string, IBaseNormComponents> kvp in
                        JobInstances.Where(kvp => kvp.Value.Enabled)
                                    .Where(kvp => kvp.Value.ComponentState == ComponentState.Halted))
                {
                    Logger.Error("The component is in Halted state, even though it is enabled!");
                    Logger.Info("We are starting " + kvp.Value.InstanceName + " component!");
                    kvp.Value.Start();
                }

                // Checking if we have any folders with files in them
                //foreach (KeyValuePair<string, IBaseNormComponents> kvp in JobInstances.Where(kvp => kvp.Value.Enabled))
                //{
                //    kvp.Value.
                //}


                Logger.Debug("Starting timer");
                maintenanceTimer.Start();



                // Watch the config file
                if (!configFileWatcher.EnableRaisingEvents)
                {
                    configFileWatcher.EnableRaisingEvents = WatchConfigSet;
                }
            }
            catch (Exception exception)
            {
                Logger.ErrorFormat(
                    "MainServiceComponent::maintenanceTimer_Elapsed(): " + exception.Message,
                    exception);
            }
        }

        /// <summary>
        /// Reconfigures everything from an updated config set
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">File change params</param>
        /// <remarks>
        /// <para>When the configuration is updated on disk, this function triggers a complete job reload.</para>
        /// <para>Note that app.config is also reloaded, but refreshed config settings only apply to <c>AppSettings></c> here, not the other sections of the config file.</para>
        /// </remarks>
        private void configFileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = "MainServiceWorker";

            // Outer-try-final to prevent dupe events
            try
            {
                // Stopping file watcher
                configFileWatcher.EnableRaisingEvents = false;
                Logger.Info("MainServiceComponent::configFileWatcher_Changed() hit. Will reconfigure.");


                ThreadContext.Stacks["NDC"].Push("RECONFIGURE");
                // Pause everything
                Pause();

                //Give it a little break
                System.Threading.Thread.Sleep(5000);

                // Reconfigure
                ConfigurationManager.RefreshSection("appSettings");
                Configure();

                Start();
                
            }
            catch (Exception ex)
            {
                Logger.Fatal("NTBNormalizer reconfiguration failed - TERMINATING! Check error log for more information");
                ErrorLogger.Error(ex.Message);
                ErrorLogger.Error(ex.StackTrace);

                if (ex.InnerException != null)
                {
                    ErrorLogger.Error(ex.InnerException.Message);
                    ErrorLogger.Error(ex.InnerException.StackTrace);
                }
                throw;
            }

            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
                configFileWatcher.EnableRaisingEvents = WatchConfigSet;
            }

        }

    }
}