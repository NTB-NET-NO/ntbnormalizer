﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringWriterWithEncoding.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The string writer with encoding.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.Normalizer.Components
{
    using System.IO;
    using System.Text;

    /// <summary>
    /// The string writer with encoding.
    /// </summary>
    public sealed class StringWriterWithEncoding : StringWriter
    {
        /// <summary>
        /// The _encoding.
        /// </summary>
        private readonly Encoding _encoding;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringWriterWithEncoding"/> class.
        /// </summary>
        /// <param name="encoding">
        /// The encoding.
        /// </param>
        public StringWriterWithEncoding(Encoding encoding)
        {
            this._encoding = encoding;
        }

        /// <summary>
        /// Gets the encoding.
        /// </summary>
        public override Encoding Encoding
        {
            get
            {
                return this._encoding;
            }
        }
    }
}