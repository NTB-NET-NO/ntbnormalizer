﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using log4net;


namespace NTB.Normalizer.Components
{
    /// <summary>
    /// Class to use when wanting to notify someone about something.
    /// Preferabely by mail
    /// </summary>
    class Notifyer
    {
        /// <summary>
        /// The logger object to be used to create the log4net object
        /// </summary>
        static ILog logger = LogManager.GetLogger(typeof(Notifyer));

        /// <summary>
        /// Refers to the name of the file
        /// </summary>
        public string PathError = "";

        /// <summary>
        /// Refers to the last error message
        /// </summary>
        public string LastErrorMessage = "";

        /// <summary>
        /// Name of the file that caused the error
        /// </summary>
        public string FileError = "";

        /// <summary>
        /// Boolean value to say if we are to send HTML mail or not
        /// </summary>
        public Boolean HTMLBody = false;

        public void SendMail()
        {
            string strFrom = "NORM@ntb.no";
            string strTo = ConfigurationManager.AppSettings["EmailReceiver"];
            MailMessage AttachementMessage = new MailMessage(strFrom, strTo);
            AttachementMessage.Subject = "Error in NTB Normalizer";

            AttachementMessage.Body = "There was an error with the file " + this.PathError + @"\" + this.FileError + "\r\n";
            AttachementMessage.Body = "The error is: " + LastErrorMessage;
            AttachementMessage.IsBodyHtml = false;

            Attachment attachment = new Attachment(this.PathError + @"\" + this.FileError);

            AttachementMessage.Attachments.Add(attachment);

            string smtpserver = ConfigurationManager.AppSettings["SMTPServer"];

            try
            {

                SmtpClient client = new SmtpClient(smtpserver);
            }
            catch (SmtpException se)
            {
                string strMessage = "An SMTP-Exception has occured: " + se.Message.ToString();
                logger.Error(strMessage);

                if (se.InnerException.Message.ToString() != "")
                {
                    strMessage = "An SMTP-InnerException has occured: " + se.InnerException.Message.ToString();
                    logger.Error(strMessage);
                }


            }
            catch (Exception e)
            {
                logger.Error(e.Message.ToString());
                logger.Error(e.StackTrace.ToString());

                if (e.InnerException.Message.ToString() != "")
                {
                    logger.Error(e.InnerException.Message.ToString());
                }
            }
            
        }
    }
}
