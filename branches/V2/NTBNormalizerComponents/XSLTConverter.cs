﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XSLTConverter.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   This class shall deal with all things related to converting files
//   it pretty much runs the XslCompiledTransform object to do this
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.Normalizer.Components
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Text;
    using System.Xml;

    using java.lang;

    using log4net;

    using Saxon.Api;

    using Configuration = net.sf.saxon.Configuration;
    using Exception = System.Exception;
    using StringBuilder = System.Text.StringBuilder;
    using Thread = System.Threading.Thread;

    /// <summary>
    /// This class shall deal with all things related to converting files
    /// it pretty much runs the XslCompiledTransform object to do this
    /// </summary>
    public class XsltConverter
    {
        /// <summary>
        /// PathRoot holds the path of the root of the Normalizer work directory
        /// </summary>
        public string PathRoot = ConfigurationManager.AppSettings["dirPathRoot"];

        /// <summary>
        /// JobName holds the name of the job we are to convert. Job = directory and XSLT-files
        /// </summary>
        public string JobName = string.Empty;

        /// <summary>
        /// FileXSL holds the name of the XSLT-file we are to use to convert the incomming document
        /// </summary>
        public string FileXsl = string.Empty;

        /// <summary>
        /// FileOutput holds the name of the file when converted by Normalizer
        /// </summary>
        public string FileOutput = string.Empty;

        /// <summary>
        /// Hashtable of paths where the transformed articles are to be put
        /// </summary>
        public Dictionary<int, string> OutputPaths = new Dictionary<int, string>();

        /// <summary>
        /// Dictionary containing encodings, these shall be mapped against output path
        /// </summary>
        public Dictionary<int, string> OutputEncodings = new Dictionary<int, string>();

        /// <summary>
        /// Dictoinary containing extensions for the file
        /// </summary>
        public Dictionary<int, string> OutputExtensions = new Dictionary<int, string>();

        /// <summary>
        /// Dictionary holding the document extention which so defines the type of document we are to create
        /// </summary>
        public Dictionary<int, string> OutputDocumentExtension = new Dictionary<int, string>();

        /// <summary>
        /// The done directory.
        /// </summary>
        public string DoneDirectory = ConfigurationManager.AppSettings["dirPathRoot"] + @"\done";

        /// <summary>
        /// The busy flag.
        /// </summary>
        public bool BusyFlag = false;

        /// <summary>
        /// The configuration.
        /// </summary>
        public Configuration Configuration = new Configuration();

        /// <summary>
        /// The iptc sequence number.
        /// </summary>
        public bool IptcSequenceNumber;

        /// <summary>
        /// The iptc sequence number file.
        /// </summary>
        public string IptcSequenceNumberFile;

        /// <summary>
        /// The processor.
        /// </summary>
        protected Processor Processor = new Processor();

        /// <summary>
        /// Creating the logger object so we can log stuff
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(XsltConverter));

        /// <summary>
        /// Creating the error logger object so we can separate errors in a separate log file
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        /// The _stop signal.
        /// </summary>
        private bool _stopSignal;

        /// <summary>
        /// Gets or sets the iptc sequence number min.
        /// </summary>
        public int IptcSequenceNumberMin { get; set; }

        /// <summary>
        /// Gets or sets the iptc sequence number max.
        /// </summary>
        public int IptcSequenceNumberMax { get; set; }

        /// <summary>
        /// Gets or sets the value of the PathError public string. It holds the path to the directory where we are to put files if they fail to be converted
        /// </summary>
        /// <remarks>
        /// This has been refactored from public string PathError = ConfigurationManager.AppSettings["dirErrorPath"];
        /// </remarks>
        public string PathError { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether stop signal.
        /// </summary>
        public bool StopSignal
        {
            get
            {
                return _stopSignal;
            }

            set
            {
                _stopSignal = value;
                if (_stopSignal)
                {
                    BusyFlag = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets the iptc sequence value.
        /// </summary>
        protected int IptcSequenceValue { get; set; }

        /// <summary>
        /// This method uses predefined sets and convert the XML-documents using Altova XML library
        /// </summary>
        /// <param name="xmlFile">
        /// The xml File.
        /// </param>
        /// <returns>
        /// Returns true if success, false if failure
        /// </returns>
        public string XslConverter(string xmlFile)
        {
            Processor.SetProperty(net.sf.saxon.lib.FeatureKeys.GENERATE_BYTE_CODE, "false");

            // Getting the XSLT-sheet to be used

            // Create a compiler
            XsltCompiler compiler = Processor.NewXsltCompiler();

            // Debug information
            Logger.Debug("This is FileXSL: " + FileXsl);

            if (FileXsl == string.Empty)
            {
                Logger.Info("No XSLT document provided!");
                return string.Empty;
            }

            try
            {
                compiler.ErrorList = new ArrayList();

                // Compile the stylesheet
                Logger.Debug("Compile XSLT-document: " + FileXsl);
                FileInfo fi = new FileInfo(FileXsl);
                if (!fi.Exists)
                {
                    Logger.Info("Provided XSLT document does not exist!");
                    return string.Empty;
                }

                // For security reasons DTD is prohibited in this XML document. To enable DTD processing set the ProhibitDtd property on XmlReaderSettings to false and pass the settings into XmlReader.Create method.
                // Error reported by XML parser: 
                // For security reasons DTD is prohibited in this XML document. 
                // To enable DTD processing set the DtdProcessing property on XmlReaderSettings to 
                // Parse and pass the settings into XmlReader.Create method
                XsltTransformer transformer = compiler.Compile(new Uri(FileXsl)).Load();
                Processor.SetProperty("http://saxon.sf.net/feature/trace-external-functions", "true");
                
                // @consider: Maybe we should move this out of the convert and instead loop files outside this class
                // We are checking that we've not received a stop signal. If we have, we shall exit this method
                if (_stopSignal)
                {
                    BusyFlag = false;
                    Logger.Info("We have received a stop signal and so we stop this batch");
                }

                // The file is the path + filename, so this is good
                fi = new FileInfo(xmlFile) { IsReadOnly = false };

                // Logging information
                Logger.Info("Start converting file " + fi.FullName);

                string doneFullName = SetDoneFullName(fi);

                // Checking if we can use the file yet
                Logger.Info("Checking if we can use the file yet");

                CheckAccessableFile(fi);

                Logger.Debug("using XmlReader to read the file " + fi.FullName);

                // Get the file to validate
                Logger.Debug("checking if we have a valid XML-file");

                var readerSettings = new XmlReaderSettings
                {
                    DtdProcessing = DtdProcessing.Parse

                };
                using (XmlReader reader = XmlReader.Create(fi.FullName, readerSettings))
                {
                    // reading the content of the file and storing it inside an XdmNode
                    
                    XdmNode input = Processor.NewDocumentBuilder().Build(reader);

                    // Run the transformation
                    Logger.Info("Run the transformation");
                    transformer.InitialContextNode = input;

                    // Creating xdmResult Object
                    Logger.Debug("Creating a xdmResult Object");
                    XdmDestination xdmResult = new XdmDestination();

                    // @done: We need to find out if we are to create an IPTC-sequence-number or not
                    if (IptcSequenceNumber)
                    {
                        GetIptcSequenceNumber(transformer);
                    }

                    // Now we are running the transformer
                    StringWriter sw = new StringWriter();

                    var serializer = new Serializer();
                    serializer.SetOutputWriter(sw);

                    transformer.Run(serializer);

                    // Giving it some settings
                    XmlWriterSettings xws = new XmlWriterSettings
                                                {
                                                    Indent = true, 
                                                    IndentChars = " ", 
                                                    Encoding = Encoding.GetEncoding("iso-8859-1")
                                                };

                    // Putting content of transformation into a string builder
                    // StringBuilder sb = new StringBuilder();
                    
                    
                    //using (XmlWriter xmlWriter = XmlWriter.Create(sw, xws))
                    //{
                    //    sw.Write(xmlWriter);
                    //    // xdmResult.XdmNode.WriteTo(xmlWriter);
                    //    xmlWriter.Flush();
                    //    xmlWriter.Close();
                    //}

                    // Moving converted file to done folder
                    Logger.Info("Moving file to Done folder");

                    if (File.Exists(doneFullName))
                    {
                        // In case the done file already exists, we'll delete the file
                        File.Delete(doneFullName);
                    }

                    if (!fi.Exists)
                    {
                        return string.Empty;
                    }

                    try
                    {
                        Logger.Info("Moving file to " + doneFullName);
                        fi.MoveTo(doneFullName);
                        fi.IsReadOnly = false;
                        Logger.Info("File moved to " + doneFullName);
                    }
                    catch (IOException io)
                    {
                        // We could try and wait a little moment and see if we could move it then
                        Thread.Sleep(50);
                        try
                        {
                            fi.MoveTo(doneFullName);
                            fi.IsReadOnly = false;
                            Logger.Info("File moved to " + doneFullName);
                        }
                        catch (IOException ioe)
                        {
                            Logger.Error("An error occuredt. Check error log for more information");
                            ErrorLogger.Error(
                                "There was an error while trying to move the converted file to the done folder", ioe);
                            ErrorLogger.Error(ioe.Message);
                            ErrorLogger.Error(ioe.StackTrace);

                            MoveErrorFile(xmlFile);
                        }

                        ErrorLogger.Error(io.Message);
                        ErrorLogger.Error(io.StackTrace);

                        MoveErrorFile(xmlFile);
                    }

                    return sw.ToString();
                }
            }
            catch (InvalidOperationException exception)
            {
                Logger.Error("An error occured. Check error log for more information");
                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                Logger.Info("Moving file " + xmlFile + " to error folder");

                MoveErrorFile(xmlFile);

                return string.Empty;
            }
            catch (DynamicError de)
            {
                Logger.Error("An error occured. Check error log for more information");
                ErrorLogger.Error(de);

                ErrorLogger.Info("Moving file " + xmlFile + " to error folder");

                MoveErrorFile(xmlFile);
            }
            catch (StaticError se)
            {
                Logger.Error("An error has occured. Check error log for more information");
                ErrorLogger.Error(se);

                Logger.Info("Moving file " + xmlFile + " to error folder");

                MoveErrorFile(xmlFile);
            }
            catch (IndexOutOfBoundsException jliexception)
            {
                Logger.Error("An error has occured. Check error log for more information!");
                ErrorLogger.Error(jliexception.Message);
                ErrorLogger.Error(jliexception.StackTrace);
                
                if (jliexception.InnerException != null)
                {
                    ErrorLogger.Error(jliexception.InnerException.Message);
                    ErrorLogger.Error(jliexception.InnerException.StackTrace);
                }

                Logger.Info("Moving file " + xmlFile + " to error folder");

                MoveErrorFile(xmlFile);

                return string.Empty;
            }
            catch (IOException
                xdme)
            {
                Logger.Error("An error has occured. Check error log for more information.");
                // Just done this so that I don't get any error messages
                ErrorLogger.Error(xdme.Message);
                ErrorLogger.Error(xdme.StackTrace);

                if (xdme.InnerException != null)
                {
                    ErrorLogger.Error(xdme.InnerException.Message);
                    ErrorLogger.Error(xdme.InnerException.StackTrace);
                }


                // We need to move the file to the error folder
                // And then we need to break!

                // MOVING ERROR FILE
                Logger.Error("Moving file to error folder");
                MoveErrorFile(xmlFile);
                return string.Empty;
            }
            catch (Exception e)
            {
                Logger.Error("error: ", e);
                if (e.Message == "Root element is missing.")
                {
                    // Logger.Error(" File: " + fi.FullName + " was empty, or it was not well formed in some way");
                    ErrorLogger.Error(e.Message);
                    ErrorLogger.Error(e.StackTrace);
                }

                
                ErrorLogger.Error(e.Message);
                ErrorLogger.Error(e.StackTrace);

                // Moving the file that created the error to the error folder
                MoveErrorFile(xmlFile);
                return string.Empty;
            }

            return string.Empty;
        }

        private string SetDoneFullName(FileInfo fi)
        {
            // We are creating the path to Done-folder by removing xslt
            string doneFullName = PathRoot + @"\done\" + JobName + @"\" + fi.Name;
            return doneFullName;
        }

        /// <summary>
        /// CREATING NEW FILE
        /// Now doing the actuall creating of the new file 
        /// That is if the document does not have a result-document tag,
        /// then we are only creating the file to be saved in the output/InstanceName folder
        /// </summary>
        /// <param name="result">
        /// Holds the content of the transformated file 
        /// </param>
        /// <param name="xmlFile">
        /// Holds the name of the xml file we have converted
        /// </param>
        public void CreateNewFile(string result, string xmlFile)
        {
            result = result.Trim();
            result = result.Replace("\0", string.Empty);

            if (result != string.Empty)
            {
                Logger.Debug("--------------------- COUNTING FILE ENCODING ARRAY ---------------------------");

                foreach (KeyValuePair<int, string> entry in OutputEncodings)
                {
                    // We now also need to get the corresponding key in the outputpath/directory
                    Logger.Debug("Directory - key: " + entry.Key + ", value: " + OutputPaths[entry.Key]);

                    CheckDirectoryExists(entry);

                    Logger.Debug("Encoding - key: " + entry.Key + ", value: " + entry.Value);
                    string stringEncoding = entry.Value.ToLower() == string.Empty ? "iso-8859-1" : entry.Value.ToLower();

                    Encoding encEncoding = Encoding.GetEncoding(stringEncoding);

                    FileInfo fi = new FileInfo(xmlFile);
                    string filename = fi.Name;
                    string strXmlFileout = OutputPaths[entry.Key] + @"\" + filename;

                    // Adding support for .html and .xhtml
                    if (OutputDocumentExtension[entry.Key].Length > 5)
                    {
                        Logger.Debug("For some reason the outputdocument-extention is longer than 4");
                    }

                    if (OutputDocumentExtension[entry.Key] != string.Empty)
                    {
                        strXmlFileout = strXmlFileout.Replace(".xml", "." + OutputDocumentExtension[entry.Key]);
                    }

                    if (strXmlFileout == string.Empty)
                    {
                        DateTime dt = DateTime.Today;

                        string createDateTime = dt.Year + dt.Month + dt.Day + "T" + dt.Hour + dt.Minute + dt.Second;
                        string fileExtension = ".xml";
                        if (OutputDocumentExtension[entry.Key] != string.Empty)
                        {
                            fileExtension = "." + OutputDocumentExtension[entry.Key];
                        }

                        strXmlFileout = "NTB-" + createDateTime + fileExtension;
                    }

                    // Writing out debug informations
                    Logger.Info("Writing converted file to " + strXmlFileout);

                    Logger.Debug("Writing file using XmlDocument and XmlWriter");

                    XmlDocument doc = new XmlDocument();

                    doc.LoadXml(result);

                    XmlWriterSettings writerSettings = new XmlWriterSettings
                                                           {
                                                               Indent = true, 
                                                               OmitXmlDeclaration = false, 
                                                               Encoding = encEncoding
                                                               
                                                               // Encoding.GetEncoding("iso-8859-1") // This has been changed while in documentation mode
                                                           };

                    using (XmlWriter writer = XmlWriter.Create(strXmlFileout, writerSettings))
                    {
                        doc.Save(writer);
                    }
                }

                Logger.Debug("--- END LOOPING ---");

                // Setting some bool values that can be read by some other class
                BusyFlag = false;
            }
            else
            {
                // We are sending an e-mail as well, if we are supposed to that is
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]))
                {
                    Logger.Debug("We must wait two seconds before we can move the file to the error folder");
                }

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendErrorEmail"]))
                {
                    Notifyer n = new Notifyer();

                    try
                    {
                        n.PathError = PathError;
                        n.FileError = xmlFile;
                    }
                    catch (Exception exception)
                    {
                        Logger.Error("There was an error while trying to move the file. Check the error log for more information");
                        ErrorLogger.Error(exception.Message);
                        ErrorLogger.Error(exception.StackTrace);

                        if (exception.InnerException != null)
                        {
                            ErrorLogger.Error(exception.InnerException.Message);
                            ErrorLogger.Error(exception.InnerException.StackTrace);
                        }
                    }
                }

                // Moving to error folder
                MoveErrorFile(xmlFile);
            }
        }

        /// <summary>
        /// The create mail notification.
        /// </summary>
        /// <param name="errorPath">
        /// The error path.
        /// </param>
        /// <param name="errorMessage">
        /// The error message.
        /// </param>
        /// <param name="errorFile">
        /// The error file.
        /// </param>
        /// <param name="htmlBody">
        /// The html body.
        /// </param>
        public void CreateMailNotification(
            string errorPath, string errorMessage, string errorFile, bool htmlBody = false)
        {
            Notifyer n = new Notifyer
                             {
                                 PathError = errorPath, 
                                 LastErrorMessage = errorMessage, 
                                 FileError = errorFile, 
                                 HTMLBody = htmlBody
                             };

            n.SendMail();
        }

        /// <summary>
        /// The is valid xml.
        /// </summary>
        /// <param name="xmlFile">
        /// The xml file.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsValidXml(string xmlFile)
        {
            using (XmlTextReader xmlTextReader = new XmlTextReader(xmlFile))
            {
                try
                {
                    Logger.Debug("Trying to read the document");
                    while (xmlTextReader.Read())
                    {
                        // Logging inside here isn't a good idea :) 
                        // logger.Debug(xmlTextReader.Name.ToString());
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("Cannot read the document!");

                    ErrorLogger.Error(exception.Message);
                    ErrorLogger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        ErrorLogger.Error(exception.InnerException.Message);
                        ErrorLogger.Error(exception.InnerException.StackTrace);
                    }
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// The move error file.
        /// </summary>
        /// <param name="xmlErrorFile">
        /// The xml error file.
        /// </param>
        public void MoveErrorFile(string xmlErrorFile)
        {
            // Waiting four seconds before we can move the file
            Thread.Sleep(4000);

            FileInfo xmlFile = new FileInfo(xmlErrorFile);

            string strErrorPath = PathError + @"\" + xmlFile.Name;
            Logger.Debug("moving error file: " + xmlErrorFile + " to " + strErrorPath);
            if (!File.Exists(strErrorPath))
            {
                try
                {
                    xmlFile.MoveTo(strErrorPath);

                    Logger.Info("File was moved to " + strErrorPath);
                }
                catch (Exception exception)
                {
                    Logger.Error("Something happened while moving the file.");
                    ErrorLogger.Error(exception.Message);
                    ErrorLogger.Error(exception.StackTrace);
                }
                finally
                {
                    // This does not look to clever... really
                    //if (xmlFile.Exists)
                    //{
                    //    Logger.Error("We're deleting the file since it exists and since we've moved it already");
                    //    xmlFile.Delete();
                    //}
                    
                }
                return;
            }
            Logger.Info("Checking to see if we are to delete some file or not");

            try
            {
                File.Delete(strErrorPath);

                xmlFile.MoveTo(strErrorPath);

                Logger.Info("File was moved to " + strErrorPath);
            }
            catch (Exception e)
            {
                Logger.Error("Something happened while moving the file");
                ErrorLogger.Error(e.Message);
                ErrorLogger.Error(e.StackTrace);

                Logger.Error("We're deleting the file");
                if (!xmlFile.Exists)
                {
                    Logger.Info("The file does not exists. Cannot delete the file");
                }

                // Deleting file so we can get rid of it 
                xmlFile.Delete();
            }
        }

        /// <summary>
        /// The is file accessable.
        /// </summary>
        /// <param name="filename">
        /// The filename.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsFileAccessable(string filename)
        {
            try
            {
                using (FileStream fs = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    fs.Close();
                }
            }
            catch (IOException ioexception)
            {
                Logger.Debug(ioexception);
                return false;
            }

            return true;
        }

        /// <summary>
        /// The check directory exists.
        /// </summary>
        /// <param name="entry">
        /// The entry.
        /// </param>
        private void CheckDirectoryExists(KeyValuePair<int, string> entry)
        {
            DirectoryInfo di = new DirectoryInfo(OutputPaths[entry.Key]);
            if (!di.Exists)
            {
                di.Create();
            }
        }

        /// <summary>
        /// The check accessable file.
        /// </summary>
        /// <param name="fi">
        /// The fi.
        /// </param>
        /// <exception cref="Exception">
        /// Throws an exception if we cannot access the file
        /// </exception>
        private void CheckAccessableFile(FileInfo fi)
        {
            int accessCounter = 0;
            while (IsFileAccessable(fi.FullName) == false)
            {
                Thread.Sleep(1500);
                accessCounter++;
                if (accessCounter == 5)
                {
                    throw new Exception("Cannot access file. Is being used by some other instance!");
                }

                Logger.Debug("We are looping and waiting to see if we can get access to the file!");
                Logger.Debug("Attempt: " + accessCounter);
            }
        }

        /// <summary>
        /// The get iptc sequence number.
        /// </summary>
        /// <param name="transformer">
        /// The transformer.
        /// </param>
        private void GetIptcSequenceNumber(XsltTransformer transformer)
        {
            FileInfo fileInfo = new FileInfo(IptcSequenceNumberFile);
            if (!fileInfo.Exists)
            {
                return;
            }

            // Now we have to read the value of the Sequence number
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(IptcSequenceNumberFile);
            XmlNode node = xmlDocument.SelectSingleNode("/Ntb/Iptc/SequenceNumber");
            DateTime dt = new DateTime();
            if (node != null && node.Attributes != null)
            {
                dt = Convert.ToDateTime(node.Attributes["date"].Value);

                Logger.Debug("Value of the date attribute: " + dt);
            }

            // Logger.Debug("Value of the node SequenceNumber: " + node.InnerText);
            if (node != null)
            {
                Logger.Debug("Value of the node SequenceNumber: " + node.InnerText);
                IptcSequenceValue = Convert.ToInt32(node.InnerText);
            }

            if (dt.Date < DateTime.Today)
            {
                IptcSequenceValue = IptcSequenceNumberMin;
            }

            QName qname = new QName("IptcSequenceNumber");

            XdmAtomicValue xdmAtomicValue = new XdmAtomicValue(IptcSequenceValue);
            XdmValue xdmValue = new XdmValue(xdmAtomicValue);
            transformer.SetParameter(qname, xdmValue);

            // And now we have to save the file
            IptcSequenceValue += 1;

            // We check if the iptc-sequence number is bigger or equal to max value
            // This means: If range is from 9000 to 10000, we are actually using values 9000-9999
            if (IptcSequenceValue >= IptcSequenceNumberMax)
            {
                // It was, so we are setting it to the min value
                IptcSequenceValue = IptcSequenceNumberMin;
            }

            // Setting the new value
            if (node != null)
            {
                node.InnerText = IptcSequenceValue.ToString();
                if (node.Attributes != null)
                {
                    XmlAttribute xmlAttribute = node.Attributes["date"];
                    if (xmlAttribute != null)
                    {
                        xmlAttribute.Value = DateTime.Today.ToShortDateString();
                    }
                }
            }

            // Saving the document
            xmlDocument.Save(IptcSequenceNumberFile);
        }
    }
}