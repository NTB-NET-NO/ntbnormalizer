﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

// XML Stuff
using System.Xml;

// Logging
using log4net;

using System.Configuration;
using System.IO;
using NTB.Normalizer.Components.enums;
using NTB.Normalizer.Components.interfaces;
using NTB.Normalizer.Utilities;
using System.Threading;

namespace NTB.Normalizer.Components
{
    public partial class CsvComponent : Component, IBaseNormComponents
    {
        #region Logging
        /// <summary>
        /// Static logger
        /// </summary>
        static readonly ILog Logger = LogManager.GetLogger(typeof(CsvComponent));

        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        #endregion

        #region Common class variables
        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);

        /// <summary>
        /// The configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected String Name;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly configured
        /// </summary>
        /// <remarks>Holds the configured status of the instance. <c>True</c> means successfully configured</remarks>
        protected Boolean Configured = false;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected Boolean ErrorRetry = false;

        /// <summary>
        /// PollDelay is used to make sure that the file is ready to be accessed. 
        /// </summary>
        protected int PollDelay = 0;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected String EmailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected String EmailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected String EmailBody;

        #endregion

        #region Polling settings

        /// <summary>
        /// The configured schedule for this instance
        /// </summary>
        /// <remarks>This is set to a time of day and is used by the <c>Scheduled</c> pollstyle. Work is triggered at the given time of day.</remarks>
        protected String Schedule;

        /// <summary>
        /// The configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;



        #endregion

        #region File folders

        /// <summary>
        /// Input file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter"/> and <see cref="IncludeSubdirs"/>
        /// </remarks>
        protected String FileInputFolder;

        /// <summary>
        /// Output file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter"/> and <see cref="IncludeSubdirs"/>
        /// </remarks>
        protected Dictionary<int, string> FileOutputFolders = new Dictionary<int, string>();

        /// <summary>
        /// Error file folder
        /// </summary>
        /// <remarks>
        ///   <para>File folder where failing files are stored.</para>
        ///   <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        protected String FileErrorFolder;

        /// <summary>
        /// File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///   <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///   <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        protected String FileDoneFolder;

        #endregion

        #region Optional configuration

        /// <summary>
        /// Input file filter
        /// </summary>
        /// <remarks>
        ///   <para>Defines what file types are being read from <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>NTBIDType.None</c></para>
        /// </remarks>
        protected String FileFilter = "*.csv";

        /// <summary>
        /// Watch subdirs for new files as well
        /// </summary>
        /// <remarks>
        ///   <para>Defines if the instance should look in subdirectories within <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>False</c></para>
        /// </remarks>
        protected Boolean IncludeSubdirs = false;

        /// <summary>
        /// Buffer size for the file watcher
        /// </summary>
        /// <remarks>
        ///   <para>Defines the internal buffer size the FileSystemWatcher when using this polling.
        /// Should not be set to high. The instance will fall back to contious polling on buffer overflows.</para>
        ///   <para>Default value: <c>4096</c></para>
        /// </remarks>
        protected int BufferSize = 4096;

        /// <summary>
        /// Flag to say if this component is working or not. I have a feeling we have another parameter for this though.
        /// </summary>
        protected bool BusyFlag = false;

        /// <summary>
        /// Dictionary to map ID to encodings, which we can use to say which type of encoding the output file shall have
        /// </summary>
        protected Dictionary<int, string> DocumentEncodings = new Dictionary<int, string>();

        /// <summary>
        /// Dictionary to map ID to file extensions, which we can use to say which type of encoding the output file shall have
        /// </summary>
        protected Dictionary<int, string> DocumentExtensions = new Dictionary<int, string>();




        #endregion

        #region Component Related
        /// <summary>
        /// Gets the enabled status of the instance.
        /// </summary>
        /// <value>The enabled status.</value>
        /// <remarks><c>True</c> if the job instance is enabled.</remarks>
        public Boolean Enabled
        {
            get { return enabled; }
        }

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The configured instance job name.</remarks>
        public String InstanceName
        {
            get { return Name; }
        }

        public ComponentState ComponentState
        {
            get { return componentState; }
        }

        /// <summary>
        /// Gets the operation mode. 
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Converter</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get { return OperationMode.Converter; }
        }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Converter</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get { return pollStyle; }
        }

        #endregion

        #region NormalizerComponents Specific

        /// <summary>
        ///     Creating the XsltConverter object
        /// </summary>
        public XsltConverter Converter = new XsltConverter();

        /// <summary>
        /// Variable used to tell if there is a convertion in the job that has started
        /// </summary>

        #endregion

        public CsvComponent()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();


            InitializeComponent();
        }

        public CsvComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void Configure(XmlNode configNode)
        {
            Logger.Debug("Node: " + configNode.Name);
            if (configNode.Attributes != null)
            {
                Logger.Debug("Name attribut: " + configNode.Attributes.GetNamedItem("Name"));

                // Basic configuration sanity check
                if (configNode.Attributes != null && (configNode.Name != "CSVComponent" || configNode.Attributes.GetNamedItem("Name") == null))
                {
                    throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
                }

                #region Basic config

                // Getting the name of this Component instance
                try
                {
                    Name = configNode.Attributes["Name"].Value;
                    enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                    
                    ThreadContext.Stacks["NDC"].Push(InstanceName);

                    FieldNames = Convert.ToBoolean(configNode.Attributes["HasFieldNames"].Value);
                    FieldSeparator = configNode.Attributes["FieldSeparator"].Value;
                    PollDelay = Convert.ToInt32(configNode.Attributes["Delay"].Value);
                }
                catch (Exception ex)
                {
                    Logger.Fatal("Not possible to configure this job instance!");
                    ErrorLogger.Error(ex.Message);
                    ErrorLogger.Error(ex.StackTrace);

                    if (ex.InnerException != null)
                    {
                        ErrorLogger.Error(ex.InnerException.Message);
                        ErrorLogger.Error(ex.InnerException.StackTrace);
                    }
                }

                //Basic operation
                try
                {
                    pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
                }
            }

            #endregion


            try
            {
                //Find the file folder to work with

                XmlNode folderNode = configNode.SelectSingleNode("CSVInputFolder");

                if (folderNode != null)
                {
                    FileInputFolder = folderNode.InnerText;

                }

                Logger.InfoFormat("Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}",
                        Enum.GetName(typeof(PollStyle), pollStyle), FileInputFolder, enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder", ex);
            }

            try
            {
                //Find the file folder to work with

                XmlNode folderNode = configNode.SelectSingleNode("CSVDoneFolder");

                if (folderNode != null)
                {
                    FileDoneFolder = folderNode.InnerText;

                }

                Logger.InfoFormat("Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}",
                        Enum.GetName(typeof(PollStyle), pollStyle), FileInputFolder, enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder", ex);
            }

            try
            {
                //Find the file folder to work with

                XmlNode folderNode = configNode.SelectSingleNode("CSVErrorFolder");

                if (folderNode != null)
                {
                    FileErrorFolder = folderNode.InnerText;
                }

                Logger.InfoFormat("Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}",
                        Enum.GetName(typeof(PollStyle), pollStyle), FileInputFolder, enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder", ex);
            }

            try
            {
                //Find the file folder to work with
                XmlNodeList folderNodes = configNode.SelectNodes("CSVOutputFolders/CSVOutputFolder");
                if (folderNodes != null)
                    foreach (XmlNode folderNode in folderNodes)
                    {
                        // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                        if (folderNode != null)
                        {
                            if (folderNode.Attributes != null)
                            {
                                FileOutputFolders.Add(Convert.ToInt32(folderNode.Attributes["Id"].Value), folderNode.InnerText);
                                DocumentEncodings.Add(Convert.ToInt32(folderNode.Attributes["Id"].Value),
                                                      folderNode.Attributes["Encoding"] != null
                                                          ? folderNode.Attributes["Encoding"].Value
                                                          : "iso-8859-1");


                                DocumentExtensions.Add(Convert.ToInt32(folderNode.Attributes["Id"].Value),
                                                       folderNode.Attributes["Extention"] != null
                                                           ? folderNode.Attributes["Extention"].Value
                                                           : "xml");
                            }
                        }

                    }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder");

                ErrorLogger.Error(ex.Message);
                ErrorLogger.Error(ex.StackTrace);

                if (ex.InnerException != null)
                {
                    ErrorLogger.Error(ex.InnerException.Message);
                    ErrorLogger.Error(ex.InnerException.StackTrace);
                }
            }

            if (enabled)
            {
                #region File folders to access
                // Checking if file folders exists
                string fileOutputFolder = "";

                try
                {
                    if (!Directory.Exists(FileInputFolder))
                    {
                        Directory.CreateDirectory(FileInputFolder);
                    }

                    if (!Directory.Exists(FileDoneFolder))
                    {
                        Directory.CreateDirectory(FileDoneFolder);
                    }

                    if (!Directory.Exists(FileErrorFolder))
                    {
                        Directory.CreateDirectory(FileErrorFolder);
                    }

                    foreach (KeyValuePair<int, string> kvp in FileOutputFolders)
                    {
                        fileOutputFolder = kvp.Value;
                        if (!Directory.Exists(fileOutputFolder))
                        {
                            Directory.CreateDirectory(fileOutputFolder);
                        }
                    }

                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid, unknown or missing file folder: " + fileOutputFolder, ex);
                }

                #endregion



                #region Set up polling
                try
                {
                    //Switch on pollstyle
                    switch (pollStyle)
                    {
                        case PollStyle.Continous:
                            Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
                            if (Interval == 0)
                            {
                                Interval = 5000;
                            }
                            pollTimer.Interval = Interval * 1000; // Value * 1000 to get seconds
                            Logger.DebugFormat("Poll interval: {0} seconds", Interval);

                            break;



                        case PollStyle.Scheduled:
                            /* 
                            schedule = configNode.Attributes["Schedule"].Value;
                            TimeSpan s = Utilities.GetScheduleInterval(schedule);
                            logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                            pollTimer.Interval = s.TotalMilliseconds;
                            */
                            throw new NotSupportedException("Invalid polling style for this job type");


                        case PollStyle.FileSystemWatch:
                            //Check for config overrides

                            if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                                BufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                            Logger.DebugFormat("FileSystemWatcher buffer size: {0}", BufferSize);

                            // Set values
                            filesWatcher.Path = FileInputFolder;
                            filesWatcher.Filter = FileFilter;
                            filesWatcher.IncludeSubdirectories = IncludeSubdirs;
                            filesWatcher.InternalBufferSize = BufferSize;

                            //Do not start the event watcher here, wait until after the startup cleaning job
                            pollTimer.Interval = 5000; // short startup;

                            break;

                        default:
                            //Unsupported pollstyle for this object
                            throw new NotSupportedException("Invalid polling style for this job type");
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
                }
                #endregion
            }
            //Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            Configured = true;
        }

        public void Start()
        {
            if (!Configured)
            {
                throw new NormalizerException("CSVComponent is not properly configured. CSVComponent::Start() Aborted!");
            }

            if (!enabled)
            {
                throw new NormalizerException("CSVComponent is not enabled. CSVSComponent::Start() Aborted!");
            }

            Logger.Info("Starting" + InstanceName);

            _stopEvent.Reset();
            _busyEvent.Set();

            componentState = ComponentState.Running;
            pollTimer.Start();
        }

        public void Stop()
        {
            if (!Configured)
            {
                throw new NormalizerException("CSVComponent is not properly configured. CSVComponent::Stop() Aborted");
            }

            if (!enabled)
            {
                throw new NormalizerException("CSVComponent is not properly configured. CSVComponent::Stop() Aborted");
            }


            const double epsilon = 0;
            if (!pollTimer.Enabled && (ErrorRetry || pollStyle !=
                PollStyle.FileSystemWatch || Math.Abs(pollTimer.Interval - 5000) < epsilon))
            {
                Logger.InfoFormat("Waiting for instance to complete work. Job {0}", InstanceName);
            }

            // Signal Stop
            _stopEvent.Set();

            // Stop events
            filesWatcher.EnableRaisingEvents = false;

            if (!_busyEvent.WaitOne(30000))
            {
                Logger.InfoFormat("Instance did not complete properly. Data may be inconsistent. Job: {0}", InstanceName);
            }

            componentState = ComponentState.Halted;
            // Kill polling
            pollTimer.Stop();
        }



        private void pollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = InstanceName;
            Logger.DebugFormat("CSVComponent::pollTimer_Elapsed() hit");

            //Reset flags and pollers
            _busyEvent.WaitOne();
            pollTimer.Stop();

            try
            {
                Logger.Debug("InputFolder for " + InstanceName + " : " + FileInputFolder);

                //Process any wating files
                List<String> files = new List<string>(Directory.GetFiles(FileInputFolder,
                    FileFilter, (IncludeSubdirs ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)));

                //Some logging
                Logger.DebugFormat("CSVComponent::pollTimer_Elapsed() Convertion items scheduled: {0}", files.Count);

                // I need to have an Debug-flag here
                if (ConfigurationManager.AppSettings["Debug"].ToUpper() == "TRUE")
                {
                    foreach (string stringFiles in files)
                    {
                        Logger.Debug("Files to be converted are: " + stringFiles);
                    }
                }

                // doing the pollstyle.Continous
                if (pollStyle == PollStyle.Continous && !ErrorRetry)
                {
                    // Now we are to convert the selected files
                    CreateConverter(files);
                }

                // Enable the event listening
                if (pollStyle == PollStyle.FileSystemWatch && !ErrorRetry)
                {
                    Logger.Debug("Setting filesWatcher.EnableRaisingEvents to true");
                    filesWatcher.EnableRaisingEvents = true;
                }

                // Process the files


            }
            catch (Exception ex)
            {
                Logger.Error("CSVComponent::pollTimer_Elapsed(). Check error log for more information");
                ErrorLogger.Error(ex.Message);
                ErrorLogger.Error(ex.StackTrace);

                if (ex.InnerException != null)
                {
                    ErrorLogger.Error(ex.InnerException.Message);
                    ErrorLogger.Error(ex.InnerException.StackTrace);
                }
            }


            //Check error state
            if (ErrorRetry)
            {
                filesWatcher.EnableRaisingEvents = false;
            }

            //Restart
            pollTimer.Interval = Interval * 1000;
            if (pollStyle != PollStyle.FileSystemWatch || (pollStyle == PollStyle.FileSystemWatch && filesWatcher.EnableRaisingEvents == false))
                pollTimer.Start();

            _busyEvent.Set();
        }

        private void CreateConverter(List<string> files)
        {
            
            try
            {
                CsvConverter myCsvConverter = new CsvConverter
                    {
                        FieldSeparatorString = FieldSeparator,
                        Files = files,
                        OutputFolders = FileOutputFolders,
                        DoneFolder = FileDoneFolder,
                        PathError = FileErrorFolder
                    };
                myCsvConverter.ParseCsvFiles();
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

            }
        }


        private void filesWatcher_Created(object sender, FileSystemEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = InstanceName;
            Logger.DebugFormat("CSVComponent::filesWatcher_Created() hit with file data: {0}", e.FullPath);

            int pollTime = PollDelay * 1000;
            Logger.Debug("We are waiting: " + pollTime.ToString() + " Seconds before starting to convert!");


            if (PollDelay > 0)
            {
                // We take the value and multiply with thousand - to get the seconds... 
                Thread.Sleep(pollTime);
            }

            try
            {
                //Wait for the busy state
                _busyEvent.WaitOne();
                
                CreateConverter(new List<string> {e.FullPath});
                
                //Reset event
                _busyEvent.Set();
            }
            catch (Exception ex)
            {
                Logger.Debug("Something happened while doing filesWatcher_Created!");
                ErrorLogger.Error(ex.Message);
                ErrorLogger.Error(ex.StackTrace);

                if (ex.InnerException != null)
                {
                    ErrorLogger.Error(ex.InnerException.Message);
                    ErrorLogger.Error(ex.InnerException.StackTrace);
                }

                // We must move the error file
                
                
            }
        }

        public string FieldSeparator { get; set; }

        public bool FieldNames { get; set; }
    }
}
