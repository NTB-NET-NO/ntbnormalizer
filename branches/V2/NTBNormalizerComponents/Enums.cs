﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.Normalizer.Components
{
    public enum ComponentState
    {
        /// <summary>
        /// This value is just used to tell if the component is in started modus (running)
        /// </summary>
        Running,

        /// <summary>
        /// this value is just used to tell if the component is in halted mode
        /// </summary>
        Halted
    }
    /// <summary>
    /// Used to specify the different operating modes. Primarily used for the common XSLT poller component.
    /// </summary>
    public enum OperationMode
    {
        /// <summary>
        /// Converter mode collects files from the input folders and converts them from 
        /// one XML-format to another
        /// </summary>
        Converter,

        /// <summary>
        /// Gatherer mode 
        /// </summary>
        Gatherer
    }

    /// <summary>
    /// Specifies the different polling styles available to the EWS poller component
    /// </summary>
    public enum PollStyle
    {
        /// <summary>
        /// Indicates continous polling og an item at a timed interval.
        /// This is the legacy pollingstyle and may be used for all job types
        /// </summary>
        Continous,
        /// <summary>
        /// Polling is limited to a time schedule, i.e. once a day/week
        /// </summary>
        Scheduled,
        /// <summary>
        /// Contious polling using an EWS folder subscription set.
        /// Allows for easier Exchange folder item checking. Only valid for EWS folder jobs.
        /// </summary>
        PullSubscribe,
        /// <summary>
        /// Notifications from EWS are actively sent to the client using a web service. Only valid for EWS folder jobs.
        /// </summary>
        PushSubscribe,
        /// <summary>
        /// Use File System Notifications with local disk folders instead of polling. Only valid for Gatherer jobs.
        /// </summary>
        FileSystemWatch,
        /// <summary>
        /// Polling happens at scheduled times of the day or once a week.
        /// </summary>
        CalendarPoll
    }
    /// <summary>
    /// Specifies the different days and which day is the first one. This so that we can support 
    /// scheduling on both day AND time
    /// </summary>

    public enum ConvertionMode
    {
        /// <summary>
        /// Convertion using XSLT version 1
        /// </summary>
        XSLT1,

        /// <summary>
        /// Conversion using XSLT version 2
        /// </summary>
        XSLT2,

        /// <summary>
        /// Conversion using Search and replace
        /// </summary>
        SearchReplace,

        /// <summary>
        /// Conversion from Comma Separated Values files
        /// </summary>
        CSV
    }

    public enum CalendarPollStyle
    {
        /// <summary>
        /// A daily CalendarPollStyle is a pullstyle that happens every day
        /// </summary>
        Daily,
        /// <summary>
        /// A daily CalendarPollStyle is a pullstyle that happens every week
        /// </summary>
        Weekly,
        /// <summary>
        /// A daily CalendarPollStyle is a pullstyle that happens every month
        /// </summary>
        Monthly,
        /// <summary>
        /// A daily CalendarPollStyle is a pullstyle that happens every year
        /// </summary>
        Yearly
    }

}
