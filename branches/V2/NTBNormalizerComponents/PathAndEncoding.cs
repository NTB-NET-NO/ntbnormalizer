﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using log4net;

namespace NormalizerComponents
{
    class PathAndEncoding
    {
        private Hashtable htJobs = new Hashtable();

        public PathAndEncoding(object jobName, Hashtable htPath, Hashtable htEncoding)
        {
            Hashtable htStuff = new Hashtable();
            htStuff.Add(htPath, htEncoding);

            // Adding to the htJobs hashtable
            htJobs.Add(jobName, htStuff);
        }

    }
}
