﻿using NTB.Normalizer.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace XsltFunctionsTest
{
    
    
    /// <summary>
    ///This is a test class for XsltConverterTest and is intended
    ///to contain all XsltConverterTest Unit Tests
    ///</summary>
    [TestClass()]
    public class XsltConverterTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SetDoneFullName
        ///</summary>
        [TestMethod()]
        [DeploymentItem("NormalizerComponents.dll")]
        public void SetDoneFullNameTest()
        {
            // string doneFullName = PathRoot + @"\done\" + JobName + @"\" + fi.Name;
            XsltConverter_Accessor target = new XsltConverter_Accessor {PathRoot = @"c:\test", JobName = "test"}; 
            FileInfo fi = new FileInfo("test.xml"); 

            const string expected = @"c:\test\done\test\test.xml"; 
            string actual = target.SetDoneFullName(fi);
            Assert.AreEqual(expected, actual);
        }
    }
}
