﻿namespace NTBNormalizerService
{
    partial class NTBNormalizerService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IntervalTimer = new System.Timers.Timer();
            this.serviceController1 = new System.ServiceProcess.ServiceController();
            this.NormalizerFileSystemWatcher = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.IntervalTimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NormalizerFileSystemWatcher)).BeginInit();
            // 
            // IntervalTimer
            // 
            this.IntervalTimer.Enabled = true;
            this.IntervalTimer.Interval = 15000D;
            this.IntervalTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.IntervalTimer_Elapsed);
            // 
            // NormalizerFileSystemWatcher
            // 
            this.NormalizerFileSystemWatcher.EnableRaisingEvents = true;
            this.NormalizerFileSystemWatcher.Filter = "*.xml";
            this.NormalizerFileSystemWatcher.IncludeSubdirectories = true;
            this.NormalizerFileSystemWatcher.Changed += new System.IO.FileSystemEventHandler(this.NormalizerFileSystemWatcher_Changed);
            // 
            // NTBNormalizerService
            // 
            this.ServiceName = "NTBNormalizerService";
            ((System.ComponentModel.ISupportInitialize)(this.IntervalTimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NormalizerFileSystemWatcher)).EndInit();

        }

        #endregion

        private System.Timers.Timer IntervalTimer;
        private System.ServiceProcess.ServiceController serviceController1;
        private System.IO.FileSystemWatcher NormalizerFileSystemWatcher;
    }
}
