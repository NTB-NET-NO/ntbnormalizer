﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("NTB.Normalizer.Service")]
[assembly: AssemblyDescription("A Service that is using XSLT to convert messages to NITF3.2 or other formats")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Norsk Telegrambyrå AS")]
[assembly: AssemblyProduct("NTB.Normalizer.Service")]
[assembly: AssemblyCopyright("Copyright © NTB 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("313a05c9-ab05-4a6f-b7f8-bd22a1d20578")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("XSLTFunctionsProject")]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("NTBNormalizerTestProject")]


//Watch log4net config
[assembly: log4net.Config.XmlConfigurator(Watch = true)]