﻿using System.ServiceProcess;
using System.Configuration;
using log4net;

namespace NTB.Normalizer.Service
{
    public class ServiceStart
    {
        public string DirLogPath = ConfigurationManager.AppSettings["dirLogPath"];
        public string DirRootPath = ConfigurationManager.AppSettings["dirPathRoot"];
        public string DirErrorPath = ConfigurationManager.AppSettings["dirErrorPath"];
        
        static readonly ILog Logger = LogManager.GetLogger(typeof(NtbNormalizerService));

        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            MDC.Set("JOBNAME", "NTB.Normalizer.Service");
            Logger.Debug("Starting up the application");


            ServiceBase[] servicesToRun = new ServiceBase[] 
                { 
                    new NtbNormalizerService() 
                };
            ServiceBase.Run(servicesToRun);
        }
    }
}
