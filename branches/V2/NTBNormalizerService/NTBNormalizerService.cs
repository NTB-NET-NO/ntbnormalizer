﻿using System;
using System.ServiceProcess;

// Creating support for logging
using log4net;

// Adding reference to components
using NTB.Normalizer.Components;


namespace NTB.Normalizer.Service
{
    public partial class NtbNormalizerService : ServiceBase
    {
        
        static ILog _logger = LogManager.GetLogger(typeof(NtbNormalizerService));

        protected MainServiceComponent Service = null;


        public NtbNormalizerService()
        {
            log4net.ThreadContext.Properties["JOBNAME"] = "NTB.Normalizer.Service";
            log4net.Config.XmlConfigurator.Configure();

            _logger = LogManager.GetLogger(typeof(NtbNormalizerService));


            // Creating the new MainServiceComponent
            Service = new MainServiceComponent();

            // Initialising this main component
            InitializeComponent();
        }

        /// <summary>
        /// This method starts the whole shebang. It will create the different job components
        /// </summary>
        protected override void OnStart(string[] args)
        {
            try
            {
                _logger.Info("NTB NormalizerService starting...");
                _startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTB NormalizerService DID NOT START properly - Terminating!", ex);
                throw;
            }
            

        }

        /// <summary>
        /// This method fires when the service stops. 
        /// @todo: This method must work with the closing event in FormDebug
        /// </summary>
        protected override void OnStop()
        {
            try
            {
                _logger.Info("Stopping service");
                Service.Stop();


                _startupConfigTimer.Enabled = false;
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTB NormalizerService did not stop properly - Terminating!"
                    , ex);
                throw;
            }
            

        }

        private void _startupConfigTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _startupConfigTimer.Stop();

            try
            {
                Service.Configure();
                Service.Start();
                _logger.Info("NTB NormalizerService Started!");
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTB NormalizerService DID NOT START properly - TERMINATING!"
                    , ex);

                throw;
            }
        }


    }
}
