﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FormDebug.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.Normalizer.Service
{
    using System;
    using System.Windows.Forms;

    using log4net;

    using NTB.Normalizer.Components;

    /// <summary>
    /// The form debug.
    /// </summary>
    public partial class FormDebug : Form
    {
        /// <summary>
        /// The actual service object
        /// </summary>
        /// <remarks>Does the actuall work</remarks>
        protected MainServiceComponent Service = null;

        /// <summary>
        /// Creating the logger object so we can log stuff
        /// </summary>
        private static ILog _logger = LogManager.GetLogger(typeof(FormDebug));

        /// <summary>
        /// Initializes a new instance of the <see cref="FormDebug"/> class. 
        /// </summary>
        /// <remarks>
        /// Default constructor
        /// </remarks>
        public FormDebug()
        {
            ThreadContext.Properties["JOBNAME"] = "Normalizer-Debug";
            log4net.Config.XmlConfigurator.Configure();
            _logger = LogManager.GetLogger(typeof(FormDebug));

            this.Service = new MainServiceComponent();

            InitializeComponent();
        }

        /// <summary>
        /// The form debug_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void FormDebug_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info("NormalizerService DEBUGMODE starting...");
                _startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                _logger.Fatal("NormalizerService DEBUGMODE DID NOT START properly", ex);
            }
        }

        /// <summary>
        /// The form debug_ form closing.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void FormDebug_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                this.Service.Stop();
                _logger.Info("NormalizerService DEBUGMODE stopped");
            }
            catch (Exception ex)
            {
                _logger.Fatal("NormalizerService DEBUGMODE DID NOT STOP properly", ex);
            }
        }

        /// <summary>
        /// The _startup config timer_ tick.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void _startupConfigTimer_Tick(object sender, EventArgs e)
        {
            // Kill the timer
            _startupConfigTimer.Stop();

            // And configure
            try
            {
                this.Service.Configure();
                this.Service.Start();
                _logger.Info("NormalizerService DEBUGMODE started");
            }
            catch (Exception ex)
            {
                _logger.Fatal("NormalizerService DEBUGMODE DID NOT START properly", ex);
            }
        }
    }
}