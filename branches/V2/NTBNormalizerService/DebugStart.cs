﻿using System;
using System.Windows.Forms;

namespace NTB.Normalizer.Service
{
    /// <summary>
    /// Entry point when compiling for debugging as a WinForm
    /// </summary>
    static class DebugStart
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormDebug());

        }
    }
}
