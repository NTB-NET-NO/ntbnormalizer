﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Configuration;
using System.Reflection;
using System.ServiceProcess;



namespace NTB.Normalizer.Service
{
    [RunInstaller(true)]
    public partial class NTBNormalizerInstaller : Installer
    {
        string strParameters = "";

        string dirLogPath = ConfigurationManager.AppSettings["dirLogPath"];
        string dirRootPath = ConfigurationManager.AppSettings["dirPathRoot"];
        string dirErrorPath = ConfigurationManager.AppSettings["dirErrorPath"];

        // private static LogFile logFile = new LogFile();

        public NTBNormalizerInstaller()
        {
            InitializeComponent();
        }

        public NTBNormalizerInstaller(string strServiceName, string strArguments)
        {
            // This is an overload where one can pass name of service to be created
            string strMessage = "Creating object with parameter: " + strServiceName;
            

            strMessage = "Creating object with parameter: " + strArguments;
            

            try
            {
                //ServiceProcessInstaller ProcessInstaller = new ServiceProcessInstaller();
                //ServiceInstaller installer = new ServiceInstaller();
                //ProcessInstaller.Account = ServiceAccount.LocalSystem;
                //string strServiceInstallName = strServiceName.Replace(" ", "");
                //installer.ServiceName = strServiceInstallName;
                //installer.StartType = ServiceStartMode.Manual;
                //installer.DisplayName = strServiceName;
                //installer.Description = "A NTB Normalizer Service running following XSL-Transformation: " + strArguments;

                //// shall also add parameters to the start
                //strParameters = strArguments;

                //// Doing the install (I believe)
                //Installers.AddRange(new Installer[] { ProcessInstaller, installer });

                //strMessage = " We should now have created a new service";
                //
            }
            catch (InstallException i)
            {
                strMessage = "Exception: " + i.Message.ToString();
                
            }

            catch (Exception e)
            {
                strMessage = "Exception: " + e.Message.ToString();
                
            }
        }

        
        protected override void OnBeforeInstall(IDictionary savedState)
        {
            Context.Parameters["assemblypath"] += strParameters;
            base.OnBeforeInstall(savedState);
        }

        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            Context.Parameters["assemblypath"] += strParameters;
            base.OnBeforeUninstall(savedState);
        }

      private ServiceInstaller GetServiceInstaller()
        {
            ServiceInstaller installer = new ServiceInstaller();
            //installer.ServiceName = GetConfigurationValue("ServiceName");
            return installer;
        }

        private ServiceProcessInstaller GetServiceProcessInstaller()
        {
            ServiceProcessInstaller installer = new ServiceProcessInstaller();
            installer.Account = ServiceAccount.LocalSystem;
            return installer;
        }



        private string GetConfigurationValue(string key)
        {
            Assembly service = Assembly.GetAssembly(typeof(NTBNormalizerInstaller));
            Configuration config = ConfigurationManager.OpenExeConfiguration(service.Location);

            if (config.AppSettings.Settings[key] != null)
            {
                return config.AppSettings.Settings[key].Value;
            }
            else
            {
                throw new IndexOutOfRangeException("Setting collection does not contain the requested key: " + key);
            }
        }

    }
}
