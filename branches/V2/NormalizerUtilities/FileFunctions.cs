﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Configuration;
using log4net;

namespace NTB.Normalizer.Utilities
{
    /// <summary>
    /// This class shall do all the file stuff and nothing else. This to clean up other classes that does this now
    /// </summary>
    class FileFunctions
    {

        #region Logging
        /// <summary>
        /// Static logger
        /// </summary>
        static readonly ILog Logger = LogManager.GetLogger(typeof(FileFunctions));

        #endregion

        /// <summary>
        /// PathRoot holds the path of the root of the Normalizer work directory
        /// </summary>
        public string PathRoot = ConfigurationManager.AppSettings["dirPathRoot"];

        /// <summary>
        /// PathXSLT holds the path to the XSLT-folder - the root of that folder that is, so all job folders are below.
        /// </summary>
        public string PathXslt = ConfigurationManager.AppSettings["dirXSLTRoot"];

        /// <summary>
        /// PathXML holds the path to the XML-folder - the root of that folder that is, so all job folders are below.
        /// </summary>
        public string PathXml = ConfigurationManager.AppSettings["dirXMLRoot"];

        /// <summary>
        /// PathError holds the path to the directory where we are to put files if they fail to be converted
        /// </summary>
        public string PathError = ConfigurationManager.AppSettings["dirErrorPath"];

        /// <summary>
        /// _JobName holds the name of the job we are to convert. Job = directory and XSLT-files
        /// </summary>
        public string JobName = "";

        /// <summary>
        /// FileXSL holds the name of the XSLT-file we are to use to convert the incomming document
        /// </summary>
        public string FileXsl = "";

        /// <summary>
        /// FileXML holds the name of the file to be converted by Normalizer
        /// </summary>
        public string FileXml = "";

        /// <summary>
        /// FileOutput holds the name of the file when converted by Normalizer
        /// </summary>
        public string FileOutput = "";



        public bool GetXsltFile()
        {
            // We have to make sure that we have some directory that we can work with
            if (JobName == "")
            {
                throw new Exception("No job defined. Cannot continue.");
            }

            // Creating the filename
            string filename = PathXslt + @"\" + JobName + @"\" + JobName + ".xsl";
            Logger.Debug("Filename: " + filename);

            // We are getting the XSL-file - which shall only be one
            FileInfo xsltfi = new FileInfo(filename);

            // And so we are testing this
            if (xsltfi.Exists)
            {
                FileXsl = filename;
                return true;
            }
            return false;
        }
        
        /// <summary>
        /// This method loops throught the directory and returns a list of files 
        /// </summary>
        public List<string> GetFiles()
        {

            // We have to make sure that we have some directory that we can work with
            if (JobName == "")
            {
                throw new Exception("No job defined. Cannot continue.");
            }

            // Getting the XML-path for the job
            string filePath = PathXml + @"\" + JobName;

            // Creating the List Files
            List<string> files = new List<string>();

            // We are adding the files to a list that we can return
            foreach (string strFiles in Directory.GetFiles(filePath))
            {
                files.Add(strFiles);
            }

            // Returning Files
            return files;
        }
    }
}
