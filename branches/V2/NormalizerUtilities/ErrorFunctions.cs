using System;
using System.IO;
using System.Threading;

using log4net;

namespace NTB.Normalizer.Utilities
{
    public static class ErrorFunctions
    {

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ErrorFunctions));

        /// <summary>
        /// Static Error Logger
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        /// Gets or sets the value of the PathError public string. It holds the path to the directory where we are to put files if they fail to be converted
        /// </summary>
        /// <remarks>
        /// This has been refactored from public string PathError = ConfigurationManager.AppSettings["dirErrorPath"];
        /// </remarks>
        public static string PathError { get; set; }
        
        /// <summary>
        /// The move error file.
        /// </summary>
        /// <param name="xmlErrorFile">
        /// The xml error file.
        /// </param>
        public static void MoveErrorFile(string xmlErrorFile)
        {
            // Waiting four seconds before we can move the file
            Thread.Sleep(4000);

            FileInfo xmlFile = new FileInfo(xmlErrorFile);

            string strErrorPath = PathError + @"\" + xmlFile.Name;
            Logger.Debug("moving error file: " + xmlErrorFile + " to " + strErrorPath);
            if (!File.Exists(strErrorPath))
            {
                try
                {
                    xmlFile.MoveTo(strErrorPath);

                    Logger.Info("File was moved to " + strErrorPath);
                }
                catch (Exception exception)
                {
                    Logger.Info("Something happened while moving the file. Check error log for more information");
                    ErrorLogger.Error(exception.Message);
                    ErrorLogger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        ErrorLogger.Error(exception.InnerException.Message);
                        ErrorLogger.Error(exception.InnerException.StackTrace);    
                    }
                    
                }
                finally
                {
                    if (xmlFile.Exists)
                    {
                        Logger.Info("We're deleting the file since it exists and since we've moved it already");
                        xmlFile.Delete();
                    }

                }
                return;
            }
            Logger.Info("Checking to see if we are to delete some file or not");

            try
            {
                File.Delete(strErrorPath);

                xmlFile.MoveTo(strErrorPath);

                Logger.Info("File was moved to " + strErrorPath);
            }
            catch (Exception exception)
            {
                Logger.Info("Something happened while moving the file. Check error log for more information.");

                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }

                Logger.Error("We're deleting the file");
                if (!xmlFile.Exists)
                {
                    Logger.Info("The file does not exists. Cannot delete the file");
                }

                // Deleting file so we can get rid of it 
                xmlFile.Delete();
            }
        }
    }
}