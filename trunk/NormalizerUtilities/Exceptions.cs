﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Exceptions.cs" company="">
//   
// </copyright>
// <summary>
//   The normalizer exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.Normalizer.Utilities
{
    using System;

    /// <summary>
    /// The normalizer exception.
    /// </summary>
    public class NormalizerException : Exception
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NormalizerException"/> class. 
        /// Initializes a new instance of the <see cref="FrontendException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public NormalizerException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NormalizerException"/> class. 
        /// Initializes a new instance of the <see cref="FrontendException"/> class.
        /// </summary>
        /// <param name="message">
        /// The error message.
        /// </param>
        /// <param name="exception">
        /// INternal exception.
        /// </param>
        public NormalizerException(string message, Exception exception)
            : base(message, exception)
        {
        }

        #endregion
    }
}