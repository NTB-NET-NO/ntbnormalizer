// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ErrorFunctions.cs" company="">
//   
// </copyright>
// <summary>
//   The error functions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.Normalizer.Utilities
{
    using System;
    using System.IO;
    
    using log4net;

    /// <summary>
    /// The error functions.
    /// </summary>
    public static class ErrorFunctions
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ErrorFunctions));

        /// <summary>
        ///     Static Error Logger
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Gets or sets the value of the PathError public string. It holds the path to the directory where we are to put files
        ///     if they fail to be converted
        /// </summary>
        /// <remarks>
        ///     This has been refactored from public string PathError = ConfigurationManager.AppSettings["dirErrorPath"];
        /// </remarks>
        public static string PathError { get; set; }

        /// <summary>
        /// The move error file.
        /// </summary>
        /// <param name="xmlErrorFile">
        /// The xml error file.
        /// </param>
        /// <param name="errorPath">
        /// The error path
        /// </param>
        public static void MoveErrorFile(string xmlErrorFile, string errorPath)
        {
            // Waiting four seconds before we can move the file
            // Thread.Sleep(4000);

            var xmlFile = new FileInfo(xmlErrorFile);

            var strErrorPath = Path.Combine(errorPath, xmlFile.Name);
            Logger.DebugFormat("Moving error file: {0} to {1}", xmlErrorFile, errorPath);
            if (!File.Exists(strErrorPath))
            {
                try
                {
                    xmlFile.MoveTo(strErrorPath);

                    Logger.InfoFormat("File was moved to {0}",errorPath);
                }
                catch (Exception exception)
                {
                    Logger.Info("Something happened while moving the file. Check error log for more information");
                    ErrorLogger.Error(exception.Message);
                    ErrorLogger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        ErrorLogger.Error(exception.InnerException.Message);
                        ErrorLogger.Error(exception.InnerException.StackTrace);
                    }
                }
                finally
                {
                    xmlFile = new FileInfo(xmlErrorFile);
                    if (xmlFile.Exists)
                    {
                        Logger.Info("We're deleting the file since it exists and since we've moved it already");
                        xmlFile.Delete();
                    }
                }

                return;
            }

            Logger.Info("Checking to see if we are to delete some file or not");

            try
            {
                File.Delete(strErrorPath);

                xmlFile.MoveTo(strErrorPath);

                Logger.InfoFormat("File was moved to {0}",strErrorPath);
            }
            catch (Exception exception)
            {
                Logger.Info("Something happened while moving the file. Check error log for more information.");

                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }

                Logger.Error("We're deleting the file");
                if (!xmlFile.Exists)
                {
                    Logger.Info("The file does not exists. Cannot delete the file");
                }

                // Deleting file so we can get rid of it 
                xmlFile.Delete();
            }
        }
    }
}