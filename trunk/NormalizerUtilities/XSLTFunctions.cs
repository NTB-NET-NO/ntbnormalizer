﻿using System;
/**
 * This class is used so that we can do some data manipulation in the 
 * program it self, where XSLT is not good enough.
 */
using System.Collections;
using System.Globalization;

namespace NTB.Normalizer.Utilities
// namespace NTBNormalizer
{

    public static class XsltFunctions
    {

        public static string EncodeContent(string content)
        {
            // We shall open up a file of replace-tags, then we shall replace all characters with this information
            var hashDictionary = new Hashtable();

            

            // ReadFileToArray(DicFilename);
            return content;
        }


        //public static string SportCalculateTimeFromWinner()
        //{
        //    return "No parameters!";
        //}

        public static string FixWinnerTime(string winnerTime)
        {
            return winnerTime.Replace(".",",").Replace(":", ".");
        }

        public static string SportCalculateTimeFromWinner(string winnerTime, string currentTime)
        {

            // It could be that we have a sprint where there are no times
            if (winnerTime == "")
            {
                return winnerTime;
            }
            if (currentTime == "")
            {
                return currentTime;
            }

            
            const string strDelemitter = "h:.,";
            char[] chrDelemitter = strDelemitter.ToCharArray();

            string[] strSplittedWinnerTime = winnerTime.Split(chrDelemitter);
            string[] strSplittedCurrentTime = currentTime.Split(chrDelemitter);

            int intAddMilliseconds;
            
            // This is run if the we have four splits
            if (strSplittedWinnerTime.Length == 4)
            {
                intAddMilliseconds = strSplittedWinnerTime[3].Length == 2 ? 100 : 10;
                int intHour = Convert.ToInt32(strSplittedWinnerTime[0]);
                int intMinutes = Convert.ToInt32(strSplittedWinnerTime[1]);
                int intSeconds = Convert.ToInt32(strSplittedWinnerTime[2]);
                int intMilliSeconds = Convert.ToInt32(strSplittedWinnerTime[3]);


                int intCurrentHour = Convert.ToInt32(strSplittedCurrentTime[0]);
                int intCurrentMinutes = Convert.ToInt32(strSplittedCurrentTime[1]);
                int intCurrentSeconds = Convert.ToInt32(strSplittedCurrentTime[2]);
                int intCurrentMilliSeconds = Convert.ToInt32(strSplittedCurrentTime[3]);


                int intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                if (intMilliSecondsBack < 0)
                {
                    // intMilliSecondsBack += 10;
                    intCurrentMilliSeconds += intAddMilliseconds;
                    intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                    intCurrentSeconds -= 1;

                }

                int intSecondsBack = intCurrentSeconds - intSeconds;
                if (intSecondsBack < 0)
                {
                    intCurrentSeconds += 60;
                    intSecondsBack = intCurrentSeconds - intSeconds;

                    intCurrentMinutes -= 1;
                }

                int intMinutesBack = intCurrentMinutes - intMinutes;
                if (intMinutesBack < 0)
                {
                    intCurrentMinutes += 60;
                    intMinutesBack = intCurrentMinutes - intMinutes;

                    intCurrentHour -= 1;
                }

                var intHoursBack = intCurrentHour - intHour;
                if (intHoursBack < 0)
                {
                    // intHoursBack = 1;
                }
                var strHours = intHoursBack.ToString(CultureInfo.InvariantCulture);
                var strMinutes = intMinutesBack.ToString(CultureInfo.InvariantCulture);
                var strSeconds = intSecondsBack.ToString(CultureInfo.InvariantCulture);
                var strMilliSeconds = intMilliSecondsBack.ToString(CultureInfo.InvariantCulture);


                if (strSeconds.Length == 1)
                {
                    strSeconds = "0" + strSeconds;
                }

                if (intHoursBack == 0)
                {


                    var strReturn = strMinutes + "." + strSeconds + "," + strMilliSeconds;
                    
                    // If we get a string that is zero, that means we have the winner time
                    if (strReturn != "0.00,0") return strReturn;
                    strReturn = winnerTime.Replace(".", ",").Replace(":", ".").Replace("h", ".");
                    return strReturn;
                }
                else
                {

                    var strReturn = strHours + "." + strMinutes + "." + strSeconds + "," + strMilliSeconds;

                   

                    // If we get a string that is zero, that means we have the winner time
                    if (strReturn != "0.00.00,0") return strReturn;
                    strReturn = winnerTime.Replace(".", ",").Replace(":", ".").Replace("h", ".");
                    return strReturn;
                }


            }

                // We get here if we have three splits
            if (strSplittedWinnerTime.Length == 3)
            {
                intAddMilliseconds = strSplittedWinnerTime[2].Length == 2 ? 100 : 10;
                var intMinutes = Convert.ToInt32(strSplittedWinnerTime[0]);
                var intSeconds = Convert.ToInt32(strSplittedWinnerTime[1]);
                var intMilliSeconds = Convert.ToInt32(strSplittedWinnerTime[2]);
                var intCurrentMinutes = Convert.ToInt32(strSplittedCurrentTime[0]);
                var intCurrentSeconds = Convert.ToInt32(strSplittedCurrentTime[1]);
                var intCurrentMilliSeconds = Convert.ToInt32(strSplittedCurrentTime[2]);

             
                var intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                if (intMilliSecondsBack < 0)
                {
                    intCurrentMilliSeconds += intAddMilliseconds;
                    intCurrentSeconds -= 1;

                    // Doing the calculations again
                    intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                }

                var intSecondsBack = intCurrentSeconds - intSeconds;
                if (intSecondsBack < 0)
                {
                    intCurrentSeconds += 60;
                    intCurrentMinutes -= 1;

                    intSecondsBack = intCurrentSeconds - intSeconds;
                }

                if (strSplittedCurrentTime.Length == 4)
                {
                    intCurrentMinutes += 60;
                }

                var intMinutesBack = intCurrentMinutes - intMinutes;



                var strMinutes = intMinutesBack.ToString(CultureInfo.InvariantCulture);
                var strSeconds = intSecondsBack.ToString(CultureInfo.InvariantCulture);
                var strMilliSeconds = intMilliSecondsBack.ToString(CultureInfo.InvariantCulture);

                if (strSeconds.Length == 1)
                {
                    strSeconds = "0" + strSeconds;
                }

                var strReturn = strMinutes + "." + strSeconds + "," + strMilliSeconds;

           
                return strReturn == "0.00,0" ? winnerTime.Replace(".", ",").Replace(":", ".") : strReturn;
            }


            return winnerTime.Replace(".", ",").Replace(":", ".");


        }


        public static string SportCalculateTimeFromWinnerWithRank(string winnerTime, string currentTime, string rank)
        {

            if (winnerTime == "")
            {
                throw new Exception("Winner Time is empty!");
            }
            if (currentTime == "")
            {
                throw new Exception("Current time is emtpy!");
            }
            if (rank == "")
            {
                throw new Exception("Rank is empty!");
            }

            const string strDelemitter = "h:.,";
            char[] chrDelemitter = strDelemitter.ToCharArray();

            string[] strSplittedWinnerTime = winnerTime.Split(chrDelemitter);
            string[] strSplittedCurrentTime = currentTime.Split(chrDelemitter);


           
            int intAddMilliseconds;
            switch (strSplittedWinnerTime.Length)
            {
                case 4:
                    {
                        intAddMilliseconds = strSplittedWinnerTime[3].Length == 2 ? 100 : 10;
                        int intHour = Convert.ToInt32(strSplittedWinnerTime[0]);
                        int intMinutes = Convert.ToInt32(strSplittedWinnerTime[1]);
                        int intSeconds = Convert.ToInt32(strSplittedWinnerTime[2]);
                        int intMilliSeconds = Convert.ToInt32(strSplittedWinnerTime[3]);


                        int intCurrentHour = Convert.ToInt32(strSplittedCurrentTime[0]);
                        int intCurrentMinutes = Convert.ToInt32(strSplittedCurrentTime[1]);
                        int intCurrentSeconds = Convert.ToInt32(strSplittedCurrentTime[2]);
                        int intCurrentMilliSeconds = Convert.ToInt32(strSplittedCurrentTime[3]);


                        int intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                        if (intMilliSecondsBack < 0)
                        {
                            // intMilliSecondsBack += 10;
                            intCurrentMilliSeconds += intAddMilliseconds;
                            intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                            intCurrentSeconds -= 1;

                        }

                        int intSecondsBack = intCurrentSeconds - intSeconds;
                        if (intSecondsBack < 0)
                        {
                            intCurrentSeconds += 60;
                            intSecondsBack = intCurrentSeconds - intSeconds;

                            intCurrentMinutes -= 1;
                        }

                        var intMinutesBack = intCurrentMinutes - intMinutes;
                        if (intMinutesBack < 0)
                        {
                            intCurrentMinutes += 60;
                            intMinutesBack = intCurrentMinutes - intMinutes;

                            intCurrentHour -= 1;
                        }

                        var intHoursBack = intCurrentHour - intHour;
                        if (intHoursBack < 0)
                        {
                            // intHoursBack = 1;
                        }
                        var strHours = intHoursBack.ToString(CultureInfo.InvariantCulture);
                        var strMinutes = intMinutesBack.ToString(CultureInfo.InvariantCulture);
                        var strSeconds = intSecondsBack.ToString(CultureInfo.InvariantCulture);
                        var strMilliSeconds = intMilliSecondsBack.ToString(CultureInfo.InvariantCulture);


                        if (strSeconds.Length == 1)
                        {
                            strSeconds = "0" + strSeconds;
                        }

                        if (intHoursBack == 0)
                        {


                            var strReturn = strMinutes + "." + strSeconds + "," + strMilliSeconds;
                            
                            // If we get a string that is zero, that means we have the winner time
                            if (strReturn == "0.00,0")
                            {
                                strReturn = winnerTime.Replace(".", ",").Replace(":", ".").Replace("h", ".");
                                return strReturn;
                            }

                            if (rank == "2")
                            {
                                strReturn += " min. bak";
                            }
                            return strReturn;

                        }
                        else
                        {

                            var strReturn = strHours + "." + strMinutes + "." + strSeconds + "," + strMilliSeconds;

                            
                            // If we get a string that is zero, that means we have the winner time
                            if (strReturn == "0.00.00,0")
                            {
                                strReturn = winnerTime.Replace(".", ",").Replace(":", ".").Replace("h", ".");
                                return strReturn;
                            }

                            if (rank == "2")
                            {
                                strReturn += " min. bak";
                            }
                            return strReturn;

                        }


                    }
                case 3:
                    {
                        intAddMilliseconds = strSplittedWinnerTime[2].Length == 2 ? 100 : 10;
                        var intMinutes = Convert.ToInt32(strSplittedWinnerTime[0]);
                        var intSeconds = Convert.ToInt32(strSplittedWinnerTime[1]);
                        var intMilliSeconds = Convert.ToInt32(strSplittedWinnerTime[2]);
                        var intCurrentMinutes = Convert.ToInt32(strSplittedCurrentTime[0]);
                        var intCurrentSeconds = Convert.ToInt32(strSplittedCurrentTime[1]);
                        var intCurrentMilliSeconds = Convert.ToInt32(strSplittedCurrentTime[2]);


                        var intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                        if (intMilliSecondsBack < 0)
                        {
                            intCurrentMilliSeconds += intAddMilliseconds;
                            intCurrentSeconds -= 1;

                            // Doing the calculations again
                            intMilliSecondsBack = intCurrentMilliSeconds - intMilliSeconds;
                        }

                        var intSecondsBack = intCurrentSeconds - intSeconds;
                        if (intSecondsBack < 0)
                        {
                            intCurrentSeconds += 60;
                            intMinutes -= 1;

                            intSecondsBack = intCurrentSeconds - intSeconds;
                        }

                        if (strSplittedCurrentTime.Length == 4)
                        {
                            intCurrentMinutes += 60;
                        }

                        var intMinutesBack = intCurrentMinutes - intMinutes;



                        var strMinutes = intMinutesBack.ToString(CultureInfo.InvariantCulture);
                        var strSeconds = intSecondsBack.ToString(CultureInfo.InvariantCulture);
                        var strMilliSeconds = intMilliSecondsBack.ToString(CultureInfo.InvariantCulture);

                        if (strSeconds.Length == 1)
                        {
                            strSeconds = "0" + strSeconds;
                        }

                        var strReturn = strMinutes + "." + strSeconds + "," + strMilliSeconds;

                        
                        if (strReturn == "0.00,0")
                        {
                            return winnerTime.Replace(".", ",").Replace(":", ".");
                        }
                        if (rank == "2")
                        {
                            strReturn += " min. bak";
                        }
                        return strReturn;
                    }
            }



            return winnerTime.Replace(".", ",").Replace(":", ".");


        }
    }
}
