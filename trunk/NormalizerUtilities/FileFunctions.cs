﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileFunctions.cs" company="Norsk Telegrambyrå AS">
//   Copyright (C) Norsk Telegrambyå
// </copyright>
// <summary>
//   This class shall do all the file stuff and nothing else. This to clean up other classes that does this now
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.Normalizer.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;

    using log4net;

    /// <summary>
    /// This class shall do all the file stuff and nothing else. This to clean up other classes that does this now
    /// </summary>
    internal class FileFunctions
    {
        #region Logging

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(FileFunctions));

        #endregion

        /// <summary>
        /// PathError holds the path to the directory where we are to put files if they fail to be converted
        /// </summary>
        private string pathError = ConfigurationManager.AppSettings["dirErrorPath"];

        /// <summary>
        /// PathRoot holds the path of the root of the Normalizer work directory
        /// </summary>
        private string pathRoot = ConfigurationManager.AppSettings["dirPathRoot"];

        /// <summary>
        /// PathXML holds the path to the XML-folder - the root of that folder that is, so all job folders are below.
        /// </summary>
        private string pathXml = ConfigurationManager.AppSettings["dirXMLRoot"];

        /// <summary>
        /// PathXSLT holds the path to the XSLT-folder - the root of that folder that is, so all job folders are below.
        /// </summary>
        private string pathXslt = ConfigurationManager.AppSettings["dirXSLTRoot"];

        /// <summary>
        /// Gets or sets the path xml.
        /// </summary>
        public string PathXml
        {
            get
            {
                return this.pathXml;
            }

            set
            {
                this.pathXml = value;
            }
        }

        /// <summary>
        /// Gets or sets the path xml.
        /// </summary>
        public string PathXslt
        {
            get
            {
                return this.pathXslt;
            }

            set
            {
                this.pathXslt = value;
            }
        }

        /// <summary>
        /// Gets or sets the path xml.
        /// </summary>
        public string PathRoot
        {
            get
            {
                return this.pathRoot;
            }

            set
            {
                this.pathRoot = value;
            }
        }

        /// <summary>
        /// Gets or sets the path xml.
        /// </summary>
        public string PathError
        {
            get
            {
                return this.pathError;
            }

            set
            {
                this.pathError = value;
            }
        }

        /// <summary>
        /// Gets or sets the file output
        /// </summary>
        public string FileOutput { get; set; }

        /// <summary>
        /// Gets or sets the file xml
        /// </summary>
        public string FileXml { get; set; }

        /// <summary>
        /// Gets or sets the file xsl
        /// </summary>
        public string FileXsl { get; set; }

        /// <summary>
        /// Gets or sets the jobname
        /// </summary>
        public string JobName { get; set; }

        /// <summary>
        /// The get xslt file.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// throws an exception on error
        /// </exception>
        public bool GetXsltFile()
        {
            // We have to make sure that we have some directory that we can work with
            if (this.JobName == string.Empty)
            {
                throw new Exception("No job defined. Cannot continue.");
            }

            // Creating the filename
            var filename = this.pathXslt + @"\" + this.JobName + @"\" + this.JobName + ".xsl";
            Logger.Debug("Filename: " + filename);

            // We are getting the XSL-file - which shall only be one
            var xsltfi = new FileInfo(filename);

            // And so we are testing this
            if (xsltfi.Exists)
            {
                this.FileXsl = filename;
                return true;
            }

            return false;
        }

        /// <summary>
        /// This method loops throught the directory and returns a list of files 
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<string> GetFiles()
        {
            // We have to make sure that we have some directory that we can work with
            if (this.JobName == string.Empty)
            {
                throw new Exception("No job defined. Cannot continue.");
            }

            // Getting the XML-path for the job
            var filePath = this.pathXml + @"\" + this.JobName;

            // Creating the List Files
            var files = new List<string>();

            // We are adding the files to a list that we can return
            foreach (var strFiles in Directory.GetFiles(filePath))
            {
                files.Add(strFiles);
            }

            // Returning Files
            return files;
        }
    }
}