﻿using System;
using System.ServiceProcess;
using System.Linq;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Reflection;
using ntb_FuncLib;
using System.Diagnostics;
using System.Collections;

namespace NTBNormalizerService
{
    public class Program
    {
        public string dirLogPath = NTBNormalizer.Properties.Settings.Default.dirLogPath;
        public string dirRootPath = NTBNormalizer.Properties.Settings.Default.dirPathRoot;
        public string dirErrorPath = NTBNormalizer.Properties.Settings.Default.dirErrorPath;

        bool boolDebug = NTBNormalizer.Properties.Settings.Default.Debug;



        /// <summary>
        /// The main entry point for the application with parameters
        /// </summary>
        /// <param name="args">string-array containing parameters</param>
        static void main(string[] args)
        {
            try
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
			{ 
				new NTBNormalizerService() 
			};
                ServiceBase.Run(ServicesToRun);
            }
            catch (Exception ex)
            {
                string SourceName = "NTBNormalizerService.ExceptionLog";
                if (!EventLog.SourceExists(SourceName))
                {
                    EventLog.CreateEventSource(SourceName, "Application");
                }

                EventLog eventLog = new EventLog();
                eventLog.Source = SourceName;
                string message = string.Format("Exception: {0} \n\nStack: {1}", ex.Message, ex.StackTrace);
                eventLog.WriteEntry(message, EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        static void Main()
        {

            try
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
			{ 
				new NTBNormalizerService() 
			};
                ServiceBase.Run(ServicesToRun);
            }
            catch (Exception ex)
            {
                string SourceName = "NTBNormalizerService.ExceptionLog";
                if (!EventLog.SourceExists(SourceName))
                {
                    EventLog.CreateEventSource(SourceName, "Application");
                }

                EventLog eventLog = new EventLog();
                eventLog.Source = SourceName;
                string message = string.Format("Exception: {0} \n\nStack: {1}", ex.Message, ex.StackTrace);
                eventLog.WriteEntry(message, EventLogEntryType.Error);
            }


        }



        public void LoopDirectories(string strFolder)
        {


            string strMessage = "";
            Boolean loopCSVFolder = false;
            // Creating the string Path
            string strPath = NTBNormalizer.Properties.Settings.Default.dirXSLTRoot + @"\" + strFolder;
            string strXMLConfigFile =
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase.ToString()) +
                @"\" + strFolder.ToString();

            UriBuilder uri = new UriBuilder(strXMLConfigFile);
            string path = Uri.UnescapeDataString(uri.Path);

            // strXMLConfigFile = Path.GetDirectoryName(path) + @"\" + strFolder + ".xml";



            if (NTBNormalizer.Properties.Settings.Default.Debug == true)
            {
                string strxMessage = "Dette er strXMLConfigFile: " + strXMLConfigFile;
                LogFile.WriteLog(ref dirLogPath, ref strxMessage);

                strMessage = "Dette er strPath: " + strPath;
                LogFile.WriteLog(ref dirLogPath, ref strMessage);
            }
            // Get the folder

            // Check if config-XML for job exists
            if (System.IO.File.Exists(strXMLConfigFile) == true)
            {
                strMessage = "Getting data from job-config-file";
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                XPathDocument xPathDoc;
                XPathNavigator xPathNav;
                XPathNodeIterator NodeIter;
                string strXPathExpression = "//csvfolder";

                xPathDoc = new XPathDocument(strXMLConfigFile);

                xPathNav = xPathDoc.CreateNavigator();
                NodeIter = xPathNav.Select(strXPathExpression);

                loopCSVFolder = Convert.ToBoolean(NodeIter.Current.Value);

                // Then we are using a foldername and are not looping directories

            }


            if (loopCSVFolder == false)
            {
                if (strFolder != "")
                {
                    strMessage = strFolder + ".xml Exists";
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                }

                if (System.IO.File.Exists(strXMLConfigFile))
                {
                    // So we are just calling the do convert:

                    strMessage = "Exists";
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                    DoConvert(strPath);
                }
                else
                {

                    LoopDirectory(strPath);
                }
            }
            else
            {
                strMessage = "loopCSVFolder is true";
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                LoopDirectory(strPath, loopCSVFolder);
            }

            // Creating the string Path


            //strPath = NTBNormalizer.Properties.Settings.Default.dirCSVRoot + @"\" + strFolder;
            //if (NTBNormalizer.Properties.Settings.Default.Debug == true)
            //{
            //    string strMessage = "Dette er strPath: " + strPath;
            //    LogFile.WriteLog(ref dirLogPath, ref strMessage);
            //}
            //// Get the folder

            //LoopDirectory(strPath, true);

        }

        public void LoopDirectory(string strPath, bool boolConvertCSV = false)
        {
            // Looping through the directory

            string[] directories = Directory.GetDirectories(strPath);
            foreach (string strDir in directories)
            {
                string strDirectory = strDir.ToString();

                if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                {
                    LogFile.WriteLog(ref dirLogPath, ref strDirectory);
                }

                if (boolConvertCSV == false)
                {
                    if (boolDebug == true)
                    {
                        string strMessage = "Doing doConvert with " + strDir;
                        string strLogPath = NTBNormalizer.Properties.Settings.Default.dirLogPath;

                        LogFile.WriteLog(ref strLogPath, ref strMessage);
                    }
                    

                    DoConvert(strDir);
                }
                else
                {
                    doCSVConvert(strDir);
                }
                LoopDirectory(strDir);
            }
        }



        /// <summary>
        /// This method does the converting, or it calles the converter jobs.
        /// er. make that it gets the different xsl-files in the different folders
        /// </summary>
        //public void DoConvert()
        //{
        //    string strFolder = "";
        //    DirectoryInfo di =
        //    new DirectoryInfo(NTBNormalizer.Properties.Settings.Default.dirXSLTRoot + @"\" + strFolder);


        //    // We are getting the XSL-file - which shall only be one
        //    FileInfo[] fileXSLT = di.GetFiles("*.xsl");
        //    string filename = NTBNormalizer.Properties.Settings.Default.dirXSLTRoot + @"\" + strFolder + @"\" + fileXSLT.First().Name.ToString();

        //    // We need to set this one to more than one output also...
        //    // We check if some value is set in config-file
        //    // We set the initial value of dirOutput here

        //    ArrayList arrOutFolder = new ArrayList();


        //    // If the OutputJobFolders setting is set to true, we add the folder
        //    if (NTBNormalizer.Properties.Settings.Default.OutputJobfolders == true)
        //    {
        //        string strOutFolder = "";
        //        ArrayList arrOutfolder = new ArrayList();
        //        strOutFolder = NTBNormalizer.Properties.Settings.Default.dirOutput;
        //        // Get the name of OutputFoldername. This so we can change it
        //        arrOutfolder.Add(strOutFolder);
        //        DirectoryInfo dof =
        //            new DirectoryInfo(arrOutfolder[0].ToString());

        //        if (!dof.Exists)
        //        {
        //            dof.Create();
        //        }

        //    }
        //    else
        //    {
        //        string strMessage = "";

        //        // We must create a job-file where output folders are defined
        //            string strJobs = NTBNormalizer.Properties.Settings.Default.JobFile;

        //            string[] strDi = strPath.Split(@"\".ToCharArray());

        //            // string strDi = Path.GetDirectoryName(strPath);

        //            int counter = strDi.Count();
        //            counter--;
        //            strMessage = "strDI = " + strDi[counter];
        //            LogFile.WriteLog(ref strLogPath, ref strMessage);


        //            string strJobName = strDi[counter];

        //            string strXOutputDir = @"//job[@name='"+strJobName+"']/output/@path";



        //            // This variable is used to read the ScheduleConfig-file
        //            XPathDocument Doc = new XPathDocument(strJobs);

        //            // Creating the XPath Navigator
        //            XPathNavigator Nav = ((IXPathNavigable)Doc).CreateNavigator();

        //            // Creating the XPathNodeIterator
        //            XPathNodeIterator iterJob = Nav.Select(strXOutputDir);




        //            // Looping the result.. if there is any
        //            while (iterJob.MoveNext())
        //            {
        //                // Is this empty then, or ?
        //                // this will only return one (and the last one)
        //                arrOutfolder.Add(iterJob.Current.Value.ToString());
        //            }



        //        }
        //    }


        //    // Now we shall get the XML-file(s), so they can be many
        //    DirectoryInfo dixml =
        //        new DirectoryInfo(NTBNormalizer.Properties.Settings.Default.dirXMLRoot + @"\" + strFolder);

        //    FileInfo[] fileXML = dixml.GetFiles("*.xml");

        //    if (fileXML.Count() > 0)
        //    {
        //        NTBNormalizer.NTBConverter xc = new NTBNormalizer.NTBConverter();
        //        // xc.XSLConverter(filename, fileXML);

        //        xc.XSLAltovaConverter(filename, fileXML, strOutfolder);
        //    }

        //}


        public void DoConvert(string strPath)
        {
            try
            {

                // Setting some values 
                string strMessage = "";
                string strLogPath = NTBNormalizer.Properties.Settings.Default.dirLogPath;
                bool boolDebug = NTBNormalizer.Properties.Settings.Default.Debug;

                strMessage = "In DoConvert(" + strPath + ")";
                LogFile.WriteLog(ref strLogPath, ref strMessage);

                // creating the directory object so that we can loop through the file
                DirectoryInfo di =
                    new DirectoryInfo(strPath);

                string filename = strPath + @"\" + di.Name + ".xsl";

                // We are getting the XSL-file - which shall only be one
                FileInfo fi = new FileInfo(filename);

                // And so we are testing this
                if (fi.Exists == true)
                {
                    // string filename = fileXSLT.First().Name.ToString();

                    // We need to set this one to more than one output also...
                    // We check if some value is set in config-file
                    // We set the initial value of dirOutput here
                    string strOutFolder = "";
                    ArrayList arrOutfolder = new ArrayList();
                    ArrayList arrFileEncoding = new ArrayList();
                    string strOutputDocType = "";


                    // If the OutputJobFolders setting is set to true, we add the folder
                    if (NTBNormalizer.Properties.Settings.Default.OutputJobfolders == true)
                    {
                        strOutFolder = NTBNormalizer.Properties.Settings.Default.dirOutput;
                        // Get the name of OutputFoldername. This so we can change it
                        string strOutputFolderName = NTBNormalizer.Properties.Settings.Default.dirOutputName;
                        strOutFolder = strPath.Replace("xslt", strOutputFolderName).ToString();
                        arrOutfolder.Add(strOutFolder);
                        DirectoryInfo dof =
                            new DirectoryInfo(arrOutfolder[0].ToString());

                        if (!dof.Exists)
                        {
                            dof.Create();
                        }
                    }
                    else
                    {
                        // We are using the jobfile to find output-files and so on.
                        string strJobs = NTBNormalizer.Properties.Settings.Default.JobFile;

                        string[] strDi = strPath.Split(@"\".ToCharArray());

                        // string strDi = Path.GetDirectoryName(strPath);

                        int counter = strDi.Count();
                        counter--;

                        if (boolDebug == true)
                        {
                            strMessage = "strDI = " + strDi[counter];
                            LogFile.WriteLog(ref strLogPath, ref strMessage);
                        }


                        string strJobName = strDi[counter];

                        string strXOutputDir = @"//job[@name='" + strJobName + "']/output/@path";
                        string strEncoding = @"//job[@name='" + strJobName + "']/output/@encoding";
                        string strDocumentOutput = @"//job[@name='" + strJobName + "']/@extention";

                        if (boolDebug == true)
                        {
                            strMessage = "strXOutputDir: " + strXOutputDir.ToString();
                            LogFile.WriteLog(ref dirLogPath, ref strMessage);

                            strMessage = "strEncoding: " + strEncoding.ToString();
                            LogFile.WriteLog(ref dirLogPath, ref strMessage);

                            strMessage = "strDocumentOutput: " + strDocumentOutput.ToString();
                            LogFile.WriteLog(ref dirLogPath, ref strMessage);
                        }


                        // This variable is used to read the ScheduleConfig-file
                        XPathDocument Doc = new XPathDocument(strJobs);

                        // Creating the XPath Navigator
                        XPathNavigator Nav = ((IXPathNavigable)Doc).CreateNavigator();

                        // Creating the XPathNodeIterator
                        XPathNodeIterator iterJob = Nav.Select(strXOutputDir);




                        // Looping the result.. if there is any
                        while (iterJob.MoveNext())
                        {
                            // Is this empty then, or ?
                            // this will only return one (and the last one)
                            arrOutfolder.Add(iterJob.Current.Value.ToString());
                        }


                        // Creating the XPathNodeIterator
                        XPathNodeIterator iterEncodingJob = Nav.Select(strEncoding);

                        // Looping the result.. if there is any
                        while (iterEncodingJob.MoveNext())
                        {
                            // Is this empty then, or ?
                            // this will only return one (and the last one)
                            arrFileEncoding.Add(iterEncodingJob.Current.Value.ToString());
                        }

                        // Creating the XPathNodeIterator
                        XPathNodeIterator iterOutputJob = Nav.Select(strDocumentOutput);

                        // Looping the result.. if there is any
                        while (iterOutputJob.MoveNext())
                        {
                            // Is this empty then, or ?
                            // this will only return one (and the last one)
                            strOutputDocType = iterOutputJob.Current.Value.ToString();


                            if (boolDebug == true)
                            {
                                strMessage = "OutputDocType = '" + strOutputDocType + "' inside while loop";
                                LogFile.WriteLog(ref dirLogPath, ref strMessage);
                            }
                        }

                        if (boolDebug == true)
                        {
                            strMessage = "OutputDocType = '" + strOutputDocType + "'";
                            LogFile.WriteLog(ref dirLogPath, ref strMessage);
                        }
                    }

                    if (boolDebug == true)
                    {
                        strMessage = "OutputDocType = '" + strOutputDocType + "' after the loop";
                        LogFile.WriteLog(ref dirLogPath, ref strMessage);
                    }



                    // We are creating the path to XML-folder by replacing xslt with xml
                    string strXMLFolder = strPath.Replace("xslt", "xml"); // NTBNormalizer.Properties.Settings.Default.dirXMLRoot + @"\" + strFolder;

                    // We are creating the path to Done-folder by removing xslt
                    string strDoneFolder = strPath.Replace("xslt", "done");



                    if (boolDebug == true)
                    {
                        strMessage = "Getting data from " + strXMLFolder;
                        LogFile.WriteLog(ref dirLogPath, ref strMessage);
                    }
                    // Now we shall get the XML-file(s), so they can be many
                    DirectoryInfo dixml =
                        new DirectoryInfo(strXMLFolder);

                    if (!dixml.Exists)
                    {
                        dixml.Create();
                    }

                    DirectoryInfo didone =
                        new DirectoryInfo(strDoneFolder);

                    if (!didone.Exists)
                    {
                        didone.Create();
                    }


                    FileInfo[] fileXML = dixml.GetFiles("*.xml");

                    if (fileXML.Count() > 0)
                    {
                        NTBNormalizer.NTBConverter xc = new NTBNormalizer.NTBConverter();
                        foreach (FileInfo xmlFile in fileXML)
                        {
                            
                            if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                            {
                                strMessage = "using " + filename + " to transform " + xmlFile.ToString() + " moving file to " + strOutFolder + " ";
                                LogFile.WriteLog(ref dirLogPath, ref strMessage);
                            }

                            // Waiting 50 something - why?

                            // Has been marked out because we have another waiting above
                            // System.Threading.Thread.Sleep(50); 


                            // Using the Altova Converter to convert the XML-file
                            bool boolConvert = xc.XSLAltovaConverter(filename, 
                                xmlFile, 
                                arrOutfolder, 
                                arrFileEncoding, 
                                strOutputDocType);

                            if (boolConvert == true)
                            {
                                // Moving files
                                if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                                {
                                    strMessage = "Moving files to done directory " + strDoneFolder;
                                    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                                }

                                if (File.Exists(strDoneFolder + @"\" + xmlFile.Name))
                                {
                                    File.Delete(strDoneFolder + @"\" + xmlFile.Name);
                                }
                                xmlFile.MoveTo(strDoneFolder + @"\" + xmlFile.Name);
                            }
                            else
                            {
                                // Moving files
                                strMessage = "There was an error. We shall send an e-mail to someone " + strDoneFolder;
                                LogFile.WriteLog(ref dirLogPath, ref strMessage);
                            }
                        }
                    }
                }
            }

            catch (IOException ioe)
            {
                // catch the exception
                string strMessage = ioe.Message.ToString();

                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                LogFile.WriteLog(ref dirLogPath, ref strMessage);
                // Catch the inner exception if there is one
                if (ioe.InnerException.Message.ToString() != "")
                {
                    strMessage = ioe.InnerException.Message.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                }
            }
            catch (Exception e)
            {
                // catch the exception
                string strMessage = e.Message.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                strMessage = e.StackTrace.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);




                //if (e.InnerException.Message.ToString() != "")
                //{
                //    strMessage = e.InnerException.Message.ToString();
                //    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                //}
            }

        }

        /// <summary>
        /// This method is used to convert CSV-files. CSV-files must have a header-line that defines
        /// the different data-fields. Without these datafields we cannot convert
        /// the CSV-file
        /// </summary>
        /// <param name="strPath">String containing the path we shall search for CSV-filers</param>
        public void doCSVConvert(string strPath)
        {
            try
            {

                string strMessage = "";
                string strLogPath = NTBNormalizer.Properties.Settings.Default.dirLogPath;
                bool boolDebug = NTBNormalizer.Properties.Settings.Default.Debug;

                DirectoryInfo di =
                    new DirectoryInfo(strPath);


                // We are getting the CSV-files to transform.
                /**
                 * The CSV-file we create will define the different tags in the XML-file
                 * 
                 */


                // We need to set this one to more than one output also...
                // We check if some value is set in config-file
                // We set the initial value of dirOutput here
                string strOutFolder = "";
                ArrayList arrOutfolder = new ArrayList();
                ArrayList arrFileEncoding = new ArrayList();
                string strOutputDocType = "";


                // If the OutputJobFolders setting is set to true, we add the folder
                if (NTBNormalizer.Properties.Settings.Default.OutputJobfolders == true)
                {
                    strOutFolder = NTBNormalizer.Properties.Settings.Default.dirOutput;
                    // Get the name of OutputFoldername. This so we can change it
                    string strOutputFolderName = NTBNormalizer.Properties.Settings.Default.dirOutputName;

                    // This part here I believe do need some fixing because we could end up
                    // with a filename that is later overwritten.
                    strOutFolder = strPath.Replace("csv", strOutputFolderName).ToString();
                    arrOutfolder.Add(strOutFolder);
                    DirectoryInfo dof =
                        new DirectoryInfo(arrOutfolder[0].ToString());

                    if (!dof.Exists)
                    {
                        dof.Create();
                    }
                }
                else
                {
                    // We must create a job-file where output folders are defined
                    string strJobs = NTBNormalizer.Properties.Settings.Default.JobFile;

                    string[] strDi = strPath.Split(@"\".ToCharArray());

                    // string strDi = Path.GetDirectoryName(strPath);

                    int counter = strDi.Count();
                    counter--;

                    if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                    {
                        strMessage = "strDI = " + strDi[counter];
                        LogFile.WriteLog(ref strLogPath, ref strMessage);
                    }

                    string strJobName = strDi[counter];

                    string strXOutputDir = @"//job[@name='" + strJobName + "']/output/@path";
                    string strEncoding = @"//job[@name='" + strJobName + "']/output/@encoding";
                    string strDocumentOutput = @"//job[@name='" + strJobName + "']/@extention";



                    // This variable is used to read the ScheduleConfig-file
                    XPathDocument Doc = new XPathDocument(strJobs);

                    // Creating the XPath Navigator
                    XPathNavigator Nav = ((IXPathNavigable)Doc).CreateNavigator();

                    // Creating the XPathNodeIterator
                    XPathNodeIterator iterJob = Nav.Select(strXOutputDir);




                    // Looping the result.. if there is any
                    while (iterJob.MoveNext())
                    {
                        // Is this empty then, or ?
                        // this will only return one (and the last one)
                        arrOutfolder.Add(iterJob.Current.Value.ToString());
                    }


                    // Creating the XPathNodeIterator
                    XPathNodeIterator iterEncodingJob = Nav.Select(strEncoding);

                    // Looping the result.. if there is any
                    while (iterEncodingJob.MoveNext())
                    {
                        // Is this empty then, or ?
                        // this will only return one (and the last one)
                        arrFileEncoding.Add(iterEncodingJob.Current.Value.ToString());
                    }

                    // Creating the XPathNodeIterator
                    XPathNodeIterator iterOutputJob = Nav.Select(strDocumentOutput);

                    // Looping the result.. if there is any
                    while (iterOutputJob.MoveNext())
                    {
                        // Is this empty then, or ?
                        // this will only return one (and the last one)
                        strOutputDocType = iterOutputJob.Current.Value.ToString();

                        if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                        {
                            strMessage = "OutputDocType = '" + strOutputDocType + "' inside while loop";
                            LogFile.WriteLog(ref dirLogPath, ref strMessage);
                        }
                    }
                    if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                    {
                        strMessage = "OutputDocType = '" + strOutputDocType + "'";
                        LogFile.WriteLog(ref dirLogPath, ref strMessage);
                    }
                }
                if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                {
                    strMessage = "OutputDocType = '" + strOutputDocType + "' after the loop";
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                }


                // We are creating the path to Done-folder by removing xslt
                string strDoneFolder = strPath.Replace("csv", "done");


                if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                {
                    //strMessage = "Getting data from " + strCSVFolder;
                    //LogFile.WriteLog(ref dirLogPath, ref strMessage);
                }

                FileInfo[] fileCSV = di.GetFiles("*.csv");

                if (fileCSV.Count() > 0)
                {

                    NTBNormalizer.NTBConverter xc = new NTBNormalizer.NTBConverter();
                    foreach (FileInfo csvFile in fileCSV)
                    {

                        if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                        {
                            strMessage = "transforming " + csvFile.ToString() + " moving file to " + strOutFolder + " ";
                            LogFile.WriteLog(ref dirLogPath, ref strMessage);
                        }
                        // Using the Altova Converter to convert the XML-file
                        System.Threading.Thread.Sleep(50);

                        //bool boolConvert = xc.CSVConverter(csvFile, arrOutfolder, arrFileEncoding, strOutputDocType);

                        //if (boolConvert == true)
                        //{
                        //    // Moving files
                        //    if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                        //    {
                        //        strMessage = "Moving files to done directory " + strDoneFolder;
                        //        LogFile.WriteLog(ref dirLogPath, ref strMessage);
                        //    }

                        //    if (File.Exists(strDoneFolder + @"\" + xmlFile.Name))
                        //    {
                        //        File.Delete(strDoneFolder + @"\" + xmlFile.Name);
                        //    }
                        //    xmlFile.MoveTo(strDoneFolder + @"\" + xmlFile.Name);
                        //}
                        //else
                        //{
                        //    // Moving files
                        //    strMessage = "There was an error. We shall send an e-mail to someone " + strDoneFolder;
                        //    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                        //}
                    }
                }

            }

            catch (IOException ioe)
            {
                // catch the exception
                string strMessage = ioe.Message.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                strMessage = ioe.StackTrace.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                // Catch the inner exception if there is one
                if (ioe.InnerException.Message.ToString() != "")
                {
                    strMessage = ioe.InnerException.Message.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                }
            }
            catch (Exception e)
            {
                // catch the exception
                string strMessage = e.Message.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                strMessage = e.StackTrace.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);


                if (e.InnerException.Message.ToString() != "")
                {
                    strMessage = e.InnerException.Message.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                }
            }
        }
    }
}
