﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Configuration.Assemblies;
using ntb_FuncLib;
using System.Net.Mail;






namespace NTBNormalizer
{
    /// <summary>
    /// This class shall deal with all things related to converting files
    /// it pretty much runs the XslCompiledTransform object to do this
    /// </summary>
    class NTBConverter
    {

        public string dirLogPath = NTBNormalizer.Properties.Settings.Default.dirLogPath;
        public string dirRootPath = NTBNormalizer.Properties.Settings.Default.dirPathRoot;
        public string dirErrorPath = NTBNormalizer.Properties.Settings.Default.dirErrorPath;





        /// <summary>
        /// This method converts the incomming XML-file to NITF
        /// (or what ever format NTB is using)
        /// </summary>
        /// <param name="fileXSL"></param>
        /// <param name="fileXML"></param>
        /// <param name="fileOutput"></param>
        public void XSLConverter(string fileXSL, string fileXML, string fileOutput)
        {
            // Creating a new XslTransform object
            XslCompiledTransform trans = new XslCompiledTransform();

            // Load the XSL-file
            trans.Load(fileXSL);

            // Transform the XML-file
            trans.Transform(fileXML, fileOutput);


        }

        /// <summary>
        /// This method converts the incomming XML-file to NITF
        /// (or what ever format NTB is using)
        /// </summary>
        /// <param name="fileXSL"></param>
        /// <param name="fileXML"></param>
        public void XSLConverter(string fileXSL, FileInfo[] fileXML)
        {
            MemoryStream Transformed = new MemoryStream();

            XmlDocument doc = new XmlDocument();


            // Creating a new XslTransform object
            XslCompiledTransform trans = new XslCompiledTransform();

            // Load the XSL-file
            // Creating the full path to the fileXSL-file

            try
            {
                trans.Load(fileXSL);
            }
            catch (XmlException xe)
            {
                Console.WriteLine(xe.Message.ToString());
                Console.WriteLine(xe.StackTrace.ToString());
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
                Console.WriteLine(e.StackTrace.ToString());
            }
            // Output directory - placed here by accident (during testing)
            string OutPath = NTBNormalizer.Properties.Settings.Default.dirOutput + @"\";


            // Creating a fileOutput name
            foreach (FileInfo strXML in fileXML)
            {

                // input = XmlReader.Create(strXML.Name);

                if (strXML.Exists)
                {
                    Console.WriteLine("Exists!");
                }

                try
                {
                    trans.Transform(strXML.FullName, OutPath + strXML.Name);
                }
                catch (XmlException xe)
                {
                    Console.WriteLine(xe.Message.ToString());
                    Console.WriteLine(xe.StackTrace.ToString());

                    // Moving file
                }

                catch (Exception e)
                {
                    Console.WriteLine(e.Message.ToString());
                    Console.WriteLine(e.StackTrace.ToString());

                    // Moving file
                }

                Transformed.Position = 0;
            }


        }



        /// <summary>
        /// This method shall convert from CSV to XML. 
        /// What we do read the XML-template and fill this with 
        /// the information from the csv-file
        /// </summary>
        /// <param name="fileXML">string containing the name of the XML-template</param>
        /// <param name="fileOutput">string containing the name of the XML-output file</param>
        public void CSVConverter(string fileXML, string fileOutput)
        {
            // We need to read the first line to get the different XML-tagnames

            // Then we need to loop through the rest of the file to get the data and 
            // save the document as XML.



        }

        /// <summary>
        /// This function uses the AltovaXML library to convert all files in a directory using XSLT 2.0 
        /// </summary>
        /// <param name="fileXSL"></param>
        /// <param name="fileXML"></param>
        public void XSLAltovaConverter(string fileXSL, FileInfo[] fileXML, ArrayList dirOutput)
        {

            foreach (FileInfo strXML in fileXML)
            {

                try
                {

                    // Creating a new AltovaXML Application
                    AltovaXMLLib.ApplicationClass appXML = new AltovaXMLLib.ApplicationClass();
                    appXML.XQuery.OutputEncoding = "ISO-8859-1";

                    appXML.XSLT2.InputXMLFileName = strXML.FullName.ToString();

                    appXML.XSLT2.XSLFileName = fileXSL.ToString();

                    // appXML.XSLT2.Execute(dirOutput + @"\" + strXML.ToString());
                    string strResult = appXML.XSLT2.ExecuteAndGetResultAsString();

                    int i = 0;

                    for (i = 0; i <= dirOutput.Count - 1; i++)
                    {

                        string strFileout = dirOutput + @"\" + strXML.ToString();

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(strResult);

                        XmlTextWriter xmlTW = new XmlTextWriter(strFileout, Encoding.GetEncoding("ISO-8859-1"));
                        xmlTW.Formatting = Formatting.Indented;

                        xmlDoc.Save(xmlTW);
                    }

                }

                catch (UriFormatException uxse)
                {
                    string strMessage = uxse.Message.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                    strMessage = uxse.StackTrace.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                }

                catch (XsltCompileException xse)
                {
                    string strMessage = xse.Message.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                    strMessage = xse.StackTrace.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                }

                catch (XmlException xe)
                {
                    string strMessage = xe.Message.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                    strMessage = xe.StackTrace.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);


                }

                catch (Exception e)
                {

                    string strMessage = e.Message.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                    strMessage = e.StackTrace.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                }
            }





        }

        /// <summary>
        /// This function uses the AltovaXML library to convert all files in a directory using XSLT 2.0 
        /// </summary>
        /// <param name="fileXSL"></param>
        /// <param name="fileXML"></param>
        public bool XSLAltovaConverter(string fileXSL, FileInfo fileXML, ArrayList dirOutput, ArrayList arrFileEncoding, string strOutputDocType)
        {
            string strLogPath = NTBNormalizer.Properties.Settings.Default.dirLogPath;
            try
            {

                string strMessage = "";
                strMessage = "Start converting file " + fileXML.FullName.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);
                // Creating a new AltovaXML Application
                // ApplicationClass appXML = new AltovaXMLLib.ApplicationClass();
                Altova.AltovaXML.ApplicationClass AltovaXML =
                    new Altova.AltovaXML.ApplicationClass();


                // Checks if the file is well formed
                Altova.AltovaXML.IXMLValidator XMLValidator =
                    AltovaXML.XMLValidator;

                // Get the file to validate
                XMLValidator.InputXMLFileName = fileXML.FullName.ToString();




                if (XMLValidator.IsWellFormed())
                {

                    // Defining the XML file
                    AltovaXML.XSLT2.InputXMLFileName = fileXML.FullName.ToString();

                    // Setting the XSLT-file
                    AltovaXML.XSLT2.XSLFileName = fileXSL.ToString();
                    // Doing the XSL Transformation
                    // AltovaXML.XSLT2.Execute(dirOutput + @"\" + fileXML.ToString());


                    string strResult = AltovaXML.XSLT2.ExecuteAndGetResultAsString();


                    if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                    {
                        strMessage = "Number of dirOutput.Count :" + dirOutput.Count;
                        LogFile.WriteLog(ref dirLogPath, ref strMessage);
                    }



                    int i = 0;
                    string strFileout = "";
                    for (i = 0; i <= dirOutput.Count - 1; i++)
                    {
                        // Get the encoding
                        // Setting it to default utf-8
                        Encoding encEncoding = Encoding.GetEncoding("UTF-8");

                        if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                        {
                            strMessage = "--------------------- COUNTING FILE ENCODING ARRAY ---------------------------";
                            LogFile.WriteLog(ref dirLogPath, ref strMessage);

                            strMessage = "arrFileEncoding.Count: " + arrFileEncoding.Count;
                            LogFile.WriteLog(ref dirLogPath, ref strMessage);

                            strMessage = "--------------------------------------------------------------";
                            LogFile.WriteLog(ref dirLogPath, ref strMessage);

                            strMessage = "---LOOPING -----------------------------------------------------------";
                            LogFile.WriteLog(ref dirLogPath, ref strMessage);
                            foreach (string strFileEncoding in arrFileEncoding)
                            {
                                strMessage = "Content: " + strFileEncoding;


                                LogFile.WriteLog(ref dirLogPath, ref strMessage);
                            }

                            strMessage = "--- END LOOPING -----------------------------------------------------------";
                            LogFile.WriteLog(ref dirLogPath, ref strMessage);

                        }

                        if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                        {
                            if (arrFileEncoding.Count > i)
                            {
                                strMessage = "arrFileEncoding[" + i + "]: " + arrFileEncoding[i].ToString().ToLower();
                                LogFile.WriteLog(ref dirLogPath, ref strMessage);
                            }
                        }

                        if (arrFileEncoding.Count > i)
                        {
                            if (arrFileEncoding[i].ToString().ToLower().Trim() == "iso-8859-1")
                            {
                                if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                                {
                                    strMessage = "Encoding to ISO-8859-1";
                                    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                                }

                                encEncoding = Encoding.GetEncoding("ISO-8859-1");
                            }
                        }

                        // TODO: Using the filename created in the XML-file - now can we get those values?
                        // We are to use the content strResult
                        string strNewFileName = "";
                        try
                        {
                            // Creating the XPath 
                            string strXPath = @"//meta[@name='filename']/@content";

                            StringReader strReader = new StringReader(strResult);

                            // Creating a XmlReader with the content
                            XmlReader xr = XmlReader.Create(strReader);


                            // XPathDocument
                            XPathDocument xpathDoc = new XPathDocument(xr);

                            // Creating the XPath Navigator
                            XPathNavigator xPathNav = xpathDoc.CreateNavigator();

                            // Creating the XPathNodeIterator
                            XPathNodeIterator iterJob = xPathNav.Select(strXPath);

                            // Getting the value out
                            
                            while (iterJob.MoveNext())
                            {
                                // Is this empty then, or ?
                                // this will only return one (and the last one)
                                strNewFileName += iterJob.Current.Value.ToString();
                            }
                            

                            // Creating the XmlDocument object
                            XmlDocument xmlDoc = new XmlDocument();
                        }
                        catch (XmlException xe)
                        {
                            
                            strMessage = xe.Message.ToString();
                            LogFile.WriteLog(ref strLogPath, ref strMessage);
                        }


                        LogFile.WriteLog(ref strLogPath, ref strResult);

                        strFileout = dirOutput[i] + @"\" + strNewFileName; // @"\" + fileXML.ToString();
                        if (strOutputDocType != "")
                        {
                            strFileout = dirOutput[i] + @"\" + strNewFileName.Replace(".xml", 
                                strOutputDocType); // @"\" + fileXML.ToString().Replace(".xml", strOutputDocType);
                        }

                        // Create the filenames to be produced

                        //if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                        //{
                        //    strMessage = "Writing converted file to " + strFileout;
                        //    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                        //}

                        strMessage = "Writing converted file to " + strFileout;
                        LogFile.WriteLog(ref dirLogPath, ref strMessage);

                        MemoryStream stream = new MemoryStream();

                        // Creating the string to be produced
                        StreamWriter StreamWriter = new StreamWriter(stream, encEncoding);

                        StreamWriter.Write(strResult);

                       
                        // Create the XML-document
                        FileStream myFile = new FileStream(strFileout, FileMode.Create);
                        StreamWriter sw = new StreamWriter(myFile, encEncoding);

                        stream.Position = 0;
                        StreamReader sr = new StreamReader(stream, encEncoding);
                        string content = sr.ReadToEnd();

                        sw.Write(strResult);
                        sw.Flush();

                        myFile.Flush();
                        myFile.Close();

                        strMessage = "Done writing file " + strFileout;
                        LogFile.WriteLog(ref dirLogPath, ref strMessage);


                        //XmlDocument xmlDoc = new XmlDocument();
                        //xmlDoc.LoadXml(strResult);

                        //// Saving the XML-document
                        //xmlDoc.Save(xmlTW);



                    }




                    return true;
                }
                else
                {
                    // we shall move the file to the error folder and we shall write to the logfile

                    System.Runtime.InteropServices.Marshal.ReleaseComObject(XMLValidator);
                    XMLValidator = null;

                    System.Runtime.InteropServices.Marshal.ReleaseComObject(AltovaXML);
                    AltovaXML = null;

                    strMessage = "The content in file " + fileXML.FullName.ToString() + " was not well-formed";
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                    // We are sending an e-mail as well, if we are supposed to that is
                    if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                    {
                        strMessage = "We must wait two seconds before we can move the file to the error folder";
                        LogFile.WriteLog(ref dirLogPath, ref strMessage);
                    }
                    System.Threading.Thread.Sleep(4000);

                    FileInfo xmlFile = new FileInfo(fileXML.FullName.ToString());

                    string filename = "This is the FullName: " + fileXML.FullName.ToString();
                    string strErrorPath = dirErrorPath + @"\" + xmlFile.Name;
                    filename = "This is the Name: " + fileXML.Name.ToString();

                    if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                    {
                        LogFile.WriteLog(ref dirLogPath, ref filename);
                        LogFile.WriteLog(ref dirLogPath, ref filename);
                        LogFile.WriteLog(ref dirLogPath, ref strErrorPath);
                    }

                    if (File.Exists(strErrorPath))
                    {
                        File.Delete(strErrorPath);
                    }

                    // Moving the file
                    xmlFile.MoveTo(strErrorPath);

                    if (NTBNormalizer.Properties.Settings.Default.SendErrorEmail == true)
                    {
                        string strFrom = "NORM@ntb.no";
                        string strTo = NTBNormalizer.Properties.Settings.Default.EmailReceiver;

                        MailMessage AttachementMessage = new MailMessage(strFrom, strTo);
                        AttachementMessage.Subject = "Error in NTB Normalizer";

                        AttachementMessage.Body = "There was an error with the file " + dirErrorPath + @"\" + fileXML.Name.ToString() + "\r\n";
                        AttachementMessage.Body = "The error is: " + XMLValidator.LastErrorMessage.ToString();
                        AttachementMessage.IsBodyHtml = false;

                        Attachment attachment = new Attachment(dirErrorPath + @"\" + xmlFile.Name.ToString());

                        AttachementMessage.Attachments.Add(attachment);

                        string smtpserver = NTBNormalizer.Properties.Settings.Default.SMTPServer;

                        SmtpClient client = new SmtpClient(smtpserver);

                        client.Send(AttachementMessage);
                    }
                    return false;

                }



            }

            catch (UriFormatException uxse)
            {

                string strMessage = "And UriFormatException has occured: " + uxse.Message.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                strMessage = uxse.Message.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                strMessage = uxse.StackTrace.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                string strErrorPath = dirErrorPath + @"\" + fileXML.Name.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strErrorPath);
                if (File.Exists(strErrorPath))
                {
                    File.Delete(strErrorPath);
                }

                fileXML.MoveTo(strErrorPath);

                strMessage = "Moving file to " + strErrorPath;
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                if (NTBNormalizer.Properties.Settings.Default.SendErrorEmail == true)
                {
                    string strFrom = "NORM@ntb.no";
                    string strTo = NTBNormalizer.Properties.Settings.Default.EmailReceiver;

                    MailMessage AttachementMessage = new MailMessage(strFrom, strTo);
                    AttachementMessage.Subject = "Error in NTB Normalizer";

                    AttachementMessage.Body = "There was an error with the file " + dirErrorPath + @"\" + fileXML.Name.ToString() + "\r\n";
                    AttachementMessage.Body = "The error is: " + uxse.Message.ToString();
                    AttachementMessage.Body = "StackTrace: " + uxse.StackTrace.ToString();
                    AttachementMessage.IsBodyHtml = false;

                    Attachment attachment = new Attachment(dirErrorPath + @"\" + fileXML.Name.ToString());

                    AttachementMessage.Attachments.Add(attachment);

                    string smtpserver = NTBNormalizer.Properties.Settings.Default.SMTPServer;

                    SmtpClient client = new SmtpClient(smtpserver);

                    client.Send(AttachementMessage);
                }

                return false;

            }

            catch (XsltCompileException xse)
            {
                string strMessage = "And XSLTCompileException has occured: " + xse.Message.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                strMessage = xse.StackTrace.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                strMessage = "Moving file to " + dirErrorPath.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                string strErrorPath = dirErrorPath + @"\" + fileXML.Name.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strErrorPath);
                if (File.Exists(strErrorPath))
                {
                    File.Delete(strErrorPath);
                }

                fileXML.MoveTo(strErrorPath);

                strMessage = "Moving file to " + strErrorPath;
                LogFile.WriteLog(ref dirLogPath, ref strMessage);


                if (NTBNormalizer.Properties.Settings.Default.SendErrorEmail == true)
                {
                    string strFrom = "NORM@ntb.no";
                    string strTo = NTBNormalizer.Properties.Settings.Default.EmailReceiver;

                    MailMessage AttachementMessage = new MailMessage(strFrom, strTo);
                    AttachementMessage.Subject = "Error in NTB Normalizer";

                    AttachementMessage.Body = "There was an error with the file " + dirErrorPath + @"\" + fileXML.Name.ToString() + "\r\n";
                    AttachementMessage.Body = "The error is: " + xse.Message.ToString();
                    AttachementMessage.Body = "StackTrace: " + xse.StackTrace.ToString();
                    AttachementMessage.IsBodyHtml = false;

                    Attachment attachment = new Attachment(dirErrorPath + @"\" + fileXML.Name.ToString());

                    AttachementMessage.Attachments.Add(attachment);

                    string smtpserver = NTBNormalizer.Properties.Settings.Default.SMTPServer;

                    SmtpClient client = new SmtpClient(smtpserver);

                    client.Send(AttachementMessage);
                }

                return false;
            }

            catch (XmlException xe)
            {
                string strMessage = "An XML-Exception has occured: " + xe.Message.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                strMessage = xe.StackTrace.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                strMessage = "Moving file to " + dirErrorPath.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                string strErrorPath = dirErrorPath + @"\" + fileXML.Name.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strErrorPath);
                if (File.Exists(strErrorPath))
                {
                    File.Delete(strErrorPath);
                }

                fileXML.MoveTo(strErrorPath);

                strMessage = "Moving file to " + strErrorPath;
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                if (NTBNormalizer.Properties.Settings.Default.SendErrorEmail == true)
                {
                    string strFrom = "NORM@ntb.no";
                    string strTo = NTBNormalizer.Properties.Settings.Default.EmailReceiver;

                    MailMessage AttachementMessage = new MailMessage(strFrom, strTo);
                    AttachementMessage.Subject = "Error in NTB Normalizer";

                    AttachementMessage.Body = "There was an error with the file " + dirErrorPath + @"\" + fileXML.Name.ToString() + "\r\n";
                    AttachementMessage.Body = "The error is: " + xe.Message.ToString();
                    AttachementMessage.Body = "StackTrace: " + xe.StackTrace.ToString();
                    AttachementMessage.IsBodyHtml = false;

                    Attachment attachment = new Attachment(dirErrorPath + @"\" + fileXML.Name.ToString());

                    AttachementMessage.Attachments.Add(attachment);

                    string smtpserver = NTBNormalizer.Properties.Settings.Default.SMTPServer;

                    SmtpClient client = new SmtpClient(smtpserver);

                    client.Send(AttachementMessage);
                }
                return false;
            }

            catch (IOException ioe)
            {

                string strMessage = "An IO-Exception has occured: " + ioe.Message.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                strMessage = ioe.StackTrace.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                //if (ioe.InnerException.Message.ToString() != "")
                //{
                //    strMessage = "An IO-InnerException has occured: " + ioe.InnerException.Message.ToString();
                //    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                //}

                strMessage = "Moving file to " + dirErrorPath.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                string strErrorPath = dirErrorPath + @"\" + fileXML.Name.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strErrorPath);
                if (File.Exists(strErrorPath))
                {
                    File.Delete(strErrorPath);
                }

                fileXML.MoveTo(strErrorPath);

                strMessage = "Moving file to " + strErrorPath;
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                if (NTBNormalizer.Properties.Settings.Default.SendErrorEmail == true)
                {
                    string strFrom = "NORM@ntb.no";
                    string strTo = NTBNormalizer.Properties.Settings.Default.EmailReceiver;

                    MailMessage AttachementMessage = new MailMessage(strFrom, strTo);
                    AttachementMessage.Subject = "Error in NTB Normalizer";

                    AttachementMessage.Body = "There was an error with the file " + dirErrorPath + @"\" + fileXML.Name.ToString() + "\r\n";
                    AttachementMessage.Body = "The error is: " + ioe.Message.ToString();
                    AttachementMessage.Body = "StackTrace: " + ioe.StackTrace.ToString();
                    AttachementMessage.IsBodyHtml = false;

                    Attachment attachment = new Attachment(dirErrorPath + @"\" + fileXML.Name.ToString());

                    AttachementMessage.Attachments.Add(attachment);

                    string smtpserver = NTBNormalizer.Properties.Settings.Default.SMTPServer;

                    SmtpClient client = new SmtpClient(smtpserver);

                    client.Send(AttachementMessage);
                }
                return false;
            }

            catch (SmtpException se)
            {
                string strMessage = "An SMTP-Exception has occured: " + se.Message.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                if (se.InnerException.Message.ToString() != "")
                {
                    strMessage = "An SMTP-InnerException has occured: " + se.InnerException.Message.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                }

                strMessage = "Moving file to " + dirErrorPath.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                string strErrorPath = dirErrorPath + @"\" + fileXML.Name.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strErrorPath);
                if (File.Exists(strErrorPath))
                {
                    File.Delete(strErrorPath);
                }

                fileXML.MoveTo(strErrorPath);

                strMessage = "Moving file to " + strErrorPath;
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                if (NTBNormalizer.Properties.Settings.Default.SendErrorEmail == true)
                {
                    string strFrom = "NORM@ntb.no";
                    string strTo = NTBNormalizer.Properties.Settings.Default.EmailReceiver;

                    MailMessage AttachementMessage = new MailMessage(strFrom, strTo);
                    AttachementMessage.Subject = "Error in NTB Normalizer";

                    AttachementMessage.Body = "There was an error with the file " + dirErrorPath + @"\" + fileXML.Name.ToString() + "\r\n";
                    AttachementMessage.Body = "The error is: " + se.Message.ToString();
                    AttachementMessage.Body = "StackTrace: " + se.StackTrace.ToString();
                    AttachementMessage.IsBodyHtml = false;

                    Attachment attachment = new Attachment(dirErrorPath + @"\" + fileXML.Name.ToString());

                    AttachementMessage.Attachments.Add(attachment);

                    string smtpserver = NTBNormalizer.Properties.Settings.Default.SMTPServer;

                    SmtpClient client = new SmtpClient(smtpserver);

                    client.Send(AttachementMessage);
                }
                return false;
            }

            catch (Exception e)
            {

                string strMessage = "An Exception has occured: " + e.Message.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                strMessage = e.StackTrace.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                strMessage = "Moving file to " + dirErrorPath.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                string strErrorPath = dirErrorPath + @"\" + fileXML.Name.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strErrorPath);
                if (File.Exists(strErrorPath))
                {
                    File.Delete(strErrorPath);
                }

                fileXML.MoveTo(strErrorPath);

                strMessage = "Moving file to " + strErrorPath;
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                if (NTBNormalizer.Properties.Settings.Default.SendErrorEmail == true)
                {
                    string strFrom = "NORM@ntb.no";
                    string strTo = NTBNormalizer.Properties.Settings.Default.EmailReceiver;

                    MailMessage AttachementMessage = new MailMessage(strFrom, strTo);
                    AttachementMessage.Subject = "Error in NTB Normalizer";

                    AttachementMessage.Body = "There was an error with the file " + dirErrorPath + @"\" + fileXML.Name.ToString() + "\r\n";
                    AttachementMessage.Body = "The error is: " + e.Message.ToString();
                    AttachementMessage.Body = "StackTrace: " + e.StackTrace.ToString();
                    AttachementMessage.IsBodyHtml = false;

                    Attachment attachment = new Attachment(dirErrorPath + @"\" + fileXML.Name.ToString());

                    AttachementMessage.Attachments.Add(attachment);

                    string smtpserver = NTBNormalizer.Properties.Settings.Default.SMTPServer;

                    SmtpClient client = new SmtpClient(smtpserver);

                    client.Send(AttachementMessage);
                }
                return false;
            }

        }

    }
}
