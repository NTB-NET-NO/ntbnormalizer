﻿namespace NTB.Normalizer.Service
{
    partial class FormDebug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._startupConfigTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // _startupConfigTimer
            // 
            this._startupConfigTimer.Interval = 1000;
            this._startupConfigTimer.Tick += new System.EventHandler(this._startupConfigTimer_Tick);
            // 
            // FormDebug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Name = "FormDebug";
            this.Text = "FormDebug - NTB Normalizer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormDebug_FormClosing);
            this.Load += new System.EventHandler(this.FormDebug_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer _startupConfigTimer;
    }
}