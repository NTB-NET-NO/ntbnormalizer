﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NTBNormalizerService.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

// Creating support for logging

// Adding reference to components

using NTB.Normalizer.Components.Components;

namespace NTB.Normalizer.Service
{
    using System;
    using System.ServiceProcess;
    using System.Timers;

    using log4net;
    using log4net.Config;

    using NTB.Normalizer.Components;

    /// <summary>
    /// The ntb normalizer service.
    /// </summary>
    public partial class NtbNormalizerService : ServiceBase
    {
        #region Static Fields

        /// <summary>
        /// The _logger.
        /// </summary>
        private static ILog _logger = LogManager.GetLogger(typeof(NtbNormalizerService));

        #endregion

        #region Fields

        /// <summary>
        /// The service.
        /// </summary>
        protected MainServiceComponent Service = null;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NtbNormalizerService"/> class.
        /// </summary>
        public NtbNormalizerService()
        {
            ThreadContext.Properties["JOBNAME"] = "NTB.Normalizer.Service";
            XmlConfigurator.Configure();

            _logger = LogManager.GetLogger(typeof(NtbNormalizerService));

            // Creating the new MainServiceComponent
            this.Service = new MainServiceComponent();

            // Initialising this main component
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method starts the whole shebang. It will create the different job components
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected override void OnStart(string[] args)
        {
            try
            {
                _logger.Info("NTB NormalizerService starting...");
                this._startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTB NormalizerService DID NOT START properly - Terminating!", ex);
                throw;
            }
        }

        /// <summary>
        ///     This method fires when the service stops.
        ///     @todo: This method must work with the closing event in FormDebug
        /// </summary>
        protected override void OnStop()
        {
            try
            {
                _logger.Info("Stopping service");
                this.Service.Stop();

                this._startupConfigTimer.Enabled = false;
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTB NormalizerService did not stop properly - Terminating!", ex);
                throw;
            }
        }

        /// <summary>
        /// The _startup config timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void _startupConfigTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this._startupConfigTimer.Stop();

            try
            {
                this.Service.Configure();
                this.Service.Start();
                _logger.Info("NTB NormalizerService Started!");
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTB NormalizerService DID NOT START properly - TERMINATING!", ex);

                throw;
            }
        }

        #endregion
    }
}