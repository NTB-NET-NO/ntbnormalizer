﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;
using System.Configuration;

namespace NTB.Normalizer.Service
{
 
    

    /// <summary>
    /// This class shall deal with folders and the different 
    /// jobs that are in these folders
    /// </summary>
    class Folder
    {
        /// <summary>
        /// Array holding the names of the XSLT-folders
        /// </summary>
        public DirectoryInfo[] JobDirectories;

        /// <summary>
        /// Array holding the names of the XML-folders
        /// </summary>
        public DirectoryInfo[]  InputDirectories;

        /// <summary>
        /// Array holding the names of the CSV-folders
        /// </summary>
        public DirectoryInfo[] CSVInputDirectories;
             

        /// <summary>
        /// This method gets the names in the root folder
        /// </summary>
        public Folder()
        {
            try
            {
                // Get the root directory for XSLT and XML folders
                string xsltroot = ConfigurationManager.AppSettings["dirXSLTRoot"];
                string xmlroot = ConfigurationManager.AppSettings["dirXMLRoot"]; 
                string dirOutput = ConfigurationManager.AppSettings["dirOutput"]; 
                string csvroot = ConfigurationManager.AppSettings["dirCSVRoot"]; 

                // Getting the different folders in the root folder (xslt)
                DirectoryInfo diXSLT = new DirectoryInfo(xsltroot);
                DirectoryInfo diXML = new DirectoryInfo(xmlroot);
                DirectoryInfo diOutput = new DirectoryInfo(dirOutput);
                DirectoryInfo diCSV = new DirectoryInfo(dirOutput);

                // Check if they exists. If they don't, we shall create them
                CheckFolderExists(xsltroot);
                CheckFolderExists(xmlroot);
                CheckFolderExists(dirOutput);
                CheckFolderExists(csvroot);


                // Populating the list with directories 
                JobDirectories = diXSLT.GetDirectories().ToArray();
                InputDirectories = diXML.GetDirectories().ToArray();
                CSVInputDirectories = diCSV.GetDirectories().ToArray();



                // we are looping through the folders in the Input directory
                foreach (DirectoryInfo InputDir in InputDirectories)
                {
                    // this is here only for debugging 
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]) == true) 
                    {
                        string strLogDir = ConfigurationManager.AppSettings["dirLogPath"]; 
                        string strMessage = "JobUtilities: " + InputDir;
                        
                        
                    }

                    // We are checking if the directory is in the XSLT-root.
                    // If it is not, we shall remove it.
                    if (JobDirectories.Contains(InputDir) == true)
                    {
                        // this is here only for debugging 
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]) == true) 
                        {
                            string strLogDir = ConfigurationManager.AppSettings["dirLogPath"];
                            string strMessage = xmlroot + @"\" + InputDir;
                            

                        }
                        

                        DirectoryInfo di = new DirectoryInfo(xmlroot + @"\" + InputDir);
                        di.Delete();
                    }

                }


                // Check if the directories exists under XML-root
                foreach (DirectoryInfo JobDir in JobDirectories)
                {

                    // this is here only for debugging 
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]) == true)
                    {
                        string strLogDir = ConfigurationManager.AppSettings["dirLogPath"];
                        string strMessage = "directory: " + JobDir;
                    }
                    

                    // We are checking if the directory is in the InputDirectories array
                    if (!InputDirectories.Contains(JobDir))
                    {
                        DirectoryInfo di = new DirectoryInfo(xmlroot + @"\" + JobDir);
                        di.Create();
                    }

                }


                // we are looping through the folders in the Input directory
                foreach (DirectoryInfo InputDir in CSVInputDirectories)
                {
                    // this is here only for debugging 
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]) == true)
                    {
                        string strLogDir = ConfigurationManager.AppSettings["dirLogPath"];
                        string strMessage = "JobUtilities: " + InputDir;
                        

                    }

                    // We are checking if the directory is in the XSLT-root.
                    // If it is not, we shall remove it.
                    if (JobDirectories.Contains(InputDir) == true)
                    {
                        // this is here only for debugging 
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]) == true)
                        {
                            string strLogDir = ConfigurationManager.AppSettings["dirLogPath"];
                            string strMessage = csvroot + @"\" + InputDir;
                            

                        }


                        DirectoryInfo di = new DirectoryInfo(csvroot + @"\" + InputDir);
                        di.Delete();
                    }

                }
            }
            catch (IOException iox)
            {
                Console.WriteLine(iox.Message.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }


        }

        /// <summary>
        /// This method is just a wrapper for the xsltdi.exists method
        /// </summary>
        /// <param name="folder">The folder we shall check if exists</param>
        public void CheckFolderExists(string folder)
        {
            DirectoryInfo di = new DirectoryInfo(folder);
            if (!di.Exists)
            {
                di.Create();
            }
        }
        
    }
}
