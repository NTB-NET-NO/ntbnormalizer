﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceStart.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The service start.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.Normalizer.Service
{
    using System.Configuration;
    using System.ServiceProcess;

    using log4net;

    /// <summary>
    /// The service start.
    /// </summary>
    public class ServiceStart
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NtbNormalizerService));

        /// <summary>
        /// The dir error path.
        /// </summary>
        public string DirErrorPath = ConfigurationManager.AppSettings["dirErrorPath"];

        /// <summary>
        /// The dir log path.
        /// </summary>
        public string DirLogPath = ConfigurationManager.AppSettings["dirLogPath"];

        /// <summary>
        /// The dir root path.
        /// </summary>
        public string DirRootPath = ConfigurationManager.AppSettings["dirPathRoot"];

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        private static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            MDC.Set("JOBNAME", "NTB.Normalizer.Service");
            Logger.Debug("Starting up the application");

            ServiceBase[] servicesToRun = new ServiceBase[] { new NtbNormalizerService() };
            ServiceBase.Run(servicesToRun);
        }
    }
}