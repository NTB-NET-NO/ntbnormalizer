﻿using System;

namespace NTB.Normalizer.Components.Exceptions
{
    public class XsltFileException : Exception
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="XsltFileException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public XsltFileException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XsltFileException"/> class.
        /// </summary>
        /// <param name="message">
        /// The error message.
        /// </param>
        /// <param name="exception">
        /// INternal exception.
        /// </param>
        public XsltFileException(string message, Exception exception)
            : base(message, exception)
        {
        }

        #endregion
    }
}