﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentTransformer.cs" company="Norsk Telegrambyrå AS">
//   Copyright (C) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The document transformer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Xml;
using log4net;
using NTB.Normalizer.Components.Interfaces;
using NTB.Normalizer.Components.Utilities;
using Saxon.Api;

namespace NTB.Normalizer.Components.Transformers
{
    /// <summary>
    ///     The document transformer.
    /// </summary>
    public class DocumentTransformer : IDocumentTransformer
    {

        /// <summary>
        /// The transformer.
        /// </summary>
        private readonly XsltTransformer _transformer;

        /// <summary>
        ///     The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(DocumentTransformer));

        public string PathErrorFolder { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentTransformer"/> class.
        /// </summary>
        /// <param name="transformer">
        /// The transformer.
        /// </param>
        public DocumentTransformer(XsltTransformer transformer)
        {
            this._transformer = transformer;
        }

        /// <summary>
        ///     Gets or sets a value indicating whether iptc sequence number.
        /// </summary>
        public bool IptcSequenceNumber { get; set; }

        /// <summary>
        ///     Gets or sets the iptc sequence number file.
        /// </summary>
        public string IptcSequenceNumberFile { get; set; }

        /// <summary>
        ///     Gets or sets the iptc sequence number min.
        /// </summary>
        public int IptcSequenceNumberMin { get; set; }

        /// <summary>
        ///     Gets or sets the iptc sequence number max.
        /// </summary>
        public int IptcSequenceNumberMax { get; set; }

        /// <summary>
        ///     Gets or sets the iptc sequence value.
        /// </summary>
        public int IptcSequenceValue { get; set; }

        /// <summary>
        /// The transform document.
        /// </summary>
        /// <param name="xmlFile">
        /// The xml file.
        /// </param>
        /// <param name="fi">
        /// The fi.
        /// </param>
        /// <param name="readerSettings">
        /// The reader settings.
        /// </param>
        /// <param name="processor">
        /// The processor.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string TransformDocument(string xmlFile, FileInfo fi, XmlReaderSettings readerSettings, Processor processor)
        {
            using (var reader = XmlReader.Create(fi.FullName, readerSettings))
            {
                try
                {
                    // reading the content of the file and storing it inside an XdmNode
                    var input = processor.NewDocumentBuilder().Build(reader);

                    // Run the transformation
                    Logger.Info("Run the transformation");
                    if (this._transformer == null)
                    {
                        Logger.Info("No transformer created. Need to return");
                        return string.Empty;
                    }
                    if (this._transformer.InitialContextNode == null)
                    {
                        
                        this._transformer.InitialContextNode = new XdmNode();
                    }

                    this._transformer.InitialContextNode = input;

                    // We need to find out if we are to create an IPTC-sequence-number or not
                    if (this.IptcSequenceNumber)
                    {
                        this.GetIptcSequenceNumber();
                    }

                    // Now we are running the transformer
                    using (var sw = new StringWriter())
                    {
                        try
                        {
                            var serializer = new Serializer();
                            serializer.SetOutputWriter(sw);

                            this._transformer.Run(serializer);

                            return sw.ToString();
                        }
                        catch (Exception exception)
                        {
                            ErrorLogger.Error(exception);

                            // move file to error folder
                            FileUtilities.PathError = this.PathErrorFolder;
                            FileUtilities.MoveErrorFile(xmlFile);

                            // Returning an empty string
                            return string.Empty;
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("An error has occured. Check the error log for more details");
                    ErrorLogger.Error(exception);
                    FileUtilities.PathError = this.PathErrorFolder;
                    FileUtilities.MoveErrorFile(fi.FullName);

                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// The get iptc sequence number.
        /// </summary>
        private void GetIptcSequenceNumber()
        {
            var fileInfo = new FileInfo(this.IptcSequenceNumberFile);
            if (!fileInfo.Exists)
            {
                return;
            }

            // Now we have to read the value of the Sequence number
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(this.IptcSequenceNumberFile);
            var node = xmlDocument.SelectSingleNode("/Ntb/Iptc/SequenceNumber");
            var dt = new DateTime();
            if (node != null && node.Attributes != null)
            {
                dt = Convert.ToDateTime(node.Attributes["date"].Value);

                Logger.Debug("Value of the date attribute: " + dt);
            }

            // Logger.Debug("Value of the node SequenceNumber: " + node.InnerText);
            if (node != null)
            {
                Logger.Debug("Value of the node SequenceNumber: " + node.InnerText);
                this.IptcSequenceValue = Convert.ToInt32(node.InnerText);
            }

            if (dt.Date < DateTime.Today)
            {
                this.IptcSequenceValue = this.IptcSequenceNumberMin;
            }

            var qname = new QName("IptcSequenceNumber");

            var xdmAtomicValue = new XdmAtomicValue(this.IptcSequenceValue);
            var xdmValue = new XdmValue(xdmAtomicValue);
            this._transformer.SetParameter(qname, xdmValue);

            // And now we have to save the file
            this.IptcSequenceValue += 1;

            // We check if the iptc-sequence number is bigger or equal to max value
            // This means: If range is from 9000 to 10000, we are actually using values 9000-9999
            if (this.IptcSequenceValue >= this.IptcSequenceNumberMax)
            {
                // It was, so we are setting it to the min value
                this.IptcSequenceValue = this.IptcSequenceNumberMin;
            }

            // Setting the new value
            if (node != null)
            {
                node.InnerText = this.IptcSequenceValue.ToString();
                if (node.Attributes != null)
                {
                    var xmlAttribute = node.Attributes["date"];
                    if (xmlAttribute != null)
                    {
                        xmlAttribute.Value = DateTime.Today.ToShortDateString();
                    }
                }
            }

            // Saving the document
            xmlDocument.Save(this.IptcSequenceNumberFile);
        }
    }
}