﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentTransformer.cs" company="Norsk Telegrambyrå AS">
//   Copyright (C) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The document transformer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using log4net;
using NTB.Normalizer.Components.Utilities;

namespace NTB.Normalizer.Components.Transformers
{
    /// <summary>
    ///     The document transformer.
    /// </summary>
    public class DotNetDocumentTransformer
    {

        /// <summary>
        /// The transformer.
        /// </summary>
        private XslCompiledTransform _transformer;

        /// <summary>
        ///     The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(DotNetDocumentTransformer));

        public string PathErrorFolder { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentTransformer"/> class.
        /// </summary>
        /// <param name="transformer">
        /// The transformer.
        /// </param>
        public DotNetDocumentTransformer(XslCompiledTransform transformer)
        {
            this._transformer = transformer;
        }

        /// <summary>
        ///     Gets or sets the xslt file to be used for the transformation
        /// </summary>
        public string XsltFile { get; set; }

        /// <summary>
        /// The transform document.
        /// </summary>
        /// <param name="xmlFile">
        /// The xml file.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string TransformDocument(string xmlFile)
        {
            try
            {
                var secureResolver = new XmlSecureResolver(new XmlUrlResolver(), this.XsltFile);

                if (this._transformer == null)
                {
                    this._transformer = new XslCompiledTransform();
                }

                this._transformer.Load(this.XsltFile, XsltSettings.TrustedXslt, secureResolver);

                // Creating the xml file
                var xml = new XPathDocument(xmlFile);

                // Setting the writer settings for transformation
                var writerSettings = this._transformer.OutputSettings.Clone();

                // We do not check characters
                writerSettings.CheckCharacters = false;

                // Now we are running the transformer
                using (var sw = new StringWriter())
                {
                    try
                    {
                        this._transformer.Transform(xml, XmlWriter.Create(sw, writerSettings)); // (fullOutputPath, writerSettings));
                        return sw.ToString();
                    }
                    catch (Exception exception)
                    {
                        ErrorLogger.Error(exception);

                        // move file to error folder
                        FileUtilities.PathError = this.PathErrorFolder;
                        FileUtilities.MoveErrorFile(xmlFile);

                        // Returning an empty string
                        return string.Empty;
                    }
                }

            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                FileUtilities.PathError = this.PathErrorFolder;
                FileUtilities.MoveErrorFile(xmlFile);

                return string.Empty;
            }

        }

    }
}