﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Notifyer.cs" company="">
//   
// </copyright>
// <summary>
//   Class to use when wanting to notify someone about something.
//   Preferabely by mail
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.Normalizer.Components
{
    using System;
    using System.Configuration;
    using System.Net.Mail;

    using log4net;

    /// <summary>
    /// Class to use when wanting to notify someone about something.
    /// Preferabely by mail
    /// </summary>
    public class Notifyer
    {
        /// <summary>
        /// The logger object to be used to create the log4net object
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Notifyer));

        /// <summary>
        /// Name of the file that caused the error
        /// </summary>
        public string FileError = string.Empty;

        /// <summary>
        /// Boolean value to say if we are to send HTML mail or not
        /// </summary>
        public bool HtmlBody = false;

        /// <summary>
        /// Refers to the last error message
        /// </summary>
        public string LastErrorMessage = string.Empty;

        /// <summary>
        /// Refers to the name of the file
        /// </summary>
        public string PathError = string.Empty;

        /// <summary>
        /// The send mail.
        /// </summary>
        public void SendMail()
        {
            var strFrom = "NORM@ntb.no";
            var strTo = ConfigurationManager.AppSettings["EmailReceiver"];
            var attachementMessage = new MailMessage(strFrom, strTo)
                                         {
                                             Subject = "Error in NTB Normalizer",
                                             Body = "There was an error with the file " + this.PathError + @"\" + this.FileError + "\r\n"
                                         };

            attachementMessage.Body = "The error is: " + this.LastErrorMessage;
            attachementMessage.IsBodyHtml = false;

            var attachment = new Attachment(this.PathError + @"\" + this.FileError);

            attachementMessage.Attachments.Add(attachment);

            var smtpserver = ConfigurationManager.AppSettings["SMTPServer"];

            try
            {
                var client = new SmtpClient(smtpserver);
            }
            catch (SmtpException se)
            {
                var strMessage = "An SMTP-Exception has occured: " + se.Message;
                Logger.Error(strMessage);

                if (se.InnerException.Message != string.Empty)
                {
                    strMessage = "An SMTP-InnerException has occured: " + se.InnerException.Message;
                    Logger.Error(strMessage);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                Logger.Error(e.StackTrace);

                if (e.InnerException.Message != string.Empty)
                {
                    Logger.Error(e.InnerException.Message);
                }
            }
        }
    }
}