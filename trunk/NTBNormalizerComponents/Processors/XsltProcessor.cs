﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using NTB.Normalizer.Components.Utilities;
using NTB.Normalizer.Utilities;
using log4net;
using NTB.Normalizer.Components.Classes;
using NTB.Normalizer.Components.Components;
using NTB.Normalizer.Components.Converters;

namespace NTB.Normalizer.Components.Processors
{
    public class XsltProcessor
    {
        public ComponentsProperties Properties { get; set; }
        
        #region Private Static Fields

        /// <summary>
        ///     The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(XsltComponent));

        #endregion

        /// <summary>
        /// The process files.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        public void ProcessFiles(FileSystemEventArgs e)
        {
            // TODO: Also try and check for files in the directory after processing all files triggered by the trigger.

            try
            {
                var retries = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertRetries"]);
                var sleep = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertRetryInterval"]);

                ThreadContext.Properties["JOBNAME"] = this.Properties.InstanceName;
                Logger.InfoFormat("XSLTComponent::filesWatcher_Created() hit with file data: {0}", e.FullPath);

                var pollTime = 1000;
                if (Convert.ToInt32(this.Properties.PollDelay) < 1000)
                {
                    pollTime = Convert.ToInt32(this.Properties.PollDelay * 1000);
                }

                Logger.InfoFormat("We are waiting: {0} Seconds before starting to convert!", pollTime / 1000);

                if (pollTime > 0)
                {
                    Thread.Sleep(pollTime);
                }

                this._busyEvent.WaitOne();

                var fileInfo = new FileInfo(e.FullPath);
                var directoryName = fileInfo.DirectoryName;
                if (directoryName != null)
                {
                    this.ProcessDirectoryFiles(directoryName, retries, sleep);
                }

                // Reset event
                this._busyEvent.Set();

                // Making sure the files watcher is listening.

            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                var path = Path.Combine(this.Properties.FileErrorFolder, e.Name);

                var fileInfo = new FileInfo(path);

                if (fileInfo.Exists)
                {
                    fileInfo.Delete();
                }

                fileInfo = new FileInfo(e.FullPath);
                fileInfo.MoveTo(path);
            }
        }


        /// <summary>
        /// The process directory files.
        /// </summary>
        /// <param name="directoryName">
        /// The directory name.
        /// </param>
        /// <param name="retries">
        /// The retries.
        /// </param>
        /// <param name="sleep">
        /// The sleep.
        /// </param>
        public void ProcessDirectoryFiles(string directoryName, int retries, int sleep)
        {
            var fileQueue = new DirectoryInfo(directoryName).GetFiles();

            Logger.InfoFormat("Number of files to be transformed: {0}", fileQueue.Length);

            foreach (var fileInfo in fileQueue)
            {
                var fi = new FileInfo(fileInfo.FullName);

                // If the file does not exists, we don't have to compute it
                if (!fi.Exists)
                {
                    continue;
                }

                // If file is locked we'll hold for a bit
                do
                {
                    FileUtilities.IsFileLocked(fi);
                }
                while (false);

                // = new // = e.Name;
                try
                {
                    var newFile = fi.FullName;
                    this.ProcessFile(newFile, retries, sleep);

                    // We are here if nothing has gone wrong
                    var path = Path.Combine(this.Properties.FileDoneFolder, fi.Name);
                    FileUtilities.MoveDoneFile(path, fi.FullName);
                }
                catch (Exception exception)
                {
                    ErrorLogger.Error(exception);
                    ErrorFunctions.PathError = this.Properties.FileErrorFolder;
                    ErrorFunctions.MoveErrorFile(fileInfo.FullName, this.Properties.FileErrorFolder);
                }
            }
        }

        /// <summary>
        /// The process file.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="retries">
        /// The retries.
        /// </param>
        /// <param name="sleep">
        /// The sleep.
        /// </param>
        public void ProcessFile(string file, int retries, int sleep)
        {
            var fi = new FileInfo(file);
            if (retries < 0)
            {
                // We must move the file to error folder
                var errorFile = new FileInfo(Path.Combine(this.Properties.FileErrorFolder, fi.Name));

                if (errorFile.Exists)
                {
                    errorFile.Delete();

                    fi.MoveTo(Path.Combine(this.Properties.FileErrorFolder, fi.Name));
                }
            }

            for (var i = 0; i <= retries; i++)
            {
                try
                {
                    // Wait for the busy state

                    // Adding files to the list and then sending that list to the converter
                    try
                    {
                        if (!fi.Exists)
                        {
                            return;
                        }

                        Logger.InfoFormat("Converting file {0}", fi.Name);
                        this.Converter(new[] { file }, this.Properties.PathXslt + @"\" + this.Properties.InstanceName + @"\" + this.Properties.InstanceName + ".xsl");

                        // Exit loop here...
                        i = retries;
                    }
                    catch (Exception exception)
                    {
                        if (!fi.Exists)
                        {
                            return;
                        }

                        // Checking if we can read the file or not. 
                        if (i > retries)
                        {
                            continue;
                        }

                        Logger.WarnFormat("Open/Read failed, retrying file: {0} Error: {1} ", file, exception.Message);
                        if (exception.InnerException != null)
                        {
                            Logger.WarnFormat("Open/Read failed, retrying file: {0} Error: {1} ", file, exception.InnerException.Message);
                        }

                        Thread.Sleep(sleep);

                        retries -= 1;

                        // And so we try again.
                        this.ProcessFile(file, retries, sleep);
                    }
                    finally
                    {
                        Logger.InfoFormat("Done converting file {0}", fi.FullName);

                        //var path = Path.Combine(this.FileDoneFolder, fi.Name);

                        //var moveFileInfo = new FileInfo(path);

                        //if (fi.Exists)
                        //{
                        //    if (moveFileInfo.Exists)
                        //    {
                        //        moveFileInfo.Delete();
                        //        fi.MoveTo(path);
                        //    }
                        //}
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.Error("Something happened while doing ProcessFile!", ex);
                    ErrorLogger.Error(ex.Message);
                    ErrorLogger.Error(ex.StackTrace);

                    if (ex.InnerException == null)
                    {
                        continue;
                    }

                    ErrorLogger.Error(ex.InnerException.Message);
                    ErrorLogger.Error(ex.InnerException.StackTrace);
                }
            }
        }



        
        /// <summary>
        /// This method is used to call the converter class
        /// </summary>
        /// <param name="inputFiles">
        /// List of strings/files that are to be converted
        /// </param>
        /// <param name="fileXsl">
        /// The XSLT-document to be used to parse the files
        /// </param>
        public void Converter(IEnumerable<string> inputFiles, string fileXsl)
        {
            Logger.Debug("In Converter - List<string> Files, string FileXSL");

            Logger.Debug("FileXSL: " + fileXsl);

            // Creating the converter
            // ;
            using (var mainXsltConverter = new DocumentConverter())
            {
                // Setting the value of the IptcSequenceNumber Check
                mainXsltConverter.IptcSequenceNumber = this.Properties.IptcSequenceNumber;

                // Setting the value of the IptcSequenceNumberFile
                mainXsltConverter.IptcSequenceNumberFile = this.Properties.IptcSequenceNumberFile;

                mainXsltConverter.IptcSequenceNumberMin = this.Properties.IptcSequenceNumberMin;

                mainXsltConverter.IptcSequenceNumberMax = this.Properties.IptcSequenceNumberMax;

                // Defining the XSLT-file
                mainXsltConverter.FileXsl = fileXsl;

                // defining document output
                mainXsltConverter.OutputDocumentExtensions = this.Properties.DocumentExtensions;

                // Defining output path(s)
                mainXsltConverter.OutputPaths = this.Properties.FileOutputFolders;

                // Defining error folder
                mainXsltConverter.PathError = this.Properties.FileErrorFolder;

                // Defining encoding(s)
                mainXsltConverter.OutputPathAndEncodings = this.Properties.DocumentEncodings;

                mainXsltConverter.JobName = this.Properties.InstanceName;

                mainXsltConverter.PathError = this.Properties.FileErrorFolder;

                /*
                 * We are using this for the time being
                 */
                var enumerable = inputFiles as string[] ?? inputFiles.ToArray();
                if (inputFiles != null && !enumerable.Any())
                {
                    return;
                }

                var processor = mainXsltConverter.CreateProcessor();

                // Creating the XSLT Compile here?
                mainXsltConverter.CreateXsltCompiler(fileXsl, processor);

                Logger.Debug("---------------------------> Starting loop <--------------------------");
                foreach (var filename in enumerable)
                {
                    var fi = new FileInfo(filename);

                    FileUtilities.IsFileLocked(fi);

                    XmlUtilities.CanReadXmlFile(filename);

                    this.Properties.BusyFlag = true;
                    Logger.Debug("Files: " + filename);

                    var transformedFile = mainXsltConverter.DoDocumentTransformation(filename, processor);
                    if (transformedFile == string.Empty)
                    {
                        Logger.InfoFormat("There must have been a problem transforming the document");
                        Logger.InfoFormat("We are moving the file to error folder");

                        if (fi.Exists)
                        {
                            var path = Path.Combine(this.Properties.FileErrorFolder, fi.Name);
                            var errorFileInfo = new FileInfo(path);

                            if (errorFileInfo.Exists)
                            {
                                errorFileInfo.Delete();
                            }

                            fi.MoveTo(path);
                        }

                        continue;
                    }

                    if (transformedFile != string.Empty)
                    {
                        // Now we shall call a new method
                        try
                        {
                            mainXsltConverter.CreateOutputFile(transformedFile, filename);
                        }
                        catch (Exception exception)
                        {
                            Logger.Error(exception);
                            var path = Path.Combine(this.Properties.FileErrorFolder, fi.Name);

                            var errorFileInfo = new FileInfo(path);
                            if (errorFileInfo.Exists)
                            {
                                errorFileInfo.Delete();
                            }

                            // Moving file
                            if (fi.Exists)
                            {
                                fi.MoveTo(path);
                            }
                        }
                        finally
                        {
                            var path = Path.Combine(this.Properties.FileDoneFolder, fi.Name);

                            var fileInfo = new FileInfo(path);
                            if (fileInfo.Exists)
                            {
                                fileInfo.Delete();
                            }

                            // Moving file
                            fi.MoveTo(path);
                        }
                    }

                    this.Properties.BusyFlag = false;
                }

                Logger.Debug("---------------------------> Ending loop <--------------------------");
                this.Properties.BusyFlag = false;
            }
        }

        
        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

    }
}
