﻿using System;
using System.Threading;
using NTB.Normalizer.Components.enums;

namespace NTB.Normalizer.Components.Components
{
    partial class XsltComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pollTimer = new System.Timers.Timer();
            this.filesWatcher = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filesWatcher)).BeginInit();
            // 
            // pollTimer
            // 
            this.pollTimer.Interval = 60000D;
            this.pollTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.PollTimerElapsed);
            // 
            // filesWatcher
            // 
            this.filesWatcher.EnableRaisingEvents = true;
            this.filesWatcher.NotifyFilter = System.IO.NotifyFilters.FileName;
            this.filesWatcher.Created += new System.IO.FileSystemEventHandler(this.FilesWatcherCreated);
            this.filesWatcher.Renamed += new System.IO.RenamedEventHandler(this.FilesWatcherRenamed);
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filesWatcher)).EndInit();

        }

        #endregion

        private System.Timers.Timer pollTimer;

        /// <summary>
        /// The enabled status for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="Enabled"/></remarks>
        protected Boolean enabled;

        /// <summary>
        /// Flag to tell which state the component is in (running/halted)
        /// </summary>
        protected ComponentState componentState;

        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// The polling style for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="PollStyle"/></remarks>
        protected PollStyle pollStyle;
        private System.IO.FileSystemWatcher filesWatcher;
    }
}
