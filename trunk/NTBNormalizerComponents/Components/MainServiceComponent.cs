﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainServiceComponent.cs" company="">
//   
// </copyright>
// <summary>
//   The main service component.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

// Adding support for log4net

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.ServiceModel;
using System.Threading;
using System.Timers;
using System.Xml;
using log4net;
using log4net.Config;
using NTB.Normalizer.Components.enums;
using NTB.Normalizer.Components.Interfaces;

namespace NTB.Normalizer.Components.Components
{
    /// <summary>
    /// The main service component.
    /// </summary>
    public partial class MainServiceComponent : Component
    {
        #region Static Fields

        /// <summary>
        /// The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MainServiceComponent));

        #endregion

        #region Fields

        /// <summary>
        ///     Name of the file that contains the job configuration set
        /// </summary>
        /// <remarks>The name of the job config XML file is stored here.</remarks>
        protected string ConfigSet = string.Empty;

        /// <summary>
        ///     List of currently configured jobs
        /// </summary>
        /// <remarks>Internal Dictionary to keep track of running worker jobs.</remarks>
        protected Dictionary<string, IBaseNormComponents> JobInstances = new Dictionary<string, IBaseNormComponents>();

        /// <summary>
        ///     Notification handler service host
        /// </summary>
        /// <remarks>
        ///     This is the actuall service host that controls the notification object
        /// </remarks>
        protected ServiceHost ServiceHost = null;

        /// <summary>
        ///     Enable watching the config set for changes and reconfigure at runtime
        /// </summary>
        /// <remarks>config setting that defines if we are wathcing the config set for changes/auto reconfigure or not.</remarks>
        protected bool WatchConfigSet = false;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="MainServiceComponent"/> class.
        /// </summary>
        static MainServiceComponent()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainServiceComponent"/> class. 
        ///     Initializes a new instance of the class
        /// </summary>
        public MainServiceComponent()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainServiceComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public MainServiceComponent(IContainer container)
        {
            container.Add(this);

            this.InitializeComponent();
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The configure.
        /// </summary>
        public void Configure()
        {
            try
            {
                // NDC.Push("CONFIG");
                ThreadContext.Stacks["NDC"].Push("CONFIG");

                // Checking if a directory exists or not
                var di = new DirectoryInfo(ConfigurationManager.AppSettings["dirOutput"]);

                if (!di.Exists)
                {
                    di.Create();
                }

                // Read params 
                this.ConfigSet = ConfigurationManager.AppSettings["ConfigurationSet"];
                this.WatchConfigSet = Convert.ToBoolean(ConfigurationManager.AppSettings["WatchConfiguration"]);

                // Logging
                Logger.InfoFormat("{0} : {1}", "ConfigurationSet", this.ConfigSet);
                Logger.InfoFormat("{0} : {1}", "WatchConfiguration", this.WatchConfigSet);

                // Load configuration set
                var config = new XmlDocument();
                config.Load(this.ConfigSet);

                // Setting up the watcher
                this.configFileWatcher.Path = Path.GetDirectoryName(this.ConfigSet);
                this.configFileWatcher.Filter = Path.GetFileName(this.ConfigSet);

                // Create XSLT Components 
                XmlNodeList nodes =
                    config.SelectNodes("/ComponentConfiguration/XSLTComponents/XSLTComponent[@Enabled='True']");
                if (nodes != null)
                {
                    Logger.InfoFormat("NormalizerComponent job instances found: {0}", nodes.Count);
                }

                this.JobInstances.Clear();
                if (nodes != null)
                {
                    foreach (XmlNode nd in nodes)
                    {
                        IBaseNormComponents xsltComponent = new XsltComponent();
                        xsltComponent.Configure(nd);

                        Logger.Debug("Adding " + xsltComponent.InstanceName);
                        this.JobInstances.Add(xsltComponent.InstanceName, xsltComponent);
                    }
                }

                nodes = config.SelectNodes("/ComponentConfiguration/CSVComponents/CSVComponent[@Enabled='True']");

                if (nodes == null)
                {
                    return;
                }

                Logger.InfoFormat("NormalizerComponent job instances found: {0}", nodes.Count);

                // jobInstances.Clear();
                foreach (XmlNode nd in nodes)
                {
                    IBaseNormComponents csvComponent = new CsvComponent();
                    csvComponent.Configure(nd);

                    Logger.Debug("Adding " + csvComponent.InstanceName);
                    this.JobInstances.Add(csvComponent.InstanceName, csvComponent);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                var mailMessage = new MailMessage("505@ntb.no", "thu@ntb.no")
                                      {
                                          Subject = "NTB Normalizer error", 
                                          Body =
                                              "There has been an error in NTB normalizer: "
                                              + exception.Message + "\r\n"
                                              + exception.StackTrace + "\r\n"
                                      };

                var client = new SmtpClient("mail.ntb.no");
                client.Send(mailMessage);

                Logger.Info("We are quitting");
            }
            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
            }
        }

        /// <summary>
        /// The pause.
        /// </summary>
        public void Pause()
        {
            this.maintenanceTimer.Stop();
            this.configFileWatcher.EnableRaisingEvents = false;

            // Stop instances
            foreach (var kvp in this.JobInstances.Where(kvp => kvp.Value.Enabled))
            {
                kvp.Value.Stop();
            }
        }

        /// <summary>
        ///     Starts the main service instance.
        /// </summary>
        /// <remarks>
        ///     The function will set up polling and events to Start all processing jobs.
        /// </remarks>
        public void Start()
        {
            ThreadContext.Stacks["NDC"].Push("START");

            try
            {
                // Start instances
                Logger.Debug("Number of jobs to start: " + this.JobInstances.Count);

                // Looping jobs
                foreach (var kvp in this.JobInstances.Where(kvp => kvp.Value.Enabled))
                {
                    Logger.Info("Starting " + kvp.Value.InstanceName);
                    kvp.Value.Start();
                }

                // Starting maintenance
                Logger.Info("Starting maintenance timer");
                Logger.Debug("Maintenance timer interval: " + this.maintenanceTimer.Interval);
                this.maintenanceTimer.Start();

                // Watch the config file
                this.configFileWatcher.EnableRaisingEvents = this.WatchConfigSet;
            }
            catch (Exception ex)
            {
                Logger.Fatal("An error has occured. Could not Start service. Check errorlog for more information");
                ErrorLogger.Error(ex.Message);
                ErrorLogger.Error(ex.StackTrace);

                if (ex.InnerException != null)
                {
                    ErrorLogger.Error(ex.InnerException.Message);
                    ErrorLogger.Error(ex.InnerException.StackTrace);
                }
            }
            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
            }
        }

        /// <summary>
        /// The stop.
        /// </summary>
        public void Stop()
        {
            this.Pause();

            // Kill notification service handler
            if (this.ServiceHost != null)
            {
                this.ServiceHost.Close();
                this.ServiceHost = null;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reconfigures everything from an updated config set
        /// </summary>
        /// <param name="sender">
        /// The source of the event.
        /// </param>
        /// <param name="e">
        /// File change params
        /// </param>
        /// <remarks>
        /// <para>
        /// When the configuration is updated on disk, this function triggers a complete job reload.
        /// </para>
        /// <para>
        /// Note that app.config is also reloaded, but refreshed config settings only apply to <c>AppSettings&gt;</c> here,
        ///         not the other sections of the config file.
        ///     </para>
        /// </remarks>
        private void configFileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = "MainServiceWorker";

            // Outer-try-final to prevent dupe events
            try
            {
                // Stopping file watcher
                this.configFileWatcher.EnableRaisingEvents = false;
                Logger.Info("MainServiceComponent::configFileWatcher_Changed() hit. Will reconfigure.");

                ThreadContext.Stacks["NDC"].Push("RECONFIGURE");

                // Pause everything
                this.Pause();

                // Give it a little break
                Thread.Sleep(5000);

                // Reconfigure
                ConfigurationManager.RefreshSection("appSettings");
                this.Configure();

                this.Start();
            }
            catch (Exception ex)
            {
                Logger.Fatal("NTBNormalizer reconfiguration failed - TERMINATING! Check error log for more information");
                ErrorLogger.Error(ex.Message);
                ErrorLogger.Error(ex.StackTrace);

                if (ex.InnerException != null)
                {
                    ErrorLogger.Error(ex.InnerException.Message);
                    ErrorLogger.Error(ex.InnerException.StackTrace);
                }

                throw;
            }
            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
                this.configFileWatcher.EnableRaisingEvents = this.WatchConfigSet;
            }
        }

        /// <summary>
        /// The maintenance timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void maintenanceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            MDC.Set("JOBNAME", "MainWorkerJob");

            Logger.Debug("In maintenanceTimer_Elapsed");

            try
            {
                Logger.Debug("Stopping timer");

                this.maintenanceTimer.Stop();

                // We are checking if the jobs are doing what they are supposed to do
                foreach (var kvp in
                    this.JobInstances.Where(kvp => kvp.Value.Enabled))
                {
                    var state = kvp.Value.GetComponentState();

                    if (state != ComponentState.Running)
                    {
                        // Mail.SendMail(string.Format("The component {0} is not running, but is enabled. We will try and restart", keyValuePair.Value.InstanceName));
                        Logger.InfoFormat("The component {0} is not running, but is enabled", kvp.Value.InstanceName);

                        Logger.DebugFormat("We are stopping the component {0}!", kvp.Value.InstanceName);
                        kvp.Value.Stop();

                        Logger.Debug("We are waiting 5 seconds!");
                        Thread.Sleep(5000);

                        // Reconfiguring the component
                        // Logger.DebugFormat("We are re-configuring the component {0}!", kvp.Value.InstanceName);

                        // Starting the component
                        Logger.DebugFormat("We are starting the component {0}!", kvp.Value.InstanceName);
                        kvp.Value.Start();

                        // Mail.SendMail(string.Format("The component {0} has been restarted.", kvp.Value.InstanceName));
                    }
                }


                /*
                 * I shall check if the jobs that are going really are working and running...
                 * 
                 */
                //foreach (var kvp in
                //    this.JobInstances.Where(kvp => kvp.Value.Enabled)
                //        .Where(kvp => kvp.Value.ComponentState == ComponentState.Halted))
                //{
                //    Logger.Error("The component is in Halted state, even though it is enabled!");
                //    Logger.Info("We are starting " + kvp.Value.InstanceName + " component!");
                //    kvp.Value.Start();
                //}


                // Checking if we have any folders with files in them
                // foreach (KeyValuePair<string, IBaseNormComponents> kvp in JobInstances.Where(kvp => kvp.Value.Enabled))
                // {
                // kvp.Value.
                // }
                Logger.Debug("Starting timer");
                this.maintenanceTimer.Start();

                // Watch the config file
                if (!this.configFileWatcher.EnableRaisingEvents)
                {
                    this.configFileWatcher.EnableRaisingEvents = this.WatchConfigSet;
                }
            }
            catch (Exception exception)
            {
                Logger.ErrorFormat("MainServiceComponent::maintenanceTimer_Elapsed(): " + exception.Message, exception);
            }
        }

        #endregion
    }
}