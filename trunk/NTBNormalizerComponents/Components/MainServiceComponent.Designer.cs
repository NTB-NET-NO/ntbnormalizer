﻿namespace NTB.Normalizer.Components.Components
{
    partial class MainServiceComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.maintenanceTimer = new System.Timers.Timer();
            this.configFileWatcher = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.maintenanceTimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.configFileWatcher)).BeginInit();
            // 
            // maintenanceTimer
            // 
            this.maintenanceTimer.Interval = 60000D;
            this.maintenanceTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.maintenanceTimer_Elapsed);
            // 
            // configFileWatcher
            // 
            this.configFileWatcher.EnableRaisingEvents = true;
            this.configFileWatcher.NotifyFilter = System.IO.NotifyFilters.LastWrite;
            this.configFileWatcher.Changed += new System.IO.FileSystemEventHandler(this.configFileWatcher_Changed);
            ((System.ComponentModel.ISupportInitialize)(this.maintenanceTimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.configFileWatcher)).EndInit();

        }

        #endregion

        private System.Timers.Timer maintenanceTimer;
        private System.IO.FileSystemWatcher configFileWatcher;
    }
}
