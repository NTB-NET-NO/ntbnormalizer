﻿using System;
using NTB.Normalizer.Components.enums;

namespace NTB.Normalizer.Components.Components
{
    /// <summary>
    /// The csv component.
    /// </summary>
    partial class CsvComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.filesWatcher = new System.IO.FileSystemWatcher();
            this.pollTimer = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.filesWatcher)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).BeginInit();
            // 
            // filesWatcher
            // 
            this.filesWatcher.EnableRaisingEvents = true;
            this.filesWatcher.NotifyFilter = System.IO.NotifyFilters.FileName;
            this.filesWatcher.Created += new System.IO.FileSystemEventHandler(this.filesWatcher_Created);
            // 
            // pollTimer
            // 
            this.pollTimer.Interval = 60000D;
            this.pollTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.pollTimer_Elapsed);
            ((System.ComponentModel.ISupportInitialize)(this.filesWatcher)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).EndInit();

        }

        #endregion

        private System.IO.FileSystemWatcher filesWatcher;
        private System.Timers.Timer pollTimer;

        /// <summary>
        /// The enabled status for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="Enabled"/></remarks>
        protected Boolean enabled;

        /// <summary>
        /// Flag to tell which state the component is in (running/halted)
        /// </summary>
        protected ComponentState componentState;

        /// <summary>
        /// The polling style for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="PollStyle"/></remarks>
        protected PollStyle pollStyle;
    }
}
