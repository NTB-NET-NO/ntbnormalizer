﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CSVComponent.cs" company="">
//   
// </copyright>
// <summary>
//   The csv component.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Timers;
using System.Xml;
using log4net;
using NTB.Normalizer.Components.Converters;
using NTB.Normalizer.Components.enums;
using NTB.Normalizer.Components.Interfaces;
using NTB.Normalizer.Utilities;

namespace NTB.Normalizer.Components.Components
{
    /// <summary>
    /// The csv component.
    /// </summary>
    public partial class CsvComponent : Component, IBaseNormComponents
    {
        #region Logging

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CsvComponent));

        /// <summary>
        /// The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        #endregion

        #region Common class variables

        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);

        /// <summary>
        /// The configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected string Name;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly configured
        /// </summary>
        /// <remarks>Holds the configured status of the instance. <c>True</c> means successfully configured</remarks>
        protected bool Configured = false;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected bool ErrorRetry = false;

        /// <summary>
        /// PollDelay is used to make sure that the file is ready to be accessed. 
        /// </summary>
        protected int PollDelay = 0;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected string EmailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailBody;

        #endregion

        #region Polling settings

        /// <summary>
        /// The configured schedule for this instance
        /// </summary>
        /// <remarks>This is set to a time of day and is used by the <c>Scheduled</c> pollstyle. Work is triggered at the given time of day.</remarks>
        protected string Schedule;

        /// <summary>
        /// The configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;

        #endregion

        /// <summary>
        /// Input file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter"/> and <see cref="IncludeSubdirs"/>
        /// </remarks>
        protected string FileInputFolder;

        /// <summary>
        /// Output file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter"/> and <see cref="IncludeSubdirs"/>
        /// </remarks>
        protected Dictionary<int, string> FileOutputFolders = new Dictionary<int, string>();

        /// <summary>
        /// Error file folder
        /// </summary>
        /// <remarks>
        ///   <para>File folder where failing files are stored.</para>
        ///   <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        protected string FileErrorFolder;

        /// <summary>
        /// File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///   <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///   <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        protected string FileDoneFolder;

        /// <summary>
        /// Initializes a new instance of the <see cref="CsvComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public CsvComponent(IContainer container)
        {
            container.Add(this);

            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CsvComponent"/> class. 
        /// Variable used to tell if there is a convertion in the job that has started
        /// </summary>
        public CsvComponent()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository()
                     .Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the field separator.
        /// </summary>
        public string FieldSeparator { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether field names.
        /// </summary>
        public bool FieldNames { get; set; }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Converter</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get
            {
                return this.pollStyle;
            }
        }

        /// <summary>
        /// Gets the enabled status of the instance.
        /// </summary>
        /// <value>The enabled status.</value>
        /// <remarks><c>True</c> if the job instance is enabled.</remarks>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
        }

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The configured instance job name.</remarks>
        public string InstanceName
        {
            get
            {
                return this.Name;
            }
        }

        /// <summary>
        /// Gets the component state.
        /// </summary>
        public ComponentState ComponentState
        {
            get
            {
                return this.componentState;
            }
        }

        /// <summary>
        /// Gets the operation mode. 
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Converter</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get
            {
                return OperationMode.Converter;
            }
        }

        /// <summary>
        /// Input file filter
        /// </summary>
        /// <remarks>
        ///   <para>Defines what file types are being read from <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>NTBIDType.None</c></para>
        /// </remarks>
        protected string FileFilter = "*.csv";

        /// <summary>
        /// Watch subdirs for new files as well
        /// </summary>
        /// <remarks>
        ///   <para>Defines if the instance should look in subdirectories within <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>False</c></para>
        /// </remarks>
        protected bool IncludeSubdirs = false;

        /// <summary>
        /// Buffer size for the file watcher
        /// </summary>
        /// <remarks>
        ///   <para>Defines the internal buffer size the FileSystemWatcher when using this polling.
        /// Should not be set to high. The instance will fall back to contious polling on buffer overflows.</para>
        ///   <para>Default value: <c>4096</c></para>
        /// </remarks>
        protected int BufferSize = 4096;

        /// <summary>
        /// Flag to say if this component is working or not. I have a feeling we have another parameter for this though.
        /// </summary>
        protected bool BusyFlag = false;

        /// <summary>
        /// Dictionary to map ID to encodings, which we can use to say which type of encoding the output file shall have
        /// </summary>
        protected Dictionary<int, string> DocumentEncodings = new Dictionary<int, string>();

        /// <summary>
        /// Dictionary to map ID to file extensions, which we can use to say which type of encoding the output file shall have
        /// </summary>
        protected Dictionary<int, string> DocumentExtensions = new Dictionary<int, string>();

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// </exception>
        /// <exception cref="NotSupportedException">
        /// </exception>
        public void Configure(XmlNode configNode)
        {
            Logger.Debug("Node: " + configNode.Name);
            if (configNode.Attributes != null)
            {
                Logger.Debug("Name attribut: " + configNode.Attributes.GetNamedItem("Name"));

                // Basic configuration sanity check
                if (configNode.Attributes != null && (configNode.Name != "CSVComponent" || configNode.Attributes.GetNamedItem("Name") == null))
                {
                    throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
                }

                // Getting the name of this Component instance
                try
                {
                    this.Name = configNode.Attributes["Name"].Value;
                    this.enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);

                    ThreadContext.Stacks["NDC"].Push(this.InstanceName);

                    this.FieldNames = Convert.ToBoolean(configNode.Attributes["HasFieldNames"].Value);
                    this.FieldSeparator = configNode.Attributes["FieldSeparator"].Value;
                    this.PollDelay = Convert.ToInt32(configNode.Attributes["Delay"].Value);
                }
                catch (Exception ex)
                {
                    Logger.Fatal("Not possible to configure this job instance!");
                    ErrorLogger.Error(ex.Message);
                    ErrorLogger.Error(ex.StackTrace);

                    if (ex.InnerException != null)
                    {
                        ErrorLogger.Error(ex.InnerException.Message);
                        ErrorLogger.Error(ex.InnerException.StackTrace);
                    }
                }

                // Basic operation
                try
                {
                    this.pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
                }
            }

            try
            {
                // Find the file folder to work with
                var folderNode = configNode.SelectSingleNode("CSVInputFolder");

                if (folderNode != null)
                {
                    this.FileInputFolder = folderNode.InnerText;
                }

                Logger.InfoFormat("Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}", Enum.GetName(typeof(PollStyle), this.pollStyle), this.FileInputFolder, this.enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder", ex);
            }

            try
            {
                // Find the file folder to work with
                var folderNode = configNode.SelectSingleNode("CSVDoneFolder");

                if (folderNode != null)
                {
                    this.FileDoneFolder = folderNode.InnerText;
                }

                Logger.InfoFormat("Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}", Enum.GetName(typeof(PollStyle), this.pollStyle), this.FileInputFolder, this.enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder", ex);
            }

            try
            {
                // Find the file folder to work with
                var folderNode = configNode.SelectSingleNode("CSVErrorFolder");

                if (folderNode != null)
                {
                    this.FileErrorFolder = folderNode.InnerText;
                }

                Logger.InfoFormat("Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}", Enum.GetName(typeof(PollStyle), this.pollStyle), this.FileInputFolder, this.enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder", ex);
            }

            try
            {
                // Find the file folder to work with
                var folderNodes = configNode.SelectNodes("CSVOutputFolders/CSVOutputFolder");
                if (folderNodes != null)
                {
                    foreach (XmlNode folderNode in folderNodes)
                    {
                        // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                        if (folderNode != null)
                        {
                            if (folderNode.Attributes != null)
                            {
                                this.FileOutputFolders.Add(Convert.ToInt32(folderNode.Attributes["Id"].Value), folderNode.InnerText);
                                this.DocumentEncodings.Add(Convert.ToInt32(folderNode.Attributes["Id"].Value), folderNode.Attributes["Encoding"] != null ? folderNode.Attributes["Encoding"].Value : "iso-8859-1");

                                this.DocumentExtensions.Add(Convert.ToInt32(folderNode.Attributes["Id"].Value), folderNode.Attributes["Extention"] != null ? folderNode.Attributes["Extention"].Value : "xml");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder");

                ErrorLogger.Error(ex.Message);
                ErrorLogger.Error(ex.StackTrace);

                if (ex.InnerException != null)
                {
                    ErrorLogger.Error(ex.InnerException.Message);
                    ErrorLogger.Error(ex.InnerException.StackTrace);
                }
            }

            if (this.enabled)
            {
                // Checking if file folders exists
                var fileOutputFolder = string.Empty;

                try
                {
                    if (!Directory.Exists(this.FileInputFolder))
                    {
                        Directory.CreateDirectory(this.FileInputFolder);
                    }

                    if (!Directory.Exists(this.FileDoneFolder))
                    {
                        Directory.CreateDirectory(this.FileDoneFolder);
                    }

                    if (!Directory.Exists(this.FileErrorFolder))
                    {
                        Directory.CreateDirectory(this.FileErrorFolder);
                    }

                    foreach (var kvp in this.FileOutputFolders)
                    {
                        fileOutputFolder = kvp.Value;
                        if (!Directory.Exists(fileOutputFolder))
                        {
                            Directory.CreateDirectory(fileOutputFolder);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid, unknown or missing file folder: " + fileOutputFolder, ex);
                }

                try
                {
                    // Switch on pollstyle
                    switch (this.pollStyle)
                    {
                        case PollStyle.Continous:
                            this.Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
                            if (this.Interval == 0)
                            {
                                this.Interval = 5000;
                            }

                            this.pollTimer.Interval = this.Interval * 1000; // Value * 1000 to get seconds
                            Logger.DebugFormat("Poll interval: {0} seconds", this.Interval);

                            break;

                        case PollStyle.Scheduled:

                            /* 
                            schedule = configNode.Attributes["Schedule"].Value;
                            TimeSpan s = Utilities.GetScheduleInterval(schedule);
                            logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                            pollTimer.Interval = s.TotalMilliseconds;
                            */
                            throw new NotSupportedException("Invalid polling style for this job type");

                        case PollStyle.FileSystemWatch:

                            // Check for config overrides
                            if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                            {
                                this.BufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                            }

                            Logger.DebugFormat("FileSystemWatcher buffer size: {0}", this.BufferSize);

                            // Set values
                            this.filesWatcher.Path = this.FileInputFolder;
                            this.filesWatcher.Filter = this.FileFilter;
                            this.filesWatcher.IncludeSubdirectories = this.IncludeSubdirs;
                            this.filesWatcher.InternalBufferSize = this.BufferSize;

                            // Do not start the event watcher here, wait until after the startup cleaning job
                            this.pollTimer.Interval = 5000; // short startup;

                            break;

                        default:

                            // Unsupported pollstyle for this object
                            throw new NotSupportedException("Invalid polling style for this job type");
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
                }
            }

            // Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            this.Configured = true;
        }

        /// <summary>
        /// The start.
        /// </summary>
        /// <exception cref="NormalizerException">
        /// </exception>
        public void Start()
        {
            if (!this.Configured)
            {
                throw new NormalizerException("CSVComponent is not properly configured. CSVComponent::Start() Aborted!");
            }

            if (!this.enabled)
            {
                throw new NormalizerException("CSVComponent is not enabled. CSVSComponent::Start() Aborted!");
            }

            Logger.Info("Starting" + this.InstanceName);

            this._stopEvent.Reset();
            this._busyEvent.Set();

            this.componentState = ComponentState.Running;
            this.pollTimer.Start();
        }

        /// <summary>
        /// The stop.
        /// </summary>
        /// <exception cref="NormalizerException">
        /// </exception>
        public void Stop()
        {
            if (!this.Configured)
            {
                throw new NormalizerException("CSVComponent is not properly configured. CSVComponent::Stop() Aborted");
            }

            if (!this.enabled)
            {
                throw new NormalizerException("CSVComponent is not properly configured. CSVComponent::Stop() Aborted");
            }

            const double epsilon = 0;
            if (!this.pollTimer.Enabled && (this.ErrorRetry || this.pollStyle != PollStyle.FileSystemWatch || Math.Abs(this.pollTimer.Interval - 5000) < epsilon))
            {
                Logger.InfoFormat("Waiting for instance to complete work. Job {0}", this.InstanceName);
            }

            // Signal Stop
            this._stopEvent.Set();

            // Stop events
            this.filesWatcher.EnableRaisingEvents = false;

            if (!this._busyEvent.WaitOne(30000))
            {
                Logger.InfoFormat("Instance did not complete properly. Data may be inconsistent. Job: {0}", this.InstanceName);
            }

            this.componentState = ComponentState.Halted;

            // Kill polling
            this.pollTimer.Stop();
        }

        /// <summary>
        /// The get component state.
        /// </summary>
        /// <returns>
        /// The <see cref="ComponentState"/>.
        /// </returns>
        public ComponentState GetComponentState()
        {
            return ComponentState.Running;
        }

        /// <summary>
        /// The poll timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void pollTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = this.InstanceName;
            Logger.DebugFormat("CSVComponent::pollTimer_Elapsed() hit");

            // Reset flags and pollers
            this._busyEvent.WaitOne();
            this.pollTimer.Stop();

            try
            {
                Logger.Debug("InputFolder for " + this.InstanceName + " : " + this.FileInputFolder);

                // Process any wating files
                var files = new List<string>(Directory.GetFiles(this.FileInputFolder, this.FileFilter, this.IncludeSubdirs ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));

                // Some logging
                Logger.DebugFormat("CSVComponent::pollTimer_Elapsed() Convertion items scheduled: {0}", files.Count);

                // I need to have an Debug-flag here
                if (ConfigurationManager.AppSettings["Debug"].ToUpper() == "TRUE")
                {
                    foreach (var stringFiles in files)
                    {
                        Logger.Debug("Files to be converted are: " + stringFiles);
                    }
                }

                // doing the pollstyle.Continous
                if (this.pollStyle == PollStyle.Continous && !this.ErrorRetry)
                {
                    // Now we are to convert the selected files
                    this.CreateConverter(files);
                }

                // Enable the event listening
                if (this.pollStyle == PollStyle.FileSystemWatch && !this.ErrorRetry)
                {
                    Logger.Debug("Setting filesWatcher.EnableRaisingEvents to true");
                    this.filesWatcher.EnableRaisingEvents = true;
                }

                // Process the files
            }
            catch (Exception ex)
            {
                Logger.Error("CSVComponent::pollTimer_Elapsed(). Check error log for more information");
                ErrorLogger.Error(ex.Message);
                ErrorLogger.Error(ex.StackTrace);

                if (ex.InnerException != null)
                {
                    ErrorLogger.Error(ex.InnerException.Message);
                    ErrorLogger.Error(ex.InnerException.StackTrace);
                }
            }

            // Check error state
            if (this.ErrorRetry)
            {
                this.filesWatcher.EnableRaisingEvents = false;
            }

            // Restart
            this.pollTimer.Interval = this.Interval * 1000;
            if (this.pollStyle != PollStyle.FileSystemWatch || (this.pollStyle == PollStyle.FileSystemWatch && this.filesWatcher.EnableRaisingEvents == false))
            {
                this.pollTimer.Start();
            }

            this._busyEvent.Set();
        }

        /// <summary>
        /// The create converter.
        /// </summary>
        /// <param name="files">
        /// The files.
        /// </param>
        private void CreateConverter(List<string> files)
        {
            try
            {
                var myCsvConverter = new CsvConverter
                                         {
                                             FieldSeparatorString = this.FieldSeparator, 
                                             Files = files, 
                                             OutputFolders = this.FileOutputFolders, 
                                             DoneFolder = this.FileDoneFolder, 
                                             PathError = this.FileErrorFolder
                                         };
                myCsvConverter.ParseCsvFiles();
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        /// The files watcher_ created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void filesWatcher_Created(object sender, FileSystemEventArgs e)
        {
            // Wait for the busy state
            this._busyEvent.WaitOne();

            ThreadContext.Properties["JOBNAME"] = this.InstanceName;
            Logger.DebugFormat("CSVComponent::filesWatcher_Created() hit with file data: {0}", e.FullPath);

            var pollTime = this.PollDelay * 1000;
            Logger.Debug("We are waiting: " + pollTime.ToString() + " Seconds before starting to convert!");

            if (this.PollDelay > 0)
            {
                // We take the value and multiply with thousand - to get the seconds... 
                Thread.Sleep(pollTime);
            }

            try
            {
                

                this.CreateConverter(
                    new List<string>
                        {
                            e.FullPath
                        });
            }
            catch (Exception ex)
            {
                Logger.Debug("Something happened while doing filesWatcher_Created!");
                ErrorLogger.Error(ex.Message);
                ErrorLogger.Error(ex.StackTrace);

                if (ex.InnerException != null)
                {
                    ErrorLogger.Error(ex.InnerException.Message);
                    ErrorLogger.Error(ex.InnerException.StackTrace);
                }

                // We must move the error file
            }

            // Reset event
            this._busyEvent.Set();
        }
    }
}