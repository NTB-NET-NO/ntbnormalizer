﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XSLTComponent.cs" company="">
//   
// </copyright>
// <summary>
//   The xslt component.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Timers;
using System.Xml;
using log4net;
using log4net.Config;
using NTB.Normalizer.Components.Converters;
using NTB.Normalizer.Components.enums;
using NTB.Normalizer.Components.Exceptions;
using NTB.Normalizer.Components.Interfaces;
using NTB.Normalizer.Components.Transformers;
using NTB.Normalizer.Components.Utilities;
using NTB.Normalizer.Utilities;

namespace NTB.Normalizer.Components.Components
{
    // Logging

    // XML Utilities

    /// <summary>
    ///     The xslt component.
    /// </summary>
    public partial class XsltComponent : Component, IBaseNormComponents
    {
        #region Fields

        /// <summary>
        ///     Buffer size for the file watcher
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         Defines the internal buffer size the FileSystemWatcher when using this polling.
        ///         Should not be set to high. The instance will fall back to contious polling on buffer overflows.
        ///     </para>
        ///     <para>Default value: <c>4096</c></para>
        /// </remarks>
        protected int BufferSize = 4096;

        /// <summary>
        ///     Flag to say if this component is working or not. I have a feeling we have another parameter for this though.
        /// </summary>
        protected bool BusyFlag;

        /// <summary>
        ///     Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly
        ///     configured
        /// </summary>
        /// <remarks>Holds the configured status of the instance. <c>True</c> means successfully configured</remarks>
        protected bool Configured;

        /// <summary>
        ///     Dictionary to map ID to encodings, which we can use to say which type of encoding the output file shall have
        /// </summary>
        protected Dictionary<int, string> DocumentEncodings = new Dictionary<int, string>();

        /// <summary>
        ///     Dictionary to map ID to file extensions, which we can use to say which type of encoding the output file shall have
        /// </summary>
        protected Dictionary<int, string> DocumentExtensions = new Dictionary<int, string>();

        /// <summary>
        ///     Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailBody;

        /// <summary>
        ///     Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected string EmailNotification;

        /// <summary>
        ///     Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailSubject;

        /// <summary>
        ///     Error-Retry flag
        /// </summary>
        /// <remarks>
        ///     The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later
        ///     retry.
        /// </remarks>
        protected bool ErrorRetry = false;

        /// <summary>
        ///     File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///     <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///     <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        protected string FileDoneFolder;

        /// <summary>
        ///     This is used to choose between version 1 and 2 of xslt
        /// </summary>
        /// <remarks>
        /// <para>This is mostly a feature added to make it possible to create non-xml-files without problems</para>
        /// <para>And also because I needed this feature to make the transformation process of the AP-files a bit more effective</para>
        /// </remarks>
        protected bool TrustedXslt;

        /// <summary>
        ///     If this is set to true the filename can contain non-ascii-characters. Otherwise these will be replaced
        /// </summary>
        protected bool AllowNonAscii;

        /// <summary>
        ///     The non-ascii characters shall be replaced by this character
        /// </summary>
        protected string NonAsciiReplaceCharacter;

        /// <summary>
        ///     Error file folder
        /// </summary>
        /// <remarks>
        ///     <para>File folder where failing files are stored.</para>
        ///     <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        protected string FileErrorFolder;

        /// <summary>
        ///     Input file filter
        /// </summary>
        /// <remarks>
        ///     <para>Defines what file types are being read from <see cref="FileInputFolder" />.</para>
        ///     <para>Default value: <c>NTBIDType.None</c></para>
        /// </remarks>
        protected string FileFilter = "*.xml";

        /// <summary>
        ///     Input file folder
        /// </summary>
        /// <remarks>
        ///     File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter" /> and
        ///     <see cref="IncludeSubdirs" />
        /// </remarks>
        protected string FileInputFolder;

        /// <summary>
        ///     Output file folder
        /// </summary>
        /// <remarks>
        ///     File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter" /> and
        ///     <see cref="IncludeSubdirs" />
        /// </remarks>
        protected Dictionary<int, string> FileOutputFolders = new Dictionary<int, string>();

        /// <summary>
        ///     Watch subdirs for new files as well
        /// </summary>
        /// <remarks>
        ///     <para>Defines if the instance should look in subdirectories within <see cref="FileInputFolder" />.</para>
        ///     <para>Default value: <c>False</c></para>
        /// </remarks>
        protected bool IncludeSubdirs = false;

        /// <summary>
        ///     The configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///     <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///     <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;

        /// <summary>
        ///     The iptc sequence number.
        /// </summary>
        protected bool IptcSequenceNumber;

        /// <summary>
        ///     The iptc sequence number file.
        /// </summary>
        protected string IptcSequenceNumberFile;

        /// <summary>
        ///     The configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName" /></remarks>
        protected string Name;

        /// <summary>
        ///     PollDelay is used to make sure that the file is ready to be accessed.
        /// </summary>
        protected double PollDelay;

        /// <summary>
        ///     The configured schedule for this instance
        /// </summary>
        /// <remarks>
        ///     This is set to a time of day and is used by the <c>Scheduled</c> pollstyle. Work is triggered at the given
        ///     time of day.
        /// </remarks>
        protected string Schedule;

        /// <summary>
        ///     The _path xslt.
        /// </summary>
        private readonly string pathXslt = ConfigurationManager.AppSettings["dirXSLTRoot"];

        /// <summary>
        ///     Exit control event
        /// </summary>
        private readonly AutoResetEvent stopEvent = new AutoResetEvent(false);

        /// <summary>
        ///     The rwlock.
        /// </summary>
        private readonly ReaderWriterLockSlim rwlock;

        #endregion

        #region Private Static Fields

        /// <summary>
        ///     The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(XsltComponent));

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="XsltComponent" /> class.
        ///     Variable used to tell if there is a convertion in the job that has started
        /// </summary>
        public XsltComponent()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }

            this.rwlock = new ReaderWriterLockSlim();

            // filesWatcher.InternalBufferSize = 4*4096;
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XsltComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public XsltComponent(IContainer container)
        {
            container.Add(this);

            this.InitializeComponent();
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the component state.
        /// </summary>
        public ComponentState ComponentState
        {
            get
            {
                return this.componentState;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the enabled status is true or false.
        /// </summary>
        /// <value>The enabled status.</value>
        /// <remarks><c>True</c> if the job instance is enabled.</remarks>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
        }

        /// <summary>
        ///     Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The configured instance job name.</remarks>
        public string InstanceName
        {
            get
            {
                return this.Name;
            }
        }

        /// <summary>
        ///     Gets the operation mode.
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Converter</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get
            {
                return OperationMode.Converter;
            }
        }

        /// <summary>
        ///     Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Converter</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get
            {
                return this.pollStyle;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the iptc sequence number max.
        /// </summary>
        protected int IptcSequenceNumberMax { get; set; }

        /// <summary>
        ///     Gets or sets the iptc sequence number min.
        /// </summary>
        protected int IptcSequenceNumberMin { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get component state.
        /// </summary>
        /// <returns>
        /// The <see cref="ComponentState"/>.
        /// </returns>
        public ComponentState GetComponentState()
        {
            if (this.pollStyle != PollStyle.FileSystemWatch)
            {
                return ComponentState.Running;
            }

            // Checking if fileswatcher is enabled
            if (this.filesWatcher.EnableRaisingEvents)
            {
                return ComponentState.Running;
            }

            this.filesWatcher.EnableRaisingEvents = false;
            var iMaxAttempts = 120;
            var iTimeOut = 30000;
            var i = 0;
            while (this.filesWatcher.EnableRaisingEvents == false && i < iMaxAttempts)
            {
                i += 1;
                try
                {
                    Logger.Info("Trying to set file system watcher enabled again");
                    this.filesWatcher.EnableRaisingEvents = true;
                }
                catch
                {
                    Logger.Error("Failing to set file system watcher enabled again");
                    this.filesWatcher.EnableRaisingEvents = false;
                    Thread.Sleep(iTimeOut);
                }
            }

            // If we've not been able to restart the files-watcher, we must set the ComponentState to halted
            if (i == iMaxAttempts && this.filesWatcher.EnableRaisingEvents == false)
            {
                return ComponentState.Halted;
            }

            return ComponentState.Running;
        }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// If something goes wrong we are returing an ArgumentException exception
        /// </exception>
        /// <exception cref="NotSupportedException">
        /// If something goes wrong we are returing an NotSupportedException exception
        /// </exception>
        public void Configure(XmlNode configNode)
        {
            Logger.Debug("Node: " + configNode.Name);

            // Basic configuration sanity check
            if (configNode.Attributes != null && (configNode.Name != "XSLTComponent" || configNode.Attributes.GetNamedItem("Name") == null))
            {
                throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
            }

            // Logger.Debug("Name attribut: " + configNode.Attributes.GetNamedItem("Name"));
            // Basic configuration sanity check
            // if (configNode.Attributes != null && (configNode != null && configNode.Name == "XSLTComponent" &&
            // configNode.Attributes.GetNamedItem("Name") != null))
            // {
            // throw new ArgumentException("The XML configuration node passed is invalid", 
            // "configNode");
            // }

            // Getting the name of this Component instance
            try
            {
                if (configNode.Attributes != null)
                {
                    this.Name = configNode.Attributes["Name"].Value;

                    ThreadContext.Stacks["NDC"].Push(this.InstanceName);

                    this.enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);

                    this.PollDelay = Convert.ToDouble(ConfigurationManager.AppSettings["PollingInterval"]);
                    if (configNode.Attributes["Delay"] != null)
                    {
                        this.PollDelay = Convert.ToDouble(configNode.Attributes["Delay"].Value);
                    }

                    this.TrustedXslt = false;
                    if (configNode.Attributes["trustedxslt"] != null)
                    {
                        this.TrustedXslt = Convert.ToBoolean(configNode.Attributes["trustedxslt"].Value);
                    }

                    this.AllowNonAscii = true;
                    if (configNode.Attributes["allow-non-ascii"] != null)
                    {
                        this.AllowNonAscii = Convert.ToBoolean(configNode.Attributes["allow-non-ascii"].Value);

                        if (configNode.Attributes["non-ascii-replace"] != null)
                        {
                            this.NonAsciiReplaceCharacter = configNode.Attributes["non-ascii-replace"].Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance. Check error log for more information.");
                ErrorLogger.Fatal("Not possible to configure this job instance!", ex);

                ThreadContext.Stacks["NDC"].Pop();

                throw new ArgumentException("Invalid or missing values in XML configuration node", ex);
            }

            // Basic operation
            try
            {
                if (configNode.Attributes != null)
                {
                    this.pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }

            // Check if we are to use sequence numbers
            this.SetIptcSequenceNumber(configNode);

            this.SetXmlInputFolder(configNode);

            this.SetOutputFolders(configNode);
            this.SetDocumentEncodings(configNode);
            this.SetDocumentExtensions(configNode);

            // adding the done folder
            this.SetDoneFolder(configNode);

            // adding the error folder
            this.SetErrorFolder(configNode);

            if (this.enabled)
            {
                // Checking if file folders exists
                var fileOutputFolder = string.Empty;

                try
                {
                    var di = new DirectoryInfo(this.FileInputFolder);
                    if (!di.Exists)
                    {
                        Directory.CreateDirectory(this.FileInputFolder);
                    }

                    foreach (var kvp in this.FileOutputFolders)
                    {
                        fileOutputFolder = kvp.Value;
                        di = new DirectoryInfo(fileOutputFolder);
                        if (!di.Exists)
                        {
                            Directory.CreateDirectory(fileOutputFolder);
                        }
                    }

                    // Check if folder exists
                    di = new DirectoryInfo(this.FileErrorFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }

                    // Check if folder exists
                    di = new DirectoryInfo(this.FileDoneFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid, unknown or missing file folder: " + fileOutputFolder, ex);
                }

                try
                {
                    // Switch on pollstyle
                    switch (this.pollStyle)
                    {
                        case PollStyle.Continous:
                            if (configNode.Attributes != null)
                            {
                                this.Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
                            }

                            if (this.Interval == 0)
                            {
                                this.Interval = 5000;
                            }

                            this.pollTimer.Interval = this.Interval * 1000; // short startup - interval * 1000;
                            Logger.DebugFormat("Poll interval: {0} seconds", this.Interval);

                            break;

                        case PollStyle.Scheduled:

                            /* 
                            schedule = configNode.Attributes["Schedule"].Value;
                            TimeSpan s = Utilities.GetScheduleInterval(schedule);
                            logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                            pollTimer.Interval = s.TotalMilliseconds;
                            */
                            throw new NotSupportedException("Invalid polling style for this job type");

                        case PollStyle.FileSystemWatch:

                            this.ConfigureFileSystemWatch(configNode);

                            // Do not start the event watcher here, wait until after the startup cleaning job
                            this.pollTimer.Interval = 5000; // short startup;

                            break;

                        default:

                            // Unsupported pollstyle for this object
                            throw new NotSupportedException("Invalid polling style for this job type");
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
                }
            }

            // A 

            // Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            this.Configured = true;
        }

        /// <summary>
        /// Configure the File System Watch
        /// </summary>
        /// <param name="configNode"></param>
        private void ConfigureFileSystemWatch(XmlNode configNode)
        {
            // Check for config overrides
            if (configNode.Attributes != null && configNode.Attributes.GetNamedItem("BufferSize") != null)
            {
                this.BufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
            }

            Logger.DebugFormat("FileSystemWatcher buffer size: {0}", this.BufferSize);

            // Making sure it does not exists!
            this.filesWatcher.Dispose();
            this.filesWatcher = null;

            // Create the object
            this.filesWatcher = new FileSystemWatcher();

            ((ISupportInitialize)(this.filesWatcher)).BeginInit();

            // Set values
            this.filesWatcher.Path = this.FileInputFolder;
            this.filesWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName;
            this.filesWatcher.Filter = this.FileFilter;
            this.filesWatcher.IncludeSubdirectories = this.IncludeSubdirs;
            this.filesWatcher.InternalBufferSize = this.BufferSize;
            this.filesWatcher.Error += this.FilesWatcherOnError;
            this.filesWatcher.Created += this.FilesWatcherCreated;
            this.filesWatcher.Renamed += this.FilesWatcherRenamed;
            ((ISupportInitialize)(this.filesWatcher)).EndInit();
        }

        private void DoSaxonConversion(IEnumerable<string> inputFiles, string fileXsl)
        {
            using (var mainXsltConverter = new DocumentConverter())
            {

                // Setting the value of the IptcSequenceNumber Check
                mainXsltConverter.IptcSequenceNumber = this.IptcSequenceNumber;

                // Setting the value of the IptcSequenceNumberFile
                mainXsltConverter.IptcSequenceNumberFile = this.IptcSequenceNumberFile;

                mainXsltConverter.IptcSequenceNumberMin = this.IptcSequenceNumberMin;

                mainXsltConverter.IptcSequenceNumberMax = this.IptcSequenceNumberMax;

                mainXsltConverter.CheckCharacters = this.TrustedXslt;

                // Defining the XSLT-file
                mainXsltConverter.FileXsl = fileXsl;

                // defining document output
                mainXsltConverter.OutputDocumentExtensions = this.DocumentExtensions;

                // Defining output path(s)
                mainXsltConverter.OutputPaths = this.FileOutputFolders;

                // Defining error folder
                mainXsltConverter.PathError = this.FileErrorFolder;

                // Defining encoding(s)
                mainXsltConverter.OutputPathAndEncodings = this.DocumentEncodings;

                mainXsltConverter.JobName = this.InstanceName;

                mainXsltConverter.PathError = this.FileErrorFolder;

                string[] files;
                if (this.CheckIfFileArrayIsEmpty(inputFiles, out files))
                {
                    return;
                }

                try
                {
                    var processor = mainXsltConverter.CreateProcessor();

                    // Creating the XSLT Compile here?
                    mainXsltConverter.CreateXsltCompiler(fileXsl, processor);

                    Logger.Debug("---------------------------> Starting loop <--------------------------");
                    foreach (var filename in files)
                    {
                        var fi = new FileInfo(filename);
                        if (this.CheckIfXmlFileCanBeRead(filename))
                        {
                            continue;
                        }

                        this.BusyFlag = true;
                        Logger.DebugFormat("Files: {0}", filename);

                        var transformedFile = mainXsltConverter.DoDocumentTransformation(filename, processor);
                        if (this.IsTransformedFileEmpty(transformedFile, fi))
                        {
                            continue;
                        }

                        mainXsltConverter.AllowNonAscii = this.AllowNonAscii;
                        mainXsltConverter.NonAsciiReplaceCharacter = this.NonAsciiReplaceCharacter;
                        this.CreateOutputFile(transformedFile, mainXsltConverter, filename, fi);

                        this.BusyFlag = false;
                    }

                    Logger.Debug("---------------------------> Ending loop <--------------------------");
                    this.BusyFlag = false;
                }
                catch (XsltFileException exception)
                {
                    ErrorLogger.Error(exception.Message);
                    var notifyer = new Notifyer();
                    notifyer.HtmlBody = false;
                    notifyer.LastErrorMessage = exception.Message;
                    notifyer.SendMail();

                    foreach (var file in files.ToList())
                    {
                        var fi = new FileInfo(file);
                        var path = Path.Combine(this.FileErrorFolder, fi.Name);

                        var fileInfo = new FileInfo(path);
                        if (fileInfo.Exists)
                        {
                            fileInfo.Delete();
                        }

                        // Moving file
                        fi.MoveTo(path);
                    }

                    throw;
                }
            }
        }

        private bool CheckIfFileArrayIsEmpty(IEnumerable<string> inputFiles, out string[] files)
        {
/*
                 * We are using this for the time being
                 */
            files = inputFiles as string[] ?? inputFiles.ToArray();
            // var enumerable = inputFiles as string[] ?? files.ToArray();
            if (inputFiles != null && !files.Any())
            {
                return true;
            }
            return false;
        }

        private void CreateOutputFile(string transformedFile, TrustedXsltConverter mainXsltConverter, string filename, FileInfo fi)
        {
            if (transformedFile == string.Empty)
            {
                Logger.Info("The transformation returned no data. Returning");
                return;
            }

            try
            {
                mainXsltConverter.PathError = this.FileErrorFolder;
                mainXsltConverter.CreateOutputFile(transformedFile, filename);
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                var path = Path.Combine(this.FileErrorFolder, fi.Name);

                var errorFileInfo = new FileInfo(path);
                if (errorFileInfo.Exists)
                {
                    errorFileInfo.Delete();
                }

                // Moving file
                if (fi.Exists)
                {
                    fi.MoveTo(path);
                }
            }
            finally
            {
                var path = Path.Combine(this.FileDoneFolder, fi.Name);

                var fileInfo = new FileInfo(path);
                if (fileInfo.Exists)
                {
                    fileInfo.Delete();
                }

                // Moving file
                fi.MoveTo(path);
            }
        }

        private void CreateOutputFile(string transformedFile, DocumentConverter mainXsltConverter, string filename, FileInfo fi)
        {
            if (transformedFile != string.Empty)
            {
                // Now we shall call a new method
                try
                {
                    mainXsltConverter.CreateOutputFile(transformedFile, filename);
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    var path = Path.Combine(this.FileErrorFolder, fi.Name);

                    var errorFileInfo = new FileInfo(path);
                    if (errorFileInfo.Exists)
                    {
                        errorFileInfo.Delete();
                    }

                    // Moving file
                    if (fi.Exists)
                    {
                        fi.MoveTo(path);
                    }
                }
                finally
                {
                    var path = Path.Combine(this.FileDoneFolder, fi.Name);

                    var fileInfo = new FileInfo(path);
                    if (fileInfo.Exists)
                    {
                        fileInfo.Delete();
                    }

                    // Moving file
                    fi.MoveTo(path);
                }
            }
        }

        private bool IsTransformedFileEmpty(string transformedFile, FileInfo fi)
        {
            if (transformedFile == string.Empty)
            {
                Logger.InfoFormat("There must have been a problem transforming the document");
                Logger.InfoFormat("We are moving the file to error folder");

                if (fi.Exists)
                {
                    var path = Path.Combine(this.FileErrorFolder, fi.Name);
                    var errorFileInfo = new FileInfo(path);

                    if (errorFileInfo.Exists)
                    {
                        errorFileInfo.Delete();
                    }

                    fi.MoveTo(path);
                }

                return true;
            }
            return false;
        }

        private bool CheckIfXmlFileCanBeRead(string filename)
        {
            var fi = new FileInfo(filename);

            FileUtilities.IsFileLocked(fi);

            try
            {
                XmlUtilities.CanReadXmlFile(filename);
            }
            catch (Exception exception)
            {
                // This means there is a problem with the file. So we need to move it to the error folder
                Logger.ErrorFormat("An error occured: {0}", exception);

                if (fi.Exists)
                {
                    var path = Path.Combine(this.FileErrorFolder, fi.Name);
                    var errorFileInfo = new FileInfo(path);

                    if (errorFileInfo.Exists)
                    {
                        errorFileInfo.Delete();
                    }

                    fi.MoveTo(path);
                }

                return true;
            }
            return false;
        }

        private void DoDotNetConversion(IEnumerable<string> inputFiles, string fileXsl)
        {
            using (var mainXsltConverter = new TrustedXsltConverter())
            {

                // Defining the XSLT-file
                mainXsltConverter.FileXsl = fileXsl;

                // defining document output
                mainXsltConverter.OutputDocumentExtensions = this.DocumentExtensions;

                // Defining output path(s)
                mainXsltConverter.OutputPaths = this.FileOutputFolders;

                // Defining error folder
                mainXsltConverter.PathError = this.FileErrorFolder;

                // Defining encoding(s)
                mainXsltConverter.OutputPathAndEncodings = this.DocumentEncodings;

                mainXsltConverter.JobName = this.InstanceName;

                Logger.DebugFormat("Eror folder set to {0}", this.FileErrorFolder);
                mainXsltConverter.PathError = this.FileErrorFolder;

                Logger.Debug("---------------------------> Starting loop <--------------------------");

                string[] files;
                if (this.CheckIfFileArrayIsEmpty(inputFiles, out files))
                {
                    return;
                }

                foreach (var filename in files)
                {
                    var fi = new FileInfo(filename);
                    if (this.CheckIfXmlFileCanBeRead(filename))
                    {
                        continue;
                    }

                    this.BusyFlag = true;
                    Logger.DebugFormat("Files: {0}", filename);

                    var transformedFile = mainXsltConverter.DoDocumentTransformation(filename);
                    if (this.IsTransformedFileEmpty(transformedFile, fi))
                    {
                        continue;
                    }

                    this.CreateOutputFile(transformedFile, mainXsltConverter, filename, fi);

                    this.BusyFlag = false;
                }

                Logger.Debug("---------------------------> Ending loop <--------------------------");
                this.BusyFlag = false;

    
            }
            
            
        }

        /// <summary>
        /// This method is used to call the converter class
        /// </summary>
        /// <param name="inputFiles">
        /// List of strings/files that are to be converted
        /// </param>
        /// <param name="fileXsl">
        /// The XSLT-document to be used to parse the files
        /// </param>
        public void Converter(IEnumerable<string> inputFiles, string fileXsl)
        {
            Logger.Debug("In Converter - List<string> Files, string FileXSL");

            Logger.Debug("FileXSL: " + fileXsl);

            // Creating the converter
            // ;

            /*
             * We are using this for the time being
             */

            if (inputFiles == null)
            {
                return;
            }
            var files = inputFiles as string[] ?? inputFiles.ToArray();
            // var enumerable = inputFiles as string[] ?? files.ToArray();
            
            if (inputFiles != null && !files.Any())
            {
                return;
            }

            if (this.TrustedXslt == false)
            {
                // this has to become shorter...
                // Hard to maintain
                // todo: Try and make this part shorter - move it to another class
                this.DoSaxonConversion(inputFiles, fileXsl);

                
            }
            else
            {
                // Setting the value of the IptcSequenceNumber Check
                this.DoDotNetConversion(inputFiles, fileXsl);
                
            }
            
        }

        /// <summary>
        ///     The start.
        /// </summary>
        /// <exception cref="NormalizerException">
        ///     If the service does not start, we send a Normalizer Exception
        /// </exception>
        public void Start()
        {
            if (!this.Configured)
            {
                throw new NormalizerException("XSLTComponent is not properly configured. XSLTComponent::Start() Aborted!");
            }

            if (!this.enabled)
            {
                throw new NormalizerException("XSLTComponent is not enabled. XSLTComponent::Start() Aborted!");
            }

            Logger.Info("Starting " + this.InstanceName);

            this.stopEvent.Reset();
            this._busyEvent.Set();

            this.componentState = ComponentState.Running;
            this.pollTimer.Start();
        }

        /// <summary>
        ///     The stop.
        /// </summary>
        /// <exception cref="NormalizerException">
        ///     If the service does not stop, we send a Normalizer Exception
        /// </exception>
        public void Stop()
        {
            if (!this.Configured)
            {
                throw new NormalizerException("XSLTComponent is not properly configured. XSLTComponent::Stop() Aborted");
            }

            if (!this.enabled)
            {
                throw new NormalizerException("XSLTComponent is not properly configured. XSLTComponent::Stop() Aborted");
            }

            const double Epsilon = 0;

            if (!this.pollTimer.Enabled && (this.ErrorRetry || this.pollStyle != PollStyle.FileSystemWatch || Math.Abs(this.pollTimer.Interval - 5000) < Epsilon))
            {
                Logger.InfoFormat("Waiting for instance to complete work. Job {0}", this.InstanceName);
            }

            // Signal Stop
            this.stopEvent.Set();

            // Stop events
            this.filesWatcher.EnableRaisingEvents = false;

            if (!this._busyEvent.WaitOne(30000))
            {
                Logger.InfoFormat("Instance did not complete properly. Data may be inconsistent. Job: {0}", this.InstanceName);
            }

            this.componentState = ComponentState.Halted;

            // Kill polling
            this.pollTimer.Stop();
        }

        #endregion

        #region Methods

        /// <summary>
        /// The files watcher on error.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="errorEventArgs">
        /// The error event args.
        /// </param>
        private void FilesWatcherOnError(object sender, ErrorEventArgs errorEventArgs)
        {
            this._busyEvent.WaitOne();
            var retries = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertRetries"]);
            var sleep = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertRetryInterval"]);

            ErrorLogger.ErrorFormat("The error is of type: {0}", errorEventArgs.GetException().GetType());

            this.filesWatcher.EnableRaisingEvents = false;
            var iMaxAttempts = 120;
            var iTimeOut = 30000;
            var i = 0;
            // Making sure it does not exists!
            this.filesWatcher.Dispose();
            this.filesWatcher = null;

            // Create the object
            this.filesWatcher = new FileSystemWatcher();

            while (this.filesWatcher.EnableRaisingEvents == false && i < iMaxAttempts)
            {
                i += 1;
                try
                {
                    ((ISupportInitialize)(this.filesWatcher)).BeginInit();

                    // Set values
                    this.filesWatcher.Path = this.FileInputFolder;
                    this.filesWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName;
                    this.filesWatcher.Filter = this.FileFilter;
                    this.filesWatcher.IncludeSubdirectories = this.IncludeSubdirs;
                    this.filesWatcher.InternalBufferSize = this.BufferSize;
                    this.filesWatcher.Error += this.FilesWatcherOnError;
                    this.filesWatcher.Created += this.FilesWatcherCreated;
                    this.filesWatcher.Renamed += this.FilesWatcherRenamed;
                    ((ISupportInitialize)(this.filesWatcher)).EndInit();
                }
                catch
                {
                    ErrorLogger.Error("Failing to set file system watcher enabled again");
                    // this.filesWatcher.EnableRaisingEvents = false;
                    Thread.Sleep(iTimeOut);

                    // We should mail me!
                    
                }
            }

            var watcher = this.filesWatcher;

            var di = new DirectoryInfo(watcher.Path);

            var files = di.GetFiles();

            if (files.Any())
            {
                var filesToProcess = new List<string>();
                foreach (var fileInfo in files)
                {
                    filesToProcess.Add(fileInfo.FullName);   
                }

                this.Converter(filesToProcess, this.pathXslt + @"\" + this.InstanceName + @"\" + this.InstanceName + ".xsl");
            }

            // this.ProcessFile(file.FullName, retries, sleep);

            // Making sure the files watcher is listening.
            if (this.filesWatcher.EnableRaisingEvents == false)
            {
                this.filesWatcher.EnableRaisingEvents = true;
            }

            this._busyEvent.Set();
        }

        /// <summary>
        /// The process file.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="retries">
        /// The retries.
        /// </param>
        /// <param name="sleep">
        /// The sleep.
        /// </param>
        private void ProcessFile(string file, int retries, int sleep)
        {
            var fi = new FileInfo(file);
            if (retries <= 0)
            {
                // return;

                throw new NormalizerException("Not possible to process file!");

            }

            for (var i = 0; i <= retries; i++)
            {
                // Wait for the busy state

                // Adding files to the list and then sending that list to the converter
                try
                {
                    if (!fi.Exists)
                    {
                        return;
                    }

                    Logger.InfoFormat("Converting file {0}", fi.Name);
                    this.Converter(new[] { file }, this.pathXslt + @"\" + this.InstanceName + @"\" + this.InstanceName + ".xsl");

                    // Exit loop here...
                    i = retries;
                }
                catch (Exception exception)
                {
                    retries -= 1;

                    if (!fi.Exists)
                    {
                        return;
                    }

                    // Checking if we can read the file or not. 
                    if (i > retries)
                    {
                        continue;
                    }

                    Logger.WarnFormat("Open/Read failed, retrying file: {0} Error: {1} ", file, exception.Message);
                    if (exception.InnerException != null)
                    {
                        Logger.WarnFormat("Open/Read failed, retrying file: {0} Error: {1} ", file, exception.InnerException.Message);
                    }
                        
                    Thread.Sleep(sleep);

                        

                    // And so we try again.
                    this.ProcessFile(file, retries, sleep);
                }
            }
        }

        /// <summary>
        /// The set document encodings.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetDocumentEncodings(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                var folderNodes = configNode.SelectNodes("XMLOutputFolders/XMLOutputFolder");
                if (folderNodes != null)
                {
                    foreach (var folderNode in folderNodes.Cast<XmlNode>().Where(folderNode => folderNode != null))
                    {
                        if (folderNode.Attributes == null)
                        {
                            continue;
                        }

                        this.DocumentEncodings.Add(Convert.ToInt32(folderNode.Attributes["Id"].Value), folderNode.Attributes["Encoding"] != null ? folderNode.Attributes["Encoding"].Value : "iso-8859-1");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder. Check error log for more information");
                ErrorLogger.Fatal("Not possible to get output-folder", ex);
            }
        }

        /// <summary>
        /// The set document extensions.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetDocumentExtensions(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                var folderNodes = configNode.SelectNodes("XMLOutputFolders/XMLOutputFolder");
                if (folderNodes != null)
                {
                    foreach (var folderNode in folderNodes.Cast<XmlNode>().Where(folderNode => folderNode != null))
                    {
                        if (folderNode.Attributes == null)
                        {
                            continue;
                        }

                        this.DocumentExtensions.Add(Convert.ToInt32(folderNode.Attributes["Id"].Value), folderNode.Attributes["Extention"] != null ? folderNode.Attributes["Extention"].Value : "xml");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder. Check error log for more information");
                ErrorLogger.Fatal("Not possible to get output-folder", ex);
            }
        }

        /// <summary>
        /// The set done folder.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetDoneFolder(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                var folderNode = configNode.SelectSingleNode("XMLDoneFolder");
                if (folderNode != null)
                {
                    this.FileDoneFolder = folderNode.InnerText;
                }

                Logger.InfoFormat("Converter job - Polling: {0} / File done folder: {1} / Enabled: {2}", Enum.GetName(typeof(PollStyle), this.pollStyle), this.FileDoneFolder, this.enabled);
            }
            catch (Exception ex)
            {
                ErrorLogger.Fatal("Not possible to get Done-folder", ex);
                Logger.Fatal("Not possible to get Done-folder. Check error log for more information");
            }
        }

        /// <summary>
        /// The set error folder.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetErrorFolder(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                var folderNode = configNode.SelectSingleNode("XMLErrorFolder");
                if (folderNode != null)
                {
                    this.FileErrorFolder = folderNode.InnerText;
                }

                Logger.InfoFormat("Converter job - Polling: {0} / File done folder: {1} / Enabled: {2}", Enum.GetName(typeof(PollStyle), this.pollStyle), this.FileErrorFolder, this.enabled);
            }
            catch (Exception ex)
            {
                ErrorLogger.Fatal("Not possible to get Done-folder", ex);
                Logger.Fatal("Not possible to get Done-folder. Check error logger for more information");
            }
        }

        /// <summary>
        /// The set iptc sequence number.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetIptcSequenceNumber(XmlNode configNode)
        {
            try
            {
                var folderNode = configNode.SelectSingleNode("IPTCSequenceNumber");
                if (folderNode != null)
                {
                    if (configNode.Attributes != null)
                    {
                        if (folderNode.Attributes != null)
                        {
                            this.IptcSequenceNumber = Convert.ToBoolean(folderNode.Attributes["value"].Value);
                            this.IptcSequenceNumberMin = Convert.ToInt32(folderNode.Attributes["min-number"].Value);
                            this.IptcSequenceNumberMax = Convert.ToInt32(folderNode.Attributes["max-number"].Value);
                        }
                    }

                    if (this.IptcSequenceNumber)
                    {
                        var iptcFolderNode = configNode.SelectSingleNode("IPTCSequenceNumber/SequenceNumberFile");

                        if (iptcFolderNode != null)
                        {
                            this.IptcSequenceNumberFile = iptcFolderNode.InnerText;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Info("We are not to use IPTC-Sequence-numbers");
                Logger.Debug(exception.Message);
                Logger.Debug(exception.StackTrace);
            }
        }

        /// <summary>
        /// The set output folders.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetOutputFolders(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                var folderNodes = configNode.SelectNodes("XMLOutputFolders/XMLOutputFolder");
                if (folderNodes != null)
                {
                    foreach (var folderNode in folderNodes.Cast<XmlNode>().Where(folderNode => folderNode != null))
                    {
                        if (folderNode.Attributes == null)
                        {
                            continue;
                        }

                        this.FileOutputFolders.Add(Convert.ToInt32(folderNode.Attributes["Id"].Value), folderNode.InnerText);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder. Check error log for more information.");
                ErrorLogger.Fatal("Not possible to get output-folder", ex);
            }
        }

        /// <summary>
        /// The set xml input folder.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetXmlInputFolder(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                var folderNode = configNode.SelectSingleNode("XMLInputFolder");

                // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                if (folderNode != null)
                {
                    this.FileInputFolder = folderNode.InnerText;

                    // Check if folder exists
                    var di = new DirectoryInfo(this.FileInputFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }

                Logger.InfoFormat("Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}", Enum.GetName(typeof(PollStyle), this.pollStyle), this.FileInputFolder, this.enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get input-folder. Check error log for more information.");
                ErrorLogger.Fatal("Not possible to get input-folder", ex);
            }
        }

        private void ConfigurePollStyle(XmlNode configNode)
        {
            try
            {
                // Switch on pollstyle
                switch (this.pollStyle)
                {
                    case PollStyle.Continous:
                        if (configNode.Attributes != null)
                        {
                            this.Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
                        }

                        if (this.Interval == 0)
                        {
                            this.Interval = 5000;
                        }

                        this.pollTimer.Interval = this.Interval * 1000; // short startup - interval * 1000;
                        Logger.DebugFormat("Poll interval: {0} seconds", this.Interval);

                        break;

                    case PollStyle.Scheduled:

                        /* 
                        schedule = configNode.Attributes["Schedule"].Value;
                        TimeSpan s = Utilities.GetScheduleInterval(schedule);
                        logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                        pollTimer.Interval = s.TotalMilliseconds;
                        */
                        throw new NotSupportedException("Invalid polling style for this job type");

                    case PollStyle.FileSystemWatch:

                        // Check for config overrides
                        if (configNode.Attributes != null && configNode.Attributes.GetNamedItem("BufferSize") != null)
                        {
                            this.BufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                        }

                        Logger.DebugFormat("FileSystemWatcher buffer size: {0}", this.BufferSize);

                        // Set values
                        this.filesWatcher.Path = this.FileInputFolder;
                        this.filesWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName;
                        this.filesWatcher.Filter = this.FileFilter;
                        this.filesWatcher.IncludeSubdirectories = this.IncludeSubdirs;
                        this.filesWatcher.InternalBufferSize = this.BufferSize;
                        this.filesWatcher.Error += this.FilesWatcherOnError;

                        // Do not start the event watcher here, wait until after the startup cleaning job
                        this.pollTimer.Interval = 5000; // short startup;

                        break;

                    default:

                        // Unsupported pollstyle for this object
                        throw new NotSupportedException("Invalid polling style for this job type");
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
            }

        }

        /// <summary>
        /// The files watcher_ created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void FilesWatcherCreated(object sender, FileSystemEventArgs e)
        {
            this._busyEvent.WaitOne();
            try
            {
                var fi = new FileInfo(e.FullPath);
                var di = fi.Directory;
                var files = di.GetFiles();
                
                if (files.Any())
                {
                    var filesToProcess = new List<string>();
                    foreach (var fileInfo in files)
                    {
                        filesToProcess.Add(fileInfo.FullName);
                    }

                    this.Converter(filesToProcess, this.pathXslt + @"\" + this.InstanceName + @"\" + this.InstanceName + ".xsl");
                }

                // this.ProcessFiles(e);
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                // Making sure the files watcher is listening.
                if (this.filesWatcher.EnableRaisingEvents == false)
                {
                    this.filesWatcher.EnableRaisingEvents = true;
                }
            }

            this._busyEvent.Set();
        }

        /// <summary>
        /// The process files.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ProcessFiles(FileSystemEventArgs e)
        {
            // TODO: Also try and check for files in the directory after processing all files triggered by the trigger.

            try
            {
                if (this.filesWatcher.EnableRaisingEvents)
                {
                    this.filesWatcher.EnableRaisingEvents = false;
                }
                
                var retries = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertRetries"]);
                var sleep = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertRetryInterval"]);

                ThreadContext.Properties["JOBNAME"] = this.InstanceName;
                Logger.InfoFormat("XSLTComponent::filesWatcher_Created() hit with file data: {0}", e.FullPath);

                var pollTime = 1000;
                if (Convert.ToInt32(this.PollDelay) < 1000)
                {
                    pollTime = Convert.ToInt32(this.PollDelay * 1000);
                }

                Logger.InfoFormat("We are waiting: {0} Seconds before starting to convert!", pollTime / 1000);

                if (pollTime > 0)
                {
                    Thread.Sleep(pollTime);
                }

                var fileInfo = new FileInfo(e.FullPath);
                var directoryName = fileInfo.DirectoryName;
                if (directoryName != null)
                {
                    this.ProcessDirectoryFiles(directoryName, retries, sleep);
                }

                // Making sure the files watcher is listening.
                if (this.filesWatcher.EnableRaisingEvents == false)
                {
                    this.filesWatcher.EnableRaisingEvents = true;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                var path = Path.Combine(this.FileErrorFolder, e.Name);

                var fileInfo = new FileInfo(path);

                if (fileInfo.Exists)
                {
                    fileInfo.Delete();
                }

                fileInfo = new FileInfo(e.FullPath);
                fileInfo.MoveTo(path);
            }
            finally
            {
                // Making sure the files watcher is listening.
                if (this.filesWatcher.EnableRaisingEvents == false)
                {
                    this.filesWatcher.EnableRaisingEvents = true;
                }
            }
        }

        /// <summary>
        /// The process directory files.
        /// </summary>
        /// <param name="directoryName">
        /// The directory name.
        /// </param>
        /// <param name="retries">
        /// The retries.
        /// </param>
        /// <param name="sleep">
        /// The sleep.
        /// </param>
        private void ProcessDirectoryFiles(string directoryName, int retries, int sleep)
        {
            var fileQueue = new DirectoryInfo(directoryName).GetFiles();

            Logger.InfoFormat("Number of files to be transformed: {0}", fileQueue.Length);

            // I need to have an Debug-flag here
            if (ConfigurationManager.AppSettings["Debug"].ToUpper() == "TRUE")
            {
                foreach (var stringFiles in fileQueue)
                {
                    Logger.Debug("Files to be converted are: " + stringFiles.Name);
                }
            }


            foreach (var fileInfo in fileQueue)
            {
                var fi = new FileInfo(fileInfo.FullName);

                // If the file does not exists, we don't have to compute it
                if (!fi.Exists)
                {
                    continue;
                }

                do
                {
                    FileUtilities.IsFileLocked(fi);
                }
                while (false);

                // = new // = e.Name;
                try
                {
                    var newFile = fi.FullName;
                    this.ProcessFile(newFile, retries, sleep);

                    // We are here if nothing has gone wrong
                    //var path = Path.Combine(this.FileDoneFolder, fi.Name);
                    //FileUtilities.MoveDoneFile(path, fi.FullName);
                    Logger.InfoFormat("Done converting file {0}", fi.FullName);
                }
                catch (Exception exception)
                {
                    ErrorLogger.Error(exception);
                    ErrorFunctions.PathError = this.FileErrorFolder;
                    ErrorFunctions.MoveErrorFile(fileInfo.FullName, this.FileErrorFolder);
                }
            }

            fileQueue = null;
        }

        /// <summary>
        /// The files watcher_ renamed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void FilesWatcherRenamed(object sender, RenamedEventArgs e)
        {
            this._busyEvent.WaitOne();
            try
            {
                // this.ProcessFiles(e);

                var fi = new FileInfo(e.FullPath);
                var di = fi.Directory;
                var files = di.GetFiles();

                if (files.Any())
                {
                    var filesToProcess = new List<string>();
                    foreach (var fileInfo in files)
                    {
                        filesToProcess.Add(fileInfo.FullName);
                    }

                    this.Converter(filesToProcess, this.pathXslt + @"\" + this.InstanceName + @"\" + this.InstanceName + ".xsl");
                }
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                // Making sure the files watcher is listening.
                if (this.filesWatcher.EnableRaisingEvents == false)
                {
                    this.filesWatcher.EnableRaisingEvents = true;
                }
            }

            this._busyEvent.Set();
            
        }

        /// <summary>
        /// The poll timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void PollTimerElapsed(object sender, ElapsedEventArgs e)
        {
            Logger.Info("XSLTComponent::pollTimer_Elapsed()");
            ThreadContext.Properties["JOBNAME"] = this.InstanceName;
            Logger.InfoFormat("XSLTComponent::pollTimer_Elapsed() hit");

            // Reset flags and pollers
            this._busyEvent.WaitOne();
            this.pollTimer.Stop();

            // Process any wating files
            try
            {
                // Enable the event listening
                if (this.pollStyle == PollStyle.FileSystemWatch && !this.ErrorRetry)
                {
                    Logger.Debug("Setting filesWatcher.EnableRaisingEvents to true");
                    this.filesWatcher.EnableRaisingEvents = true;
                }

                // Process the files
                Logger.Debug("Process the files using XSLT-sheet: " + this.pathXslt + @"\" + this.InstanceName + @"\" + this.InstanceName + ".xsl");

                // this.Converter(files, this.pathXslt + @"\" + this.InstanceName + @"\" + this.InstanceName + ".xsl");
                Logger.Debug("InputFolder for " + this.InstanceName + " : " + this.FileInputFolder);

                var retries = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertRetries"]);
                var sleep = Convert.ToInt32(ConfigurationManager.AppSettings["ConvertRetryInterval"]);

                var di = new DirectoryInfo(this.FileInputFolder);
                var files = di.GetFiles();

                if (files.Any())
                {
                    var filesToProcess = new List<string>();
                    foreach (var fileInfo in files)
                    {
                        filesToProcess.Add(fileInfo.FullName);
                    }

                    this.Converter(filesToProcess, this.pathXslt + @"\" + this.InstanceName + @"\" + this.InstanceName + ".xsl");
                }
                // this.ProcessDirectoryFiles(this.FileInputFolder, retries, sleep);
            }
            catch (Exception ex)
            {
                ErrorLogger.Debug("Something happened while doing filesWatcher_Created!", ex);
                ErrorLogger.Error("XSLTComponent::pollTimer_Elapsed() error: " + ex.Message, ex);
            }
            

            // Check error state
            if (this.ErrorRetry)
            {
                this.filesWatcher.EnableRaisingEvents = false;
            }

            // Restart
            this.pollTimer.Interval = this.Interval * 1000;
            if (this.pollStyle != PollStyle.FileSystemWatch || (this.pollStyle == PollStyle.FileSystemWatch && this.filesWatcher.EnableRaisingEvents == false))
            {
                this.pollTimer.Start();
            }

            this._busyEvent.Set();
        }

        #endregion
    }
}