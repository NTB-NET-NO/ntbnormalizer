﻿namespace NTB.Normalizer.Components.enums
{
    /// <summary>
    /// Used to specify the different operating modes. Primarily used for the common XSLT poller component.
    /// </summary>
    public enum OperationMode
    {
        /// <summary>
        /// Converter mode collects files from the input folders and converts them from 
        /// one XML-format to another
        /// </summary>
        Converter,

        /// <summary>
        /// Gatherer mode 
        /// </summary>
        Gatherer
    }
}