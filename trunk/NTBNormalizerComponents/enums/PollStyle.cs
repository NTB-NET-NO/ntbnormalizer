﻿namespace NTB.Normalizer.Components.enums
{
    /// <summary>
    /// Specifies the different polling styles available to the EWS poller component
    /// </summary>
    public enum PollStyle
    {
        /// <summary>
        /// Indicates continous polling og an item at a timed interval.
        /// This is the legacy pollingstyle and may be used for all job types
        /// </summary>
        Continous,
        /// <summary>
        /// Polling is limited to a time schedule, i.e. once a day/week
        /// </summary>
        Scheduled,
        /// <summary>
        /// Contious polling using an EWS folder subscription set.
        /// Allows for easier Exchange folder item checking. Only valid for EWS folder jobs.
        /// </summary>
        PullSubscribe,
        /// <summary>
        /// Notifications from EWS are actively sent to the client using a web service. Only valid for EWS folder jobs.
        /// </summary>
        PushSubscribe,
        /// <summary>
        /// Use File System Notifications with local disk folders instead of polling. Only valid for Gatherer jobs.
        /// </summary>
        FileSystemWatch,
        /// <summary>
        /// Polling happens at scheduled times of the day or once a week.
        /// </summary>
        CalendarPoll
    }
}