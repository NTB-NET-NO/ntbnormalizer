﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CalendarPollStyle.cs" company="">
//   
// </copyright>
// <summary>
//   The calendar poll style.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.Normalizer.Components.enums
{
    /// <summary>
    /// The calendar poll style.
    /// </summary>
    public enum CalendarPollStyle
    {
        /// <summary>
        /// A daily CalendarPollStyle is a pullstyle that happens every day
        /// </summary>
        Daily, 

        /// <summary>
        /// A daily CalendarPollStyle is a pullstyle that happens every week
        /// </summary>
        Weekly, 

        /// <summary>
        /// A daily CalendarPollStyle is a pullstyle that happens every month
        /// </summary>
        Monthly, 

        /// <summary>
        /// A daily CalendarPollStyle is a pullstyle that happens every year
        /// </summary>
        Yearly
    }
}