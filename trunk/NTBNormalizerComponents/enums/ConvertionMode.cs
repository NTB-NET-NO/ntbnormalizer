﻿namespace NTB.Normalizer.Components.enums
{
    /// <summary>
    /// Specifies the different days and which day is the first one. This so that we can support 
    /// scheduling on both day AND time
    /// </summary>

    public enum ConvertionMode
    {
        /// <summary>
        /// Convertion using XSLT version 1
        /// </summary>
        XSLT1,

        /// <summary>
        /// Conversion using XSLT version 2
        /// </summary>
        XSLT2,

        /// <summary>
        /// Conversion using Search and replace
        /// </summary>
        SearchReplace,

        /// <summary>
        /// Conversion from Comma Separated Values files
        /// </summary>
        CSV
    }
}