﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using log4net;
using log4net.Config;
using NTB.Normalizer.Utilities;

namespace NTB.Normalizer.Components.Converters
{
    public class CsvConverter
    {
        /// <summary>
        /// Static logger
        /// </summary>
        static readonly ILog Logger = LogManager.GetLogger(typeof(CsvConverter));

        


        /// <summary>
        /// Propery to store if we are to use fieldnames or not
        /// </summary>
        public bool HasFieldNames { get; set; }

        /// <summary>
        /// Gets or sets the value of FileName
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the delimiter character (\t or ; for instance)
        /// </summary>
        public char Delimiter { get; set; }

        /// <summary>
        /// Gets or sets the header record (true if we want to use the header, false otherwise)
        /// </summary>
        public bool HeaderRecord { get; set; }

        /// <summary>
        /// Gets or sets the value of the PathError public string. It holds the path to the directory where we are to put files if they fail to be converted
        /// </summary>
        /// <remarks>
        /// This has been refactored from public string PathError = ConfigurationManager.AppSettings["dirErrorPath"];
        /// </remarks>
        public string PathError { get; set; }

        /// <summary>
        /// The constructor for this class
        /// </summary>
        public CsvConverter()
        {
            //Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
                BasicConfigurator.Configure();
        }

        private XmlDocument _GenerateXMLStructure(IEnumerable<Dictionary<string, string>> content)
        {
            XmlDocument doc = new XmlDocument();
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);
            XmlElement root = doc.CreateElement("CSV");

            doc.AppendChild(root);

            foreach (Dictionary<string, string> myContent in content)
            {
                
                XmlElement xmlDivision = doc.CreateElement("Division");

                foreach (KeyValuePair<string, string> kvp in myContent)
                {
                    
                    XmlElement xmlKeyElement = doc.CreateElement(kvp.Key);
                    XmlText xmlTextValue = doc.CreateTextNode(kvp.Value);
                    xmlKeyElement.AppendChild(xmlTextValue);
                    xmlDivision.AppendChild(xmlKeyElement);    
                }
                root.AppendChild(xmlDivision);    
                
            }
            
            // doc.AppendChild(root);
            return doc;
        }

        /// <summary>
        /// Private method to write the document
        /// </summary>
        /// <param name="doc"></param>
        private void _WriteDocument(XmlDocument doc)
        {
            foreach (KeyValuePair<int,string> kvp in this.OutputFolders)
            {
                FileInfo fi = new FileInfo(this.FileName);
                string extention = fi.Extension;
                string currentFile = fi.Name.Replace(extention, ".xml");
                string outputFileName = kvp.Value + @"/" + currentFile;

                if (File.Exists(outputFileName))
                {
                    File.Delete(outputFileName);
                }

                Logger.Info("Saving: " + outputFileName);
                doc.Save(outputFileName);
            }
        }

        /// <summary>
        /// Parsing the CSV files
        /// </summary>
        public void ParseCsvFiles()
        {
            if (!this.Files.Any()) return;

            foreach (string fileName in this.Files)
            {
                try
                {
                    this.FileName = fileName;
                    List<Dictionary<string, string>> content = this.ParseCsvFile();

                    XmlDocument convertedFile = this._GenerateXMLStructure(content);
                    this._WriteDocument(convertedFile);

                    // Now we are moving the file
                    FileInfo fi = new FileInfo(fileName);

                    // First we delete the old file ...
                    if (File.Exists(this.DoneFolder + @"\" + fi.Name))
                    {
                        File.Delete(this.DoneFolder + @"\" + fi.Name);
                    }

                    // ... then we move the file...
                    fi.MoveTo(this.DoneFolder + @"\" + fi.Name);
                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while parsing csv files");
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                    ErrorFunctions.PathError = this.PathError;
                    ErrorFunctions.MoveErrorFile(fileName, this.PathError);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> ParseCsvFile()
        {
            this._GetDelimeterChar();
            List<Dictionary<string, string>> content = new List<Dictionary<string, string>>();
            using (LumenWorks.Framework.IO.Csv.CsvReader cReader =
                new LumenWorks.Framework.IO.Csv.CsvReader(new StreamReader(this.FileName), true, this.Delimiter))
            {
                try
                {
                    int fieldCount = cReader.FieldCount;

                    string[] headers = cReader.GetFieldHeaders();

                    while (cReader.ReadNextRecord())
                    {
                        Dictionary<string, string> dictionaryCsvFile = new Dictionary<string, string>();
                        for (int i = 0; i < fieldCount; i++)
                        {
                            dictionaryCsvFile.Add(headers[i], cReader[i] ?? "MISSING");
                        }
                        content.Add(dictionaryCsvFile);
                    }

                    return content;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);

                    return null;
                }
                
            } 

        }

        private void _GetDelimeterChar()
        {
            if (this.FieldSeparatorString.Length == 2)
            {

                var escaped = System.Text.RegularExpressions.Regex.Unescape(this.FieldSeparatorString);
                if (escaped.Length == 1)
                {
                    var character = escaped.ToCharArray()[0];
                    if (char.IsControl(character))
                    {
                        this.Delimiter = character;
                    }

                }
            }
            else
            {
                this.Delimiter = Convert.ToChar(this.FieldSeparatorString);
            }

        }

        public string FieldSeparatorString { get; set; }

        public List<string> Files { get; set; }

        public Dictionary<int, string> OutputFolders { get; set; }

        public string DoneFolder { get; set; }
    }
}
