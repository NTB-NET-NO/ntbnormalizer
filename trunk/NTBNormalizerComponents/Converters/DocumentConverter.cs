﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentConverter.cs" company="Norsk Telegrambyrå AS">
//   Copyright (C) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The document converter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Xml;
using log4net;
using net.sf.saxon.lib;
using NTB.Normalizer.Components.Interfaces;
using NTB.Normalizer.Components.Transformers;
using NTB.Normalizer.Components.Utilities;
using NTB.Normalizer.Utilities;
using Saxon.Api;

namespace NTB.Normalizer.Components.Converters
{
    /// <summary>
    /// The document converter.
    /// </summary>
    public class DocumentConverter : IDocumentConverter, IDisposable
    {
        /// <summary>
        ///     The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(DocumentConverter));

        /// <summary>
        ///     The disposed property saying if this converter is disposed or not
        /// </summary>
        private bool disposed;

        /// <summary>
        ///     Gets or sets the File Xsl
        /// </summary>
        public string FileXsl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether iptc sequence number.
        /// </summary>
        public bool IptcSequenceNumber { get; set; }

        /// <summary>
        ///     Gets or sets the iptc sequence number file.
        /// </summary>
        public string IptcSequenceNumberFile { get; set; }

        /// <summary>
        ///     Gets or sets the iptc sequence number min.
        /// </summary>
        public int IptcSequenceNumberMin { get; set; }

        /// <summary>
        ///     Gets or sets the iptc sequence number max.
        /// </summary>
        public int IptcSequenceNumberMax { get; set; }

        public bool CheckCharacters { get; set; }

        /// <summary>
        ///     Gets or sets Dictionary holding the document extention which so defines the type of document we are to create
        /// </summary>
        public Dictionary<int, string> OutputDocumentExtensions { get; set; }

        /// <summary>
        ///     Gets or sets Dictionary containing encodings, these shall be mapped against output path
        /// </summary>
        public Dictionary<int, string> OutputPathAndEncodings { get; set; }

        /// <summary>
        ///     Gets or sets Hashtable of paths where the transformed articles are to be put
        /// </summary>
        public Dictionary<int, string> OutputPaths { get; set; }

        /// <summary>
        ///     Gets or sets PathRoot holds the path of the root of the Normalizer work directory
        /// </summary>
        public string PathRoot { get; set; }

        /// <summary>
        ///     Gets or sets the non ascii character allowed. If false, we replace the non-ascii characters with the defined replacement character
        /// </summary>
        public bool AllowNonAscii { get; set; }

        /// <summary>
        ///     Gets or sets the character to replace the non-ascii-character
        /// </summary>
        public string NonAsciiReplaceCharacter { get; set; }

        /// <summary>
        /// Gets or sets the iptc sequence value.
        /// </summary>
        public int IptcSequenceValue { get; set; }

        /// <summary>
        ///     Gets or sets the value of the PathError public string. It holds the path to the directory where we are to put files
        ///     if they fail to be converted
        /// </summary>
        /// <remarks>
        ///     This has been refactored from public string PathError = ConfigurationManager.AppSettings["dirErrorPath"];
        /// </remarks>
        public string PathError { get; set; }

        /// <summary>
        ///     Gets or sets job name property. This property holds the name of the job we are to convert. Job = directory and XSLT-files
        /// </summary>
        public string JobName { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether stop signal.
        /// </summary>
        public bool StopSignal
        {
            get
            {
                return this.Signal;
            }

            set
            {
                this.Signal = value;
                if (this.Signal)
                {
                    this.BusyFlag = false;
                }
            }
        }

        /// <summary>
        ///     Gets or sets the transformer.
        /// </summary>
        public XsltTransformer Transformer { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether signal.
        /// </summary>
        private bool Signal { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether busy flag.
        /// </summary>
        private bool BusyFlag { get; set; }

        /// <summary>
        ///     The dispose.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The create xslt compiler.
        /// </summary>
        /// <param name="xsltFile">
        /// The xslt file.
        /// </param>
        /// <param name="processor">
        /// The processor
        /// </param>
        public void CreateXsltCompiler(string xsltFile, Processor processor)
        {
            try
            {
                // Debug information
                Logger.Debug("This is FileXSL: " + this.FileXsl);

                if (!this.CheckIfXsltDocumentExists())
                {
                    return;
                }

                this.CompileXsltDocument(processor);
            }
            catch (Exception exception)
            {
                Logger.Error("Something happened while creating the xslt compiler file");
                ErrorLogger.Error(exception);
            }
        }

        /// <summary>
        ///     The create processor.
        /// </summary>
        /// <returns>
        ///     The <see cref="Processor" />.
        /// </returns>
        public Processor CreateProcessor()
        {
            // Create the processor
            var processor = new Processor();
            processor.SetProperty("http://saxon.sf.net/feature/trace-external-functions", "true");
            processor.SetProperty(FeatureKeys.GENERATE_BYTE_CODE, "false");

            return processor;
        }

        /// <summary>
        ///     The check if xslt document exists.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool CheckIfXsltDocumentExists()
        {
            if (this.FileXsl == string.Empty)
            {
                Logger.Info("No XSLT document provided!");
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// The compile xslt document.
        /// </summary>
        /// <param name="processor">
        /// The processor.
        /// </param>
        public void CompileXsltDocument(Processor processor)
        {
            // Create a compiler
            var compiler = processor.NewXsltCompiler();
            
            compiler.ErrorList = new List<Exception>();
            try
            {
                Logger.Debug("Creating the transformer");
                if (new FileInfo(this.FileXsl).Exists)
                {
                    
                    this.Transformer = compiler
                        .Compile(new Uri(this.FileXsl))
                        .Load();
                }
                else
                {
                    throw new NormalizerException("XSLT file not found!");
                }
            }
            catch (Exception exception)
            {
                
                Logger.Error("something happened when creating the transformer");
                Logger.Error("Please check the error log for more information");
                ErrorLogger.Error(exception);

                foreach (var errorMessage in compiler.ErrorList)
                {
                    ErrorLogger.ErrorFormat("Error message: {0}", errorMessage);
                }
                throw;
            }
        }

        /// <summary>
        /// The do document transformation.
        /// </summary>
        /// <param name="xmlFile">
        /// The xml file.
        /// </param>
        /// <param name="processor">
        /// The processor.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string DoDocumentTransformation(string xmlFile, Processor processor)
        {
            // Debug information
            Logger.Debug("This is FileXSL: " + this.FileXsl);

            if (!this.CheckIfXsltDocumentExists())
            {
                return string.Empty;
            }

            if (!XmlUtilities.IsValidXml(xmlFile))
            {
                return string.Empty;
            }

            if (this.Signal)
            {
                this.BusyFlag = false;
                Logger.Info("We have received a stop signal and so we stop this batch");
                return string.Empty;
            }

            // The file is the path + filename, so this is good
            var fi = new FileInfo(xmlFile)
                         {
                             IsReadOnly = false
                         };

            try
            {
                // Logging information
                Logger.Info("Start converting file " + fi.FullName);

                // Get the file to validate
                Logger.Debug("checking if we have a valid XML-file");

                var readerSettings = new XmlReaderSettings
                                         {
                                             DtdProcessing = DtdProcessing.Parse,
                                             CheckCharacters = this.CheckCharacters 
                                         };

                var documentTransformer = new DocumentTransformer(this.Transformer)
                                              {
                                                  IptcSequenceNumber = this.IptcSequenceNumber,
                                                  IptcSequenceNumberFile = this.IptcSequenceNumberFile,
                                                  IptcSequenceNumberMax = this.IptcSequenceNumberMax,
                                                  IptcSequenceNumberMin = this.IptcSequenceNumberMin,
                                                  IptcSequenceValue = this.IptcSequenceValue,
                                                  PathErrorFolder = this.PathError
                                              };

                var returnString = documentTransformer.TransformDocument(xmlFile, fi, readerSettings, processor);

                return returnString != string.Empty ? returnString : string.Empty;
            }
            catch (Exception e)
            {
                Logger.Error("error: ", e);
                if (e.Message == "Root element is missing.")
                {
                    // Logger.Error(" File: " + fi.FullName + " was empty, or it was not well formed in some way");
                    ErrorLogger.Error(e.Message);
                    ErrorLogger.Error(e.StackTrace);
                }

                ErrorLogger.Error(e.Message);
                ErrorLogger.Error(e.StackTrace);

                // Moving the file that created the error to the error folder
                FileUtilities.PathError = this.PathError;
                FileUtilities.MoveErrorFile(xmlFile);
                return string.Empty;
            }
        }

        /// <summary>
        /// CREATING NEW FILE
        ///     Now doing the actuall creating of the new file
        ///     That is if the document does not have a result-document tag,
        ///     then we are only creating the file to be saved in the output/InstanceName folder
        /// </summary>
        /// <param name="result">
        /// Holds the content of the transformated file
        /// </param>
        /// <param name="xmlFile">
        /// Holds the name of the xml file we have converted
        /// </param>
        public void CreateOutputFile(string result, string xmlFile)
        {
            result = result.Trim();
            result = result.Replace("\0", string.Empty);

            if (result != string.Empty)
            {
                Logger.Debug("--------------------- COUNTING FILE ENCODING ARRAY ---------------------------");

                foreach (var entry in this.OutputPathAndEncodings)
                {
                    // We now also need to get the corresponding key in the outputpath/directory
                    Logger.Debug("Directory - key: " + entry.Key + ", value: " + this.OutputPaths[entry.Key]);

                    DirectoryUtilities.CheckDirectoryExists(this.OutputPaths[entry.Key]);

                    Logger.Debug("Encoding - key: " + entry.Key + ", value: " + entry.Value);
                    var stringEncoding = entry.Value.ToLower() == string.Empty ? "iso-8859-1" : entry.Value.ToLower();

                    var encEncoding = Encoding.GetEncoding(stringEncoding);

                    if (this.OutputDocumentExtensions[entry.Key].Length > 5)
                    {
                        Logger.Debug("For some reason the outputdocument-extention is longer than 4");
                    }

                    var fileExtension = string.Empty;
                    if (this.OutputDocumentExtensions[entry.Key] != string.Empty)
                    {
                        fileExtension = FileUtilities.CreateFileExtension(this.OutputDocumentExtensions[entry.Key]);
                    }

                    var strXmlFileout = FileUtilities.CreateXmlFileName(xmlFile, fileExtension);

                    strXmlFileout = FileUtilities.CreateFullXmlFileName(strXmlFileout, this.OutputPaths[entry.Key]);

                    if (this.AllowNonAscii == false)
                    {
                        strXmlFileout = FileUtilities.ReplaceNonAsciiCharacters(strXmlFileout, this.NonAsciiReplaceCharacter);
                    }

                    try
                    {
                        if (this.OutputDocumentExtensions[entry.Key] == "xml")
                        {
                            XmlUtilities.WriteXmlDocument(result, encEncoding, strXmlFileout);
                        }
                        else
                        {
                            FileUtilities.WriteTextDocument(result, strXmlFileout, encEncoding);
                        }
                    }
                    catch (Exception exception)
                    {
                        ErrorLogger.Error(exception.Message);
                        ErrorLogger.Error(exception.StackTrace);

                        throw;
                    }
                }

                Logger.Debug("--- END LOOPING ---");

                // Setting some bool values that can be read by some other class
                this.BusyFlag = false;

                result = null;
                xmlFile = null;
            }
            else
            {
                // We are sending an e-mail as well, if we are supposed to that is
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]))
                {
                    Logger.Debug("We must wait two seconds before we can move the file to the error folder");
                }

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendErrorEmail"]))
                {
                    var n = new Notifyer();

                    try
                    {
                        n.PathError = this.PathError;
                        n.FileError = xmlFile;
                    }
                    catch (Exception exception)
                    {
                        Logger.Error("There was an error while trying to move the file. Check the error log for more information");
                        ErrorLogger.Error(exception.Message);
                        ErrorLogger.Error(exception.StackTrace);

                        if (exception.InnerException != null)
                        {
                            ErrorLogger.Error(exception.InnerException.Message);
                            ErrorLogger.Error(exception.InnerException.StackTrace);
                        }
                    }
                }

                // Moving to error folder
                FileUtilities.MoveErrorFile(xmlFile);
            }
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                // Free unmanaged resources
                var properties = this.GetType()
                    .GetProperties();

                foreach (var propertyInfo in properties)
                {
                    if (propertyInfo.PropertyType == typeof(string))
                    {
                        propertyInfo.SetValue(this, string.Empty, null);
                    }
                }
            }

            this.disposed = true;
        }
    }
}