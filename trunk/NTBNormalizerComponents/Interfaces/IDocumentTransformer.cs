﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDocumentTransformer.cs" company="Norsk Telegrambyrå AS">
//   Copyright (C) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The DocumentTransformer interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.Normalizer.Components.Interfaces
{
    using System.IO;
    using System.Xml;

    using Saxon.Api;

    /// <summary>
    /// The DocumentTransformer interface.
    /// </summary>
    public interface IDocumentTransformer
    {
        /// <summary>
        /// The transform document.
        /// </summary>
        /// <param name="xmlFile">
        /// The xml file.
        /// </param>
        /// <param name="fi">
        /// The fi.
        /// </param>
        /// <param name="readerSettings">
        /// The reader settings.
        /// </param>
        /// <param name="processor">
        /// The processor.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string TransformDocument(string xmlFile, FileInfo fi, XmlReaderSettings readerSettings, Processor processor);
    }
}