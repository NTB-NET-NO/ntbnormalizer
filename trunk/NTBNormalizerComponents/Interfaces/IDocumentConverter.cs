﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDocumentConverter.cs" company="Norsk Telegrambyrå AS">
//   Copyright (C) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The DocumentConverter interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.Normalizer.Components.Interfaces
{
    using Saxon.Api;

    /// <summary>
    ///     The DocumentConverter interface.
    /// </summary>
    public interface IDocumentConverter
    {
        /// <summary>
        /// The create xslt compiler.
        /// </summary>
        /// <param name="xsltFile">
        /// The xslt file.
        /// </param>
        /// <param name="processor">
        /// The processor.
        /// </param>
        void CreateXsltCompiler(string xsltFile, Processor processor);

        /// <summary>
        /// The create processor.
        /// </summary>
        /// <returns>
        /// The <see cref="Processor"/>.
        /// </returns>
        Processor CreateProcessor();

        /// <summary>
        /// The check if xslt document exists.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool CheckIfXsltDocumentExists();

        /// <summary>
        /// The compile xslt document.
        /// </summary>
        /// <param name="processor">
        /// The processor.
        /// </param>
        void CompileXsltDocument(Processor processor);

        /// <summary>
        /// The do document transformation.
        /// </summary>
        /// <param name="xmlFile">
        /// The xml file.
        /// </param>
        /// <param name="processor">
        /// The processor.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string DoDocumentTransformation(string xmlFile, Processor processor);

        /// <summary>
        /// The create output file.
        /// </summary>
        /// <param name="result">
        /// The result.
        /// </param>
        /// <param name="xmlFile">
        /// The xml file.
        /// </param>
        void CreateOutputFile(string result, string xmlFile);
    }
}