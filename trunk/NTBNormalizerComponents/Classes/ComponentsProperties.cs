﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace NTB.Normalizer.Components.Classes
{
    public class ComponentsProperties
    {
        /// <summary>
        ///     Input file filter
        /// </summary>
        /// <remarks>
        ///     <para>Defines what file types are being read from <see cref="FileInputFolder" />.</para>
        ///     <para>Default value: <c>NTBIDType.None</c></para>
        /// </remarks>
        public string FileFilter = "*.xml";

        /// <summary>
        ///     Input file folder
        /// </summary>
        /// <remarks>
        ///     File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter" /> and
        ///     <see cref="IncludeSubdirs" />
        /// </remarks>
        public string FileInputFolder;

        /// <summary>
        ///     Output file folder
        /// </summary>
        /// <remarks>
        ///     File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter" /> and
        ///     <see cref="IncludeSubdirs" />
        /// </remarks>
        public Dictionary<int, string> FileOutputFolders = new Dictionary<int, string>();

        /// <summary>
        ///     Watch subdirs for new files as well
        /// </summary>
        /// <remarks>
        ///     <para>Defines if the instance should look in subdirectories within <see cref="FileInputFolder" />.</para>
        ///     <para>Default value: <c>False</c></para>
        /// </remarks>
        public bool IncludeSubdirs = false;

        /// <summary>
        ///     The configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///     <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///     <para>Default value: <c>60</c></para>
        /// </remarks>
        public int Interval = 60;

        /// <summary>
        ///     The iptc sequence number.
        /// </summary>
        public bool IptcSequenceNumber;

        /// <summary>
        ///     The iptc sequence number file.
        /// </summary>
        public string IptcSequenceNumberFile;

        /// <summary>
        ///     The _path xslt.
        /// </summary>
        public readonly string PathXslt = ConfigurationManager.AppSettings["dirXSLTRoot"];

        /// <summary>
        ///     Gets or sets the iptc sequence number max.
        /// </summary>
        public int IptcSequenceNumberMax { get; set; }

        /// <summary>
        ///     Gets or sets the iptc sequence number min.
        /// </summary>
        public int IptcSequenceNumberMin { get; set; }

        /// <summary>
        ///     Dictionary to map ID to encodings, which we can use to say which type of encoding the output file shall have
        /// </summary>
        public Dictionary<int, string> DocumentEncodings = new Dictionary<int, string>();

        /// <summary>
        ///     Dictionary to map ID to file extensions, which we can use to say which type of encoding the output file shall have
        /// </summary>
        public Dictionary<int, string> DocumentExtensions = new Dictionary<int, string>();

        /// <summary>
        ///     Flag to say if this component is working or not. I have a feeling we have another parameter for this though.
        /// </summary>
        public bool BusyFlag;

        public string FileDoneFolder { get; set; }

        public string FileErrorFolder { get; set; }

        public string InstanceName { get; set; }

        public double PollDelay { get; set; }
    }
}
