﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Logger.cs" company="">
//   
// </copyright>
// <summary>
//   The logger.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.Normalizer.Components.Classes
{
    using log4net;
    using log4net.Appender;
    using log4net.Core;
    using log4net.Layout;
    using log4net.Repository.Hierarchy;

    /// <summary>
    /// The logger.
    /// </summary>
    public class Logger
    {
        /// <summary>
        /// Gets or sets the log path.
        /// </summary>
        public string LogPath { get; set; }

        /// <summary>
        /// Gets or sets the log file name.
        /// </summary>
        public string LogFileName { get; set; }

        /// <summary>
        /// The setup.
        /// </summary>
        public void Setup()
        {
            var hierarchy = (Hierarchy)LogManager.GetRepository();

            var patternLayout = new PatternLayout();
            patternLayout.ConversionPattern = "%date [%-2thread] %-5level %logger [%mdc{JOBNAME}] [%ndc] - %message%newline";
            patternLayout.ActivateOptions();

            // <lockingModel type="log4net.Appender.FileAppender+MinimalLock" />
            // <conversionPattern value="%date [%-2thread] %-5level %logger [%mdc{JOBNAME}] [%ndc] - %message%newline"/>
            var roller = new RollingFileAppender
                             {
                                 AppendToFile = true,
                                 File = this.LogFileName,
                                 Layout = patternLayout,
                                 MaxSizeRollBackups = 5,
                                 MaximumFileSize = "1GB",
                                 RollingStyle = RollingFileAppender.RollingMode.Size,
                                 StaticLogFileName = true
                             };
            roller.ActivateOptions();
            roller.LockingModel = new FileAppender.MinimalLock();
            hierarchy.Root.AddAppender(roller);

            var memory = new MemoryAppender();
            memory.ActivateOptions();
            hierarchy.Root.AddAppender(memory);

            hierarchy.Root.Level = Level.Info;
            hierarchy.Configured = true;
        }
    }
}