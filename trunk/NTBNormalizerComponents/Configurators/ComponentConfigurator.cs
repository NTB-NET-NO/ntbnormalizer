﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using log4net;
using NTB.Normalizer.Components.Components;
using NTB.Normalizer.Components.enums;

namespace NTB.Normalizer.Components.Configurators
{
    public class ComponentConfigurator
    {
        /// <summary>
        ///     The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(XsltComponent));


        /// <summary>
        ///     The iptc sequence number.
        /// </summary>
        public bool IptcSequenceNumber;

        /// <summary>
        ///     The iptc sequence number file.
        /// </summary>
        public string IptcSequenceNumberFile;

        /// <summary>
        ///     The configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName" /></remarks>
        public string Name;

        /// <summary>
        ///     PollDelay is used to make sure that the file is ready to be accessed.
        /// </summary>
        public double PollDelay;


        /// <summary>
        ///     The configured schedule for this instance
        /// </summary>
        /// <remarks>
        ///     This is set to a time of day and is used by the <c>Scheduled</c> pollstyle. Work is triggered at the given
        ///     time of day.
        /// </remarks>
        public string Schedule;

        /// <summary>
        ///     Buffer size for the file watcher
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         Defines the internal buffer size the FileSystemWatcher when using this polling.
        ///         Should not be set to high. The instance will fall back to contious polling on buffer overflows.
        ///     </para>
        ///     <para>Default value: <c>4096</c></para>
        /// </remarks>
        public int BufferSize = 4096;

        /// <summary>
        ///     Flag to say if this component is working or not. I have a feeling we have another parameter for this though.
        /// </summary>
        public bool BusyFlag;

        /// <summary>
        ///     Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly
        ///     configured
        /// </summary>
        /// <remarks>Holds the configured status of the instance. <c>True</c> means successfully configured</remarks>
        public bool Configured;

        /// <summary>
        ///     Dictionary to map ID to encodings, which we can use to say which type of encoding the output file shall have
        /// </summary>
        public Dictionary<int, string> DocumentEncodings = new Dictionary<int, string>();

        /// <summary>
        ///     Dictionary to map ID to file extensions, which we can use to say which type of encoding the output file shall have
        /// </summary>
        public Dictionary<int, string> DocumentExtensions = new Dictionary<int, string>();

        /// <summary>
        ///     Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        public string EmailBody;

        /// <summary>
        ///     Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        public string EmailNotification;

        /// <summary>
        ///     Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        public string EmailSubject;

        /// <summary>
        ///     Error-Retry flag
        /// </summary>
        /// <remarks>
        ///     The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later
        ///     retry.
        /// </remarks>
        public bool ErrorRetry = false;

        /// <summary>
        ///     File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///     <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///     <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        public string FileDoneFolder;

        /// <summary>
        ///     Error file folder
        /// </summary>
        /// <remarks>
        ///     <para>File folder where failing files are stored.</para>
        ///     <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        public string FileErrorFolder;

        /// <summary>
        ///     Input file filter
        /// </summary>
        /// <remarks>
        ///     <para>Defines what file types are being read from <see cref="FileInputFolder" />.</para>
        ///     <para>Default value: <c>NTBIDType.None</c></para>
        /// </remarks>
        public string FileFilter = "*.xml";

        /// <summary>
        /// The enabled status for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="Enabled"/></remarks>
        public Boolean enabled;

        /// <summary>
        /// Flag to tell which state the component is in (running/halted)
        /// </summary>
        public ComponentState componentState;

        /// <summary>
        ///     Input file folder
        /// </summary>
        /// <remarks>
        ///     File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter" /> and
        ///     <see cref="IncludeSubdirs" />
        /// </remarks>
        public string FileInputFolder;

        /// <summary>
        ///     Output file folder
        /// </summary>
        /// <remarks>
        ///     File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter" /> and
        ///     <see cref="IncludeSubdirs" />
        /// </remarks>
        public Dictionary<int, string> FileOutputFolders = new Dictionary<int, string>();

        /// <summary>
        ///     Watch subdirs for new files as well
        /// </summary>
        /// <remarks>
        ///     <para>Defines if the instance should look in subdirectories within <see cref="FileInputFolder" />.</para>
        ///     <para>Default value: <c>False</c></para>
        /// </remarks>
        public bool IncludeSubdirs = false;

        /// <summary>
        ///     The configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///     <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///     <para>Default value: <c>60</c></para>
        /// </remarks>
        public int Interval = 60;

        /// <summary>
        ///     Gets the component state.
        /// </summary>
        public ComponentState ComponentState
        {
            get
            {
                return this.componentState;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the enabled status is true or false.
        /// </summary>
        /// <value>The enabled status.</value>
        /// <remarks><c>True</c> if the job instance is enabled.</remarks>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
        }

        /// <summary>
        ///     Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The configured instance job name.</remarks>
        public string InstanceName
        {
            get
            {
                return this.Name;
            }
        }

        /// <summary>
        ///     Gets the operation mode.
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Converter</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get
            {
                return OperationMode.Converter;
            }
        }

        /// <summary>
        /// The polling style for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="PollStyle"/></remarks>
        protected PollStyle pollStyle;

        /// <summary>
        ///     Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Converter</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get
            {
                return this.pollStyle;
            }
        }

        /// <summary>
        ///     Gets or sets the iptc sequence number max.
        /// </summary>
        public int IptcSequenceNumberMax { get; set; }

        /// <summary>
        ///     Gets or sets the iptc sequence number min.
        /// </summary>
        public int IptcSequenceNumberMin { get; set; }

        public bool Configure(XmlNode configNode)
        {
            Logger.Debug("Node: " + configNode.Name);

            // Basic configuration sanity check
            if (configNode.Attributes != null && (configNode.Name != "XSLTComponent" || configNode.Attributes.GetNamedItem("Name") == null))
            {
                throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
            }

            // Logger.Debug("Name attribut: " + configNode.Attributes.GetNamedItem("Name"));
            // Basic configuration sanity check
            // if (configNode.Attributes != null && (configNode != null && configNode.Name == "XSLTComponent" &&
            // configNode.Attributes.GetNamedItem("Name") != null))
            // {
            // throw new ArgumentException("The XML configuration node passed is invalid", 
            // "configNode");
            // }

            // Getting the name of this Component instance
            try
            {
                if (configNode.Attributes != null)
                {
                    this.Name = configNode.Attributes["Name"].Value;

                    ThreadContext.Stacks["NDC"].Push(this.InstanceName);

                    this.enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);

                    this.PollDelay = Convert.ToDouble(ConfigurationManager.AppSettings["PollingInterval"]);
                    if (configNode.Attributes["Delay"] != null)
                    {
                        this.PollDelay = Convert.ToDouble(configNode.Attributes["Delay"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance. Check error log for more information.");
                ErrorLogger.Fatal("Not possible to configure this job instance!", ex);

                ThreadContext.Stacks["NDC"].Pop();

                throw new ArgumentException("Invalid or missing values in XML configuration node", ex);
            }

            // Basic operation
            try
            {
                if (configNode.Attributes != null)
                {
                    this.pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }

            // Check if we are to use sequence numbers
            this.SetIptcSequenceNumber(configNode);

            this.SetXmlInputFolder(configNode);

            this.SetOutputFolders(configNode);
            this.SetDocumentEncodings(configNode);
            this.SetDocumentExtensions(configNode);

            // adding the done folder
            this.SetDoneFolder(configNode);

            // adding the error folder
            this.SetErrorFolder(configNode);

            if (this.enabled)
            {
                // Checking if file folders exists
                var fileOutputFolder = string.Empty;

                try
                {
                    var di = new DirectoryInfo(this.FileInputFolder);
                    if (!di.Exists)
                    {
                        Directory.CreateDirectory(this.FileInputFolder);
                    }

                    foreach (var kvp in this.FileOutputFolders)
                    {
                        fileOutputFolder = kvp.Value;
                        di = new DirectoryInfo(fileOutputFolder);
                        if (!di.Exists)
                        {
                            Directory.CreateDirectory(fileOutputFolder);
                        }
                    }

                    // Check if folder exists
                    di = new DirectoryInfo(this.FileErrorFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }

                    // Check if folder exists
                    di = new DirectoryInfo(this.FileDoneFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid, unknown or missing file folder: " + fileOutputFolder, ex);
                }

            }

            // A 

            // Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            this.Configured = true;

            return true;
        }

        /// <summary>
        /// The set document encodings.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetDocumentEncodings(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                var folderNodes = configNode.SelectNodes("XMLOutputFolders/XMLOutputFolder");
                if (folderNodes != null)
                {
                    foreach (var folderNode in folderNodes.Cast<XmlNode>().Where(folderNode => folderNode != null))
                    {
                        if (folderNode.Attributes == null)
                        {
                            continue;
                        }

                        this.DocumentEncodings.Add(Convert.ToInt32(folderNode.Attributes["Id"].Value), folderNode.Attributes["Encoding"] != null ? folderNode.Attributes["Encoding"].Value : "iso-8859-1");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder. Check error log for more information");
                ErrorLogger.Fatal("Not possible to get output-folder", ex);
            }
        }

        /// <summary>
        /// The set document extensions.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetDocumentExtensions(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                var folderNodes = configNode.SelectNodes("XMLOutputFolders/XMLOutputFolder");
                if (folderNodes != null)
                {
                    foreach (var folderNode in folderNodes.Cast<XmlNode>().Where(folderNode => folderNode != null))
                    {
                        if (folderNode.Attributes == null)
                        {
                            continue;
                        }

                        this.DocumentExtensions.Add(Convert.ToInt32(folderNode.Attributes["Id"].Value), folderNode.Attributes["Extention"] != null ? folderNode.Attributes["Extention"].Value : "xml");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder. Check error log for more information");
                ErrorLogger.Fatal("Not possible to get output-folder", ex);
            }
        }

        /// <summary>
        /// The set done folder.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetDoneFolder(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                var folderNode = configNode.SelectSingleNode("XMLDoneFolder");
                if (folderNode != null)
                {
                    this.FileDoneFolder = folderNode.InnerText;
                }

                Logger.InfoFormat("Converter job - Polling: {0} / File done folder: {1} / Enabled: {2}", Enum.GetName(typeof(PollStyle), this.pollStyle), this.FileDoneFolder, this.enabled);
            }
            catch (Exception ex)
            {
                ErrorLogger.Fatal("Not possible to get Done-folder", ex);
                Logger.Fatal("Not possible to get Done-folder. Check error log for more information");
            }
        }

        /// <summary>
        /// The set error folder.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetErrorFolder(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                var folderNode = configNode.SelectSingleNode("XMLErrorFolder");
                if (folderNode != null)
                {
                    this.FileErrorFolder = folderNode.InnerText;
                }

                Logger.InfoFormat("Converter job - Polling: {0} / File done folder: {1} / Enabled: {2}", Enum.GetName(typeof(PollStyle), this.pollStyle), this.FileErrorFolder, this.enabled);
            }
            catch (Exception ex)
            {
                ErrorLogger.Fatal("Not possible to get Done-folder", ex);
                Logger.Fatal("Not possible to get Done-folder. Check error logger for more information");
            }
        }

        /// <summary>
        /// The set iptc sequence number.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetIptcSequenceNumber(XmlNode configNode)
        {
            try
            {
                var folderNode = configNode.SelectSingleNode("IPTCSequenceNumber");
                if (folderNode != null)
                {
                    if (configNode.Attributes != null)
                    {
                        if (folderNode.Attributes != null)
                        {
                            this.IptcSequenceNumber = Convert.ToBoolean(folderNode.Attributes["value"].Value);
                            this.IptcSequenceNumberMin = Convert.ToInt32(folderNode.Attributes["min-number"].Value);
                            this.IptcSequenceNumberMax = Convert.ToInt32(folderNode.Attributes["max-number"].Value);
                        }
                    }

                    if (this.IptcSequenceNumber)
                    {
                        var iptcFolderNode = configNode.SelectSingleNode("IPTCSequenceNumber/SequenceNumberFile");

                        if (iptcFolderNode != null)
                        {
                            this.IptcSequenceNumberFile = iptcFolderNode.InnerText;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Info("We are not to use IPTC-Sequence-numbers");
                Logger.Debug(exception.Message);
                Logger.Debug(exception.StackTrace);
            }
        }

        /// <summary>
        /// The set output folders.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetOutputFolders(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                var folderNodes = configNode.SelectNodes("XMLOutputFolders/XMLOutputFolder");
                if (folderNodes != null)
                {
                    foreach (var folderNode in folderNodes.Cast<XmlNode>().Where(folderNode => folderNode != null))
                    {
                        if (folderNode.Attributes == null)
                        {
                            continue;
                        }

                        this.FileOutputFolders.Add(Convert.ToInt32(folderNode.Attributes["Id"].Value), folderNode.InnerText);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder. Check error log for more information.");
                ErrorLogger.Fatal("Not possible to get output-folder", ex);
            }
        }

        /// <summary>
        /// The set xml input folder.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetXmlInputFolder(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                var folderNode = configNode.SelectSingleNode("XMLInputFolder");

                // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                if (folderNode != null)
                {
                    this.FileInputFolder = folderNode.InnerText;

                    // Check if folder exists
                    var di = new DirectoryInfo(this.FileInputFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }

                Logger.InfoFormat("Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}", Enum.GetName(typeof(PollStyle), this.pollStyle), this.FileInputFolder, this.enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get input-folder. Check error log for more information.");
                ErrorLogger.Fatal("Not possible to get input-folder", ex);
            }
        }

    }
}
