﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XSLTConverter.cs" company="Norsk Telegrambyrå AS">
//   Copyright (C) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   This class shall deal with all things related to converting files
//   it pretty much runs the XslCompiledTransform object to do this
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.Normalizer.Components
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Text;
    using System.Threading;
    using System.Xml;

    using log4net;

    using net.sf.saxon.lib;

    using NTB.Normalizer.Components.Classes;
    using NTB.Normalizer.Components.Utilities;

    using Saxon.Api;

    /// <summary>
    ///     This class shall deal with all things related to converting files
    ///     it pretty much runs the XslCompiledTransform object to do this
    /// </summary>
    public class XsltConverter
    {
        /// <summary>
        ///     Creating the error Logger object so we can separate errors in a separate log file
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        /// The logger.
        /// </summary>
        private static ILog logger;

        /// <summary>
        /// Gets the logger.
        /// </summary>
        public static ILog Logger
        {
            get
            {
                if (logger != null)
                {
                    return logger;
                }

                var type = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                logger = LogManager.GetLogger(type);
                return logger;
            }
        }

        /// <summary>
        ///     The processor.
        /// </summary>
        // private readonly Processor processor = new Processor();

        /// <summary>
        ///     Gets or sets fileOutput holds the name of the file when converted by Normalizer
        /// </summary>
        private string FileOutput { get; set; }


        /// <summary>
        /// The set done full name.
        /// </summary>
        /// <param name="fi">
        /// The fi.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string SetDoneFullName(FileSystemInfo fi)
        {
            // We are creating the path to Done-folder by removing xslt
            var doneFullName = this.PathRoot + @"\done\" + this.JobName + @"\" + fi.Name;
            return doneFullName;
        }

        /// <summary>
        /// The get iptc sequence number.
        /// </summary>
        /// <param name="transformer">
        /// The transformer.
        /// </param>
        private void GetIptcSequenceNumber(XsltTransformer transformer)
        {
            var fileInfo = new FileInfo(this.IptcSequenceNumberFile);
            if (!fileInfo.Exists)
            {
                return;
            }

            // Now we have to read the value of the Sequence number
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(this.IptcSequenceNumberFile);
            var node = xmlDocument.SelectSingleNode("/Ntb/Iptc/SequenceNumber");
            var dt = new DateTime();
            if (node != null && node.Attributes != null)
            {
                dt = Convert.ToDateTime(node.Attributes["date"].Value);

                Logger.Debug("Value of the date attribute: " + dt);
            }

            // Logger.Debug("Value of the node SequenceNumber: " + node.InnerText);
            if (node != null)
            {
                Logger.Debug("Value of the node SequenceNumber: " + node.InnerText);
                this.IptcSequenceValue = Convert.ToInt32(node.InnerText);
            }

            if (dt.Date < DateTime.Today)
            {
                this.IptcSequenceValue = this.IptcSequenceNumberMin;
            }

            var qname = new QName("IptcSequenceNumber");

            var xdmAtomicValue = new XdmAtomicValue(this.IptcSequenceValue);
            var xdmValue = new XdmValue(xdmAtomicValue);
            transformer.SetParameter(qname, xdmValue);

            // And now we have to save the file
            this.IptcSequenceValue += 1;

            // We check if the iptc-sequence number is bigger or equal to max value
            // This means: If range is from 9000 to 10000, we are actually using values 9000-9999
            if (this.IptcSequenceValue >= this.IptcSequenceNumberMax)
            {
                // It was, so we are setting it to the min value
                this.IptcSequenceValue = this.IptcSequenceNumberMin;
            }

            // Setting the new value
            if (node != null)
            {
                node.InnerText = this.IptcSequenceValue.ToString();
                if (node.Attributes != null)
                {
                    var xmlAttribute = node.Attributes["date"];
                    if (xmlAttribute != null)
                    {
                        xmlAttribute.Value = DateTime.Today.ToShortDateString();
                    }
                }
            }

            // Saving the document
            xmlDocument.Save(this.IptcSequenceNumberFile);
        }

        /// <summary>
        /// The do transforming.
        /// </summary>
        /// <param name="xmlFile">
        /// The xml file.
        /// </param>
        /// <param name="transformer">
        /// The transformer.
        /// </param>
        /// <param name="doneFullName">
        /// The done full name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string DoTransformation(string xmlFile, XsltTransformer transformer, string doneFullName)
        {
            // Now we are running the transformer
            using (var sw = new StringWriter())
            {
                try
                {
                    var serializer = new Serializer();
                    serializer.SetOutputWriter(sw);

                    transformer.Run(serializer);

                    return sw.ToString();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);

                    // move file to error folder
                    FileUtilities.MoveErrorFile(xmlFile);

                    // Returning an empty string
                    return string.Empty;
                }
            }
        }
    }
}