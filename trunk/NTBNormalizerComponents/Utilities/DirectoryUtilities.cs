﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DirectoryUtility.cs" company="Norsk Telegrambyrå AS">
//   Copyright (C) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The directory utility.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.Normalizer.Components.Utilities
{
    using System.IO;

    /// <summary>
    /// The directory utility.
    /// </summary>
    public static class DirectoryUtilities
    {
        /// <summary>
        /// The check directory exists.
        /// </summary>
        /// <param name="directoryName">
        /// The directory Name.
        /// </param>
        public static void CheckDirectoryExists(string directoryName)
        {
            var di = new DirectoryInfo(directoryName);
            if (!di.Exists)
            {
                di.Create();
            }
        }
    }
}