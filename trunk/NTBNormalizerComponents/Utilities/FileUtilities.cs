// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileUtilities.cs" company="Norsk Telegrambyrå AS">
//   Copyright (C) 2010-2016 Norsk Telegrambyrå (NTB) AS
// </copyright>
// <summary>
//   The file utilities.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Linq;
using sun.reflect.generics.tree;

namespace NTB.Normalizer.Components.Utilities
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Text;
    using System.Threading;
    
    using log4net;
    /// <summary>
    /// The file utilities.
    /// </summary>
    public class FileUtilities
    {
        /// <summary>
        ///     The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(FileUtilities));

        /// <summary>
        /// Initializes a new instance of the <see cref="FileUtilities"/> class.
        /// </summary>
        public FileUtilities()
        {
            PathError = ConfigurationManager.AppSettings["dirErrorPath"];
        }

        /// <summary>
        ///     Gets or sets the value of the PathError public string. It holds the path to the directory where we are to put files
        ///     if they fail to be converted
        /// </summary>
        /// <remarks>
        ///     This has been refactored from public string PathError = ConfigurationManager.AppSettings["dirErrorPath"];
        /// </remarks>
        public static string PathError { get; set; }

        /// <summary>
        /// The is file locked.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                /*
                 * the file is unavailable because it is:
                 * still being written to
                 * or being processed by another thread
                 * or does not exist (has already been processed)
                 */
                Thread.Sleep(100);
                return true;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }

            // file is not locked
            return false;
        }

        /// <summary>
        /// The move done file.
        /// </summary>
        /// <param name="doneFullName">
        /// The done full name.
        /// </param>
        /// <param name="fullName">
        /// The full name of the file we are to move
        /// </param>
        public static void MoveDoneFile(string doneFullName, string fullName)
        {
            if (File.Exists(doneFullName))
            {
                // In case the done file already exists, we'll delete the file
                File.Delete(doneFullName);
            }

            var fi = new FileInfo(fullName);

            if (fi.Exists)
            {
                return;
            }

            try
            {
                Logger.InfoFormat("Moving file to {0}", doneFullName);
                fi.MoveTo(doneFullName);
                fi.IsReadOnly = false;
                Logger.InfoFormat("File moved to {0}", doneFullName);
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                // We could try and wait a little moment and see if we could move it then
                Thread.Sleep(50);
                fi.MoveTo(doneFullName);
                fi.IsReadOnly = false;
                Logger.InfoFormat("File moved to {0}", doneFullName);    
            }
        }

        /// <summary>
        /// The move error file.
        /// </summary>
        /// <param name="xmlErrorFile">
        /// The xml error file.
        /// </param>
        public static void MoveErrorFile(string xmlErrorFile)
        {
            // Waiting four seconds before we can move the file
            Thread.Sleep(4000);

            var xmlFile = new FileInfo(xmlErrorFile);

            var strErrorPath = Path.Combine(PathError, xmlFile.Name);
            Logger.DebugFormat("Moving error file: {0} to {1}", xmlErrorFile, strErrorPath);
            if (!File.Exists(strErrorPath))
            {
                try
                {
                    xmlFile.MoveTo(strErrorPath);

                    Logger.InfoFormat("File was moved to {0}", strErrorPath);
                }
                catch (Exception exception)
                {
                    Logger.Error("Something happened while moving the file.");
                    ErrorLogger.Error(exception.Message);
                    ErrorLogger.Error(exception.StackTrace);
                }

                return;
            }

            Logger.Info("Checking to see if we are to delete some file or not");

            try
            {
                File.Delete(strErrorPath);

                xmlFile.MoveTo(strErrorPath);

                Logger.InfoFormat("File was moved to {0}", strErrorPath);
            }
            catch (Exception e)
            {
                Logger.Error("Something happened while moving the file");
                ErrorLogger.Error(e.Message);
                ErrorLogger.Error(e.StackTrace);

                Logger.Error("We're deleting the file");
                if (!xmlFile.Exists)
                {
                    Logger.Info("The file does not exists. Cannot delete the file");
                }

                // Deleting file so we can get rid of it 
                xmlFile.Delete();
            }
        }

        /// <summary>
        /// The create file extension.
        /// </summary>
        /// <param name="extension">
        /// The entry.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string CreateFileExtension(string extension)
        {
            if (extension.Contains("."))
            {
                extension = extension.Replace(".", string.Empty);
            }

            return extension;
        }

        public static string ReplaceNonAsciiCharacters(string filename, string replacementCharacter)
        {
            // Find out if the string contains non-ascii characters
            var sb = new StringBuilder();


            // Making sure we do set it to an emtpy string
            if (string.IsNullOrEmpty(replacementCharacter))
            {
                replacementCharacter = string.Empty;
            }

            char[] chars = filename.ToCharArray();

            char ch;

            try
            {
                for (int i = 0; i < chars.Length; i++)
                {
                    ch = chars[i];

                    

                    if (ch > 128)
                    {
                        chars[i] = replacementCharacter[0];
                    }

                }

                var returnString = new string(chars);
                return returnString;

            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }

            
            return sb.ToString();
        }

        /// <summary>
        /// The create xml file name.
        /// </summary>
        /// <param name="filename">
        /// The filename.
        /// </param>
        /// <param name="fileExtension">
        /// The filename.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string CreateXmlFileName(string filename, string fileExtension)
        {
            if (filename != string.Empty)
            {
                if (fileExtension == string.Empty)
                {
                    return filename;
                }

                if (fileExtension.Contains("."))
                {
                    fileExtension = fileExtension.Replace(".", string.Empty);
                }

                return filename.Remove(filename.LastIndexOf('.') + 1) + fileExtension;

            }

            var dt = DateTime.Today;

            var createDateTime = dt.Year + dt.Month + dt.Day + "T" + dt.Hour + dt.Minute + dt.Second;

            if (fileExtension != string.Empty)
            {
                if (fileExtension.Contains("."))
                {
                    fileExtension = fileExtension.Replace(".", string.Empty);
                }

                fileExtension = "." + fileExtension;
            }

            var strXmlFileout = "NTB-" + createDateTime + fileExtension;
            return strXmlFileout;
        }

        /// <summary>
        /// The create xml file name.
        /// </summary>
        /// <param name="xmlFile">
        /// The xml file.
        /// </param>
        /// <param name="outputPath">
        /// The entry.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string CreateFullXmlFileName(string xmlFile, string outputPath)
        {
            var fi = new FileInfo(xmlFile);
            var filename = fi.Name;
            var strXmlFileOut = Path.Combine(outputPath, filename);

            return strXmlFileOut;
        }

        /// <summary>
        /// The write text document.
        /// </summary>
        /// <param name="result">
        /// The result.
        /// </param>
        /// <param name="strXmlFileout">
        /// The str xml fileout.
        /// </param>
        /// <param name="encEncoding">
        /// The enc encoding.
        /// </param>
        public static void WriteTextDocument(string result, string strXmlFileout, Encoding encEncoding)
        {
            Logger.Debug("Writing file in text format");
            Logger.InfoFormat("Storing file: {0}", strXmlFileout);

            File.WriteAllText(strXmlFileout, result, encEncoding);
        }
    }
}