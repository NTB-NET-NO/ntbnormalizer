﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XmlUtilities.cs" company="Norsk Telegrambyrå AS">
//   Copyright (C) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The xml utilities.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.Normalizer.Components.Utilities
{
    using System;
    using System.Text;
    using System.Xml;

    using log4net;

    using NTB.Normalizer.Utilities;

    /// <summary>
    /// The xml utilities.
    /// </summary>
    public static class XmlUtilities
    {
        /// <summary>
        ///     The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(XmlUtilities));

        /// <summary>
        /// The can read xml file.
        /// </summary>
        /// <param name="filename">
        /// The filename.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="NormalizerException">
        /// Throws an exception on error
        /// </exception>
        public static bool CanReadXmlFile(string filename)
        {
            try
            {
                // This test is here just to check if we can open up the file
                var doc = new XmlDocument();
                doc.Load(filename);

                return true;
            }
            catch (XmlException ex)
            {
                // Throws an exception if the file was not possible to open
                throw new NormalizerException("Could not open XML file: " + filename, ex);
            }
            catch (Exception ex)
            {
                // Throws an exception if the file was not possible to open
                throw new NormalizerException("Could not open XML file: " + filename, ex);
            }
        }

        /// <summary>
        /// The is valid xml.
        /// </summary>
        /// <param name="xmlFile">
        /// The xml file.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool IsValidXml(string xmlFile)
        {
            using (var xmlTextReader = new XmlTextReader(xmlFile))
            {
                try
                {
                    Logger.Debug("Trying to read the document");
                    while (xmlTextReader.Read())
                    {
                    }

                    return true;
                }
                catch (Exception exception)
                {
                    Logger.Error("Cannot read the document!");

                    ErrorLogger.Error(exception.Message);
                    ErrorLogger.Error(exception.StackTrace);

                    if (exception.InnerException == null)
                    {
                        return false;
                    }

                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);

                    return false;
                }
            }
        }

        /// <summary>
        /// The write xml document.
        /// </summary>
        /// <param name="result">
        /// The result.
        /// </param>
        /// <param name="encEncoding">
        /// The enc encoding.
        /// </param>
        /// <param name="strXmlFileout">
        /// The str xml fileout.
        /// </param>
        public static void WriteXmlDocument(string result, Encoding encEncoding, string strXmlFileout)
        {
            Logger.Debug("Writing file using XmlDocument and XmlWriter");
            var doc = new XmlDocument();

            doc.LoadXml(result);

            var writerSettings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = false,
                Encoding = encEncoding

                // Encoding.GetEncoding("iso-8859-1") // This has been changed while in documentation mode
            };

            using (var writer = XmlWriter.Create(strXmlFileout, writerSettings))
            {
                Logger.InfoFormat("Storing file: {0}", strXmlFileout);
                doc.Save(writer);
            }
        }
    }
}