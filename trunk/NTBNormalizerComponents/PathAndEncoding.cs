﻿namespace NTB.Normalizer.Components
{
    using System.Collections;

    class PathAndEncoding
    {
        private Hashtable htJobs = new Hashtable();

        public PathAndEncoding(object jobName, Hashtable htPath, Hashtable htEncoding)
        {
            Hashtable htStuff = new Hashtable();
            htStuff.Add(htPath, htEncoding);

            // Adding to the htJobs hashtable
            this.htJobs.Add(jobName, htStuff);
        }

    }
}
