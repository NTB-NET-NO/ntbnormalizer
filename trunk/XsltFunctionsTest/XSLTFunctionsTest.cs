﻿using NTB.Normalizer.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace XsltFunctionsTest
{
    
    
    /// <summary>
    ///This is a test class for XsltFunctionsTest and is intended
    ///to contain all XsltFunctionsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class XsltFunctionsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SportCalculateTimeFromWinner
        ///</summary>
        [TestMethod()]
        public void SportCalculateTimeFromWinnerTest()
        {
            string winnerTime = "24.46,2"; // TODO: Initialize to an appropriate value
            string currentTime = "24.47,2"; // TODO: Initialize to an appropriate value
            string expected = "0.01,0"; // TODO: Initialize to an appropriate value
            string actual;
            actual = XsltFunctions.SportCalculateTimeFromWinner(winnerTime, currentTime);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
