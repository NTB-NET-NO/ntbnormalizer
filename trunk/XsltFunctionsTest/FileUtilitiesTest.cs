﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.Normalizer.Components.Utilities;

namespace XsltFunctionsTest
{
    [TestClass]
    public class FileUtilitiesTest
    {
        [TestMethod]
        public void CreateXmlFileNameTest()
        {
            // var fileUtilities = new FileUtilities();
            var actual = FileUtilities.CreateXmlFileName("test.doc", "txt");

            var expected = "test.txt";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CheckNonAsciiCharacters()
        {
            const string filename = "TrondHusø.xml";

            var actual = FileUtilities.ReplaceNonAsciiCharacters(filename, "-");

            const string expected = "TrondHus-.xml";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CreateXmlFileNameTest_ExtentionEmpty()
        {
            // var fileUtilities = new FileUtilities();
            var actual = FileUtilities.CreateXmlFileName("Test.txt", string.Empty);

            var expected = "Test.txt";

            Assert.AreEqual(expected, actual);
        }
    }
}
