﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XsltConverterTest.cs" company="">
//   
// </copyright>
// <summary>
//   This is a test class for XsltConverterTest and is intended
//   to contain all XsltConverterTest Unit Tests
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace XsltFunctionsTest
{
    using System.IO;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using NTB.Normalizer.Components;

    /// <summary>
    /// This is a test class for XsltConverterTest and is intended
    /// to contain all XsltConverterTest Unit Tests
    /// </summary>
    [TestClass()]
    public class XsltConverterTest
    {
        /// <summary>
        /// The test context instance.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        
    }
}