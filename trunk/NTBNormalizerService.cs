﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceProcess;
using System.Reflection;
using System.Configuration;
using System.IO;
using ntb_FuncLib;
using NTBNormalizer;
using System.Xml;
using System.Xml.XPath;
using System.Threading;

namespace NTBNormalizerService
{
    public partial class NTBNormalizerService : ServiceBase
    {
        string dirLogPath = NTBNormalizer.Properties.Settings.Default.dirLogPath;
        string dirRootPath = NTBNormalizer.Properties.Settings.Default.dirPathRoot;
        string dirErrorPath = NTBNormalizer.Properties.Settings.Default.dirErrorPath;
        string dirXSLTPath = NTBNormalizer.Properties.Settings.Default.dirXSLTRoot;

        public string strJobName = "";

        public string strMessage = "";

        private static LogFile logFile = new LogFile();

        bool boolRunningConvert = false;

        public bool loopCSVFolder = false;

        public NTBNormalizerService()
        {
            InitializeComponent();

            // We should also check if we have folders that exists or not
        }


        protected override void OnStart(string[] args)
        {

            // Let's test this then:
            string[] arguments = Environment.GetCommandLineArgs();

            if (NTBNormalizer.Properties.Settings.Default.Debug == true)
            {
                strMessage = "argument.length: " + arguments.Length.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);


                if (arguments.Length > 0)
                {
                    for (int i = 0; i < arguments.Length; i++)
                    {
                        strMessage = "argument (" + i.ToString() + ": " + arguments[i].ToString();
                        LogFile.WriteLog(ref dirLogPath, ref strMessage);
                    }
                }

            }
            
            try
            {
                EventLog.WriteEntry("Starting NTB Normalizer");
                strMessage = "";

                string strLine = "------------------------------------------------------------";
                string strStartMessage = "                  STARTING NTB Normalizer";



                LogFile.WriteLogNoDate(ref dirLogPath, ref strLine);
                LogFile.WriteLogNoDate(ref dirLogPath, ref strStartMessage);
                LogFile.WriteLogNoDate(ref dirLogPath, ref strLine);

                NormalizerFileSystemWatcher.Path = NTBNormalizer.Properties.Settings.Default.dirXMLRoot;
                

                if (arguments.Length > 1)
                {
                    strMessage = "Number of arguments: " + arguments.Length.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                    // let's create some arguments
                    /**
                     * /j Jobname - Defines which job the service shall run - cannot have multiple jobs
                     * /i jobname - Defines that the jobname shall be installed - can have multiple jobs
                     * /d jobname - deletes the 
                     */

                    switch (arguments[1].ToString())
                    {
                        case "/j":

                            strJobName = arguments[2].ToString();

                            strMessage = "Setting job string to: " + strJobName;
                            LogFile.WriteLog(ref dirLogPath, ref strMessage);

                            break;

                        case "/i":
                            int installcounter = 0;
                            ArrayList arrInstallServiceJob = new ArrayList();

                            foreach (string strArguments in args)
                            {
                                if (installcounter > 0)
                                {

                                    arrInstallServiceJob.Add(strArguments);
                                }
                                installcounter++;
                            }

                            InstallService(arrInstallServiceJob);
                            break;

                        case "/u":
                            int uninstallcounter = 0;
                            ArrayList arrUinstallServiceJob = new ArrayList();

                            foreach (string strArguments in args)
                            {
                                if (uninstallcounter > 0)
                                {
                                    arrUinstallServiceJob.Add(strArguments.ToString());
                                }
                                uninstallcounter++;
                            }
                            break;


                    }
                }
                else if (arguments.Length <= 1) 
                {
                    // Then we need to loop through the jobs in the jobfile 
                    // (which I though I have done in a previous code)

                    // Setting the timer interval to whatever is set in the config-file
                    IntervalTimer.Interval = Convert.ToInt32(NTBNormalizer.Properties.Settings.Default.PollingInterval);

                    // Enable the timer
                    IntervalTimer.Enabled = true;

                    // start the timer
                    IntervalTimer.Start();
                    

                }

                IntervalTimer.Interval = Convert.ToInt32(NTBNormalizer.Properties.Settings.Default.PollingInterval);
                IntervalTimer.Enabled = true;
                IntervalTimer.Start();
            }

            catch (Exception e)
            {
                strMessage = e.Message.ToString();
                string strStack = e.StackTrace.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);
                LogFile.WriteLog(ref dirLogPath, ref strStack);


                if (e.InnerException.Message.ToString() != "")
                {
                    strMessage = e.InnerException.Message.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                }
            }
        }

        protected override void OnStop()
        {
            
            try
            {
                string strLine = "------------------------------------------------------------";
                string strStartMessage = "                  STOPPING NTB Normalizer";
                LogFile.WriteLogNoDate(ref dirLogPath, ref strLine);
                LogFile.WriteLogNoDate(ref dirLogPath, ref strStartMessage);
                LogFile.WriteLogNoDate(ref dirLogPath, ref strLine);


                IntervalTimer.Enabled = false;
                IntervalTimer.Stop();

                

            }
            catch (Exception e)
            {
                string strMessage = e.Message.ToString();
                string strStack = e.StackTrace.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strMessage);
                LogFile.WriteLog(ref dirLogPath, ref strStack);


                if (e.InnerException.Message.ToString() != "")
                {
                    strMessage = e.InnerException.Message.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                }

            }

        }



        private void InstallService(ArrayList args)
        {
            Boolean boolServiceExists = false;

            strMessage = "In InstallService";
            LogFile.WriteLog(ref dirLogPath, ref strMessage);


            strMessage = "Number of arguments: " + args.Count.ToString();
            LogFile.WriteLog(ref dirLogPath, ref strMessage);

            foreach (string strArguments in args)
            {

                ServiceController sc = new ServiceController();
                string strDisplayName = "NTB Normalizer Service" + ":" + strArguments;

                // NTB Normalizer Service:FIS2NITF
                if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                {
                    strMessage = "strDisplayName: " + strDisplayName;
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                    strMessage = "sc.Displayname: " + sc.DisplayName.ToString();
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                }
                // Creating a ServiceController object
                ServiceController[] services = ServiceController.GetServices();




                foreach (ServiceController service in services)
                {
                    if (service.DisplayName == strDisplayName)
                    {
                        // NTB Normalizer Service:FIS2NITF
                        if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                        {
                            strMessage = "strDisplayName: " + service.ServiceName;
                            LogFile.WriteLog(ref dirLogPath, ref strMessage);
                        }

                        boolServiceExists = true;

                        if (service.Status == ServiceControllerStatus.Stopped)
                        {
                            service.Start();
                        }
                    }

                }

                if (sc.DisplayName.ToString() == strDisplayName)
                {
                    switch (sc.Status)
                    {
                        case ServiceControllerStatus.Running:
                            strMessage = "Running";
                            break;
                        case ServiceControllerStatus.Stopped:
                            strMessage = "Stopped";
                            sc.Start();
                            break;
                        case ServiceControllerStatus.Paused:
                            strMessage = "Paused";
                            break;
                        case ServiceControllerStatus.StopPending:
                            strMessage = "Stopping";
                            break;
                        case ServiceControllerStatus.StartPending:
                            strMessage = "Starting";
                            break;
                        default:
                            strMessage = "Status Changing";
                            break;
                    }
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                }
                else
                {
                    // Does not exists
                    strMessage = "NTB Normalizer Service:" + strArguments + " does not exists. We will install it";
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                    // Then we shall create it
                    strMessage = "Creating NTB Normalizer Service:" + strArguments;
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);

                    NTBNormalizer._ServiceInstaller si = new NTBNormalizer._ServiceInstaller();
                    string strServiceName = strDisplayName.Replace(" ", "");

                    if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                    {
                        strMessage = "strExePath:" + Assembly.GetExecutingAssembly().Location.ToString();
                        LogFile.WriteLog(ref dirLogPath, ref strMessage);
                    }

                    // Creating the exe-file string used in the path for Service
                    string strExePath = Assembly.GetExecutingAssembly().GetName().CodeBase;
                    UriBuilder uri = new UriBuilder(strExePath);
                    string path = Uri.UnescapeDataString(uri.Path);

                    if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                    {
                        strMessage = "path:" + path.ToString();
                        LogFile.WriteLog(ref dirLogPath, ref strMessage);
                    }
                    strExePath = System.IO.Path.GetFullPath(path) + " /j " + strArguments;

                    si.InstallService(strExePath, strServiceName, strDisplayName);

                    ServiceController msc = new ServiceController(strDisplayName);
                    if (msc.Status == ServiceControllerStatus.Stopped)
                    {
                        // Does not exists
                        strMessage = "Starting NTB Normalizer Service:" + strArguments;
                        LogFile.WriteLog(ref dirLogPath, ref strMessage);

                        msc.Start();
                    }




                }


            }
        }

        private void IntervalTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {

                if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                {
                    strMessage = "In Elapsed timer!";
                    LogFile.WriteLog(ref dirLogPath, ref strMessage);
                }
                
                /*
                 * I want to test if the there are changes in the folder and that we can get 
                 * the file that has just been copied to any of the directories and 
                 * convert it as soon as possible
                 * 
                 * This to make the normalizer to work faster
                 */


                NormalizerFileSystemWatcher.EnableRaisingEvents = true;
                
                // We are checking if converting has started or not
                if (boolRunningConvert == false)
                {
                    if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                    {
                        strMessage = "sett to true";
                        LogFile.WriteLog(ref dirLogPath, ref strMessage);
                    }

                    // We are now setting that converting is in progress
                    boolRunningConvert = true;

                    // We are creating a new program object
                    Program p = new Program();

                    // We are checking that we do have a jobname to work on
                    if (strJobName != "")
                    {
                        strMessage = "Running job: " + strJobName;
                        LogFile.WriteLog(ref dirLogPath, ref strMessage);

                        p.LoopDirectories(strJobName);
                    }
                    else
                    {
                        p.LoopDirectories(@"");
                    }

                    if (NTBNormalizer.Properties.Settings.Default.Debug == true)
                    {
                        strMessage = "set to false";
                        LogFile.WriteLog(ref dirLogPath, ref strMessage);
                    }
                    boolRunningConvert = false;
                }
                 
            }
            catch (Exception ie)
            {
                string strMessage = "An error has occured: ";
                strMessage += ie.Message.ToString();

                string strStack = ie.StackTrace.ToString();
                LogFile.WriteLog(ref dirLogPath, ref strStack);
                LogFile.WriteLog(ref dirLogPath, ref strMessage);

                boolRunningConvert = false;
            }
                

        }

        private void NormalizerFileSystemWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            // This is something cool I guess
            string strMessage = "A NormalizerFileSystemWatcher_Changed Event has occured: ";
            strMessage += e.ChangeType.ToString();

            LogFile.WriteLog(ref dirLogPath, ref strMessage);
        }

        


    }
}
